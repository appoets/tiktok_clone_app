<?php
//ini_set('display_errors', 1);
include_once 'db.php';

require './PHPMailer/src/Exception.php';
require './PHPMailer/src/PHPMailer.php';
require './PHPMailer/src/SMTP.php';
include("csrfhandler.lib.php");

date_default_timezone_set("Asia/Kolkata");

$token = $_POST["_token"];

$isValid = csrf::checkToken($token);

$mail = new PHPMailer\PHPMailer\PHPMailer();

if($isValid == true){

    csrf::flushKeys();
    if(isset($_POST) && isset($_POST['Email']))
    {
        unset($_POST['_token']);

        $list=0;

        if(!empty($_POST['list']) && $_POST['list']==1)
        {
            $list=1;
            unset($_POST['list']); 
        }
        unset($_POST['phone_dummy']); 

        unset($_POST['to']);  

       $sql = "INSERT INTO tiktok_clone_leads (mail,email,form_title,created_at,updated_at) VALUES ('".json_encode($_POST)."','".$_POST["Email"]."','".$_POST["form-title"]."','".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."')";
        mysqli_query($conn,$sql);
        
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                 
            $mail->isSMTP();                                      
            $mail->Host = MAIL_HOST;
            $mail->SMTPAuth = true;                               
            $mail->Username = MAIL_USERNAME;                 
            $mail->Password = MAIL_PASSWORD;                           
            $mail->SMTPSecure = 'tls';                            
            $mail->Port = 587;                                    
         
        $to = "ben@blockchainappfactory.com,mahi@appoets.com,amar@appoets.com,anbarasan@appoets.com,santhosh@appdupe.com,prasanth@appoets.com,ravikishen@appoets.com,hema@appoets.com,pooja@appdupe.com,brindha@appoets.com,afshana@appdupe.com,shalini@appdupe.com,joseph@appoets.com,ramkumarn@appdupe.com,harikrishnan@appdupe.com,nisha@appoets.com,kamesh@appoets.com,james@blockchainappfactory.com,ram@appdupe.com,ram@uberlikeapp.com,vijay@appoets.com";
             //$to = "vijay@appoets.com,nisha@appoets.com";
            
            $tomails=explode(",",$to);
         
            //Recipients
            $mail->setFrom(MAIL_FROM_ADDRESS, MAIL_FROM_NAME);
         
            foreach($tomails as $ekey => $tomail)
            {
               $mail->addAddress($tomail);
            }

            // $mail->addBCC('nisha@appoets.com');
         
            $htmlcontent = "<b>TikTok Clone App | Contact Form</b><br><br>";

            if($list==1)
            {
               
               foreach ($_POST as $key => $listing) {

                   if($key == "listing-services")
                   {
                       $htmlcontent .= "<b>".ucwords($key)." :</b>".implode(", ",$listing)."<br><br>";

                   }
                   else if($key == "total_price_btc")
                   {
                       $htmlcontent .= "<b>Total Cost :</b>".$listing." BTC<br><br>";
                   }
                   else
                       $htmlcontent .= "<b>".ucwords($key)." :</b>".$listing."<br><br>";

               } 
            }else{   
                unset($_POST['to']);

                 if(isset($_POST['form-title'])){
                    $htmlcontent .= "<b>Form Title : </b>".$_POST['form-title']."<br><br>";
                }

                $htmlcontent .= "<b>Name : </b>".$_POST['Name']."<br><br>";
                
                $htmlcontent .= "<b>Email : </b>".$_POST['Email']."<br><br>";

                $htmlcontent .= "<b>Skype :</b>".$_POST['skype']."<br><br>"; 
             
                $htmlcontent .= "<b>Phone : </b>".$_POST['Phone']."<br><br>";
             
                $htmlcontent .= "<b>Subject : </b>".$_POST['Subject']."<br><br>";        
             
                $htmlcontent .= "<b>Message : </b>".$_POST['Message']."<br><br>";

                $htmlcontent .= "<b>URL : </b>".$_POST['Page_URL']."<br><br>";

                $htmlcontent .= "<b>IP : </b>".$_POST['IP']."<br><br>";
             
             
               

                if(isset($_POST['btc_value'])){
                    $htmlcontent .= "<b>Pricing Package :</b>".$_POST['btc_value']."<br><br>";
                }
            }    
         
            $message = $htmlcontent;        
            
            //Content
            $mail->isHTML(true);                                  
            $mail->Subject = 'TikTok Clone App | Lead Contacts';
            $mail->Body    = $htmlcontent;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
         
            $mail->send();
            //echo 'Message has been sent';
            echo 1;
         } catch (Exception $e) {
            //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
             echo 0;
         }

     }
}else{  
    echo 'Invalid token';
}     
?>
