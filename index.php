<?php include("csrfhandler.lib.php"); ?>
<!doctype html>
<html lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development</title>
    <meta name="description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! "/>
    <meta name="keywords" content="Tiktok clone, Tiktok clone app, Tiktok clone script, social video dubbing app development, video sharing app development, Tiktok like app development, app like Tiktok, Tiktok app clone, Tiktok app clone development, social video sharing app development, white label Tiktok clone, white-label Tiktok app clone, Tiktok alternative app development" />

    <link rel="canonical" href="https://www.tiktokcloneapp.com/" />

    <meta property="og:title" content="Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.tiktokcloneapp.com/" />
                 
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="400" />
    <meta property="og:site_name" content="Tiktok Clone App" />
    <meta property="og:description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! " />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Tiktok Clone App"/>
    <meta name="twitter:title" content="Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development" />
    <meta name="twitter:description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! " />
    <meta name="twitter:image" content="https://www.tiktokcloneapp.com/assets/img/og_image.jpg" />
    <meta itemprop="image" content="https://www.tiktokcloneapp.com/assets/img/og_image.jpg" />
    <meta property="og:image" content="https://www.tiktokcloneapp.com/assets/img/og_image.jpg" /> 
    
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Source+Sans+Pro:600,700" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/style.min.css"/>
      <link rel="stylesheet" href="build/css/intlTelInput.css">
      <link rel="shortcut icon" href="assets/img/favicon.png" sizes="32x32" />
        <link rel="icon" href="assets/img/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="assets/img/favicon.png" />
        <meta name="csrf-token" content="<?php echo csrf::setToken();?>">

        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171522429-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171522429-1');
</script>
<meta name="google-site-verification" content="3z0pw86tIk9-cvKvW4jh444S9IsIOLUoNnQvIgqcYvc" />
      <style>
         svg:not(:root).svg-inline--fa{overflow:visible;}
         .svg-inline--fa{display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;}
         .svg-inline--fa.fa-w-12{width:.75em;}
         .svg-inline--fa.fa-w-14{width:.875em;}
         .svg-inline--fa.fa-w-18{width:1.125em;}
         .order-1{-ms-flex-order:1;order:1;}
         .order-2{-ms-flex-order:2;order:2;}
         @media (min-width:992px){
         .order-lg-1{-ms-flex-order:1;order:1;}
         .order-lg-2{-ms-flex-order:2;order:2;}
         }
         .media{display:-ms-flexbox;display:flex;-ms-flex-align:start;align-items:flex-start;}
         .media-body{-ms-flex:1;flex:1;}
         .d-none{display:none!important;}
         @media (min-width:992px){
         .d-lg-block{display:block!important;}
         }
         .cta h3 {
    color: #fff;
    font-size: 40px;
    line-height: 1.3;
}
         .justify-content-between{-ms-flex-pack:justify!important;justify-content:space-between!important;}
         .overflow-hidden{overflow:hidden!important;}
         .mt-4{margin-top:1.5rem!important;}
         .mb-4{margin-bottom:1.5rem!important;}
         .py-2{padding-top:.5rem!important;}
         .py-2{padding-bottom:.5rem!important;}
         .pt-4{padding-top:1.5rem!important;}
         .pr-4{padding-right:1.5rem!important;}
         .mx-auto{margin-right:auto!important;}
         .mx-auto{margin-left:auto!important;}
         @media (min-width:992px){
         .pt-lg-0{padding-top:0!important;}
         }
         .text-capitalize{text-transform:capitalize!important;}
         .btn{font-size:15px;font-weight:500;color:#fff;line-height:1;text-align:center;padding:18px 30px;border:0 none;border-radius:6px;outline:0 none;position:relative;z-index:1;}
         .btn:hover,.btn:focus,.btn:active{color:#fff;-webkit-box-shadow:0 10px 25px rgba(0, 0, 0, 0.1);box-shadow:0 10px 25px rgba(0, 0, 0, 0.1);}
         .btn,.btn:active{background:linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);}
         .service-icon span{height:54px;width:54px;display:inline-block;text-align:center;line-height:54px;font-size:24px;color:#7c4fe0;border-radius:4px;background-color:#fff;border:1px solid rgba(0, 0, 0, 0.1);-webkit-box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);-webkit-transition:all 0.3s ease 0s;transition:all 0.3s ease 0s;}
         @media (max-width: 991px){
         h2{font-size:36px;}
         }
         @media (max-width: 767px){
         h2{font-size:30px;line-height:1.4;}
         }
         @media (max-width: 575px){
         h2{font-size:24px;line-height:1.4;}
         p{line-height:26px;}
         }
         /*  */
         .rounded-pill{border-radius:50rem!important;}
         .justify-content-center{-ms-flex-pack:center!important;justify-content:center!important;}
         .overflow-hidden{overflow:hidden!important;}
         .shadow-sm{box-shadow:0 .125rem .25rem rgba(0,0,0,.075)!important;}
         .mr-1{margin-right:.25rem!important;}
         .mb-2{margin-bottom:.5rem!important;}
         .mb-3{margin-bottom:1rem!important;}
         .mt-4{margin-top:1.5rem!important;}
         .py-2{padding-top:.5rem!important;}
         .py-2{padding-bottom:.5rem!important;}
         .px-4{padding-right:1.5rem!important;}
         .px-4{padding-left:1.5rem!important;}
         .p-5{padding:3rem!important;}
         .text-center{text-align:center!important;}
         .text-primary{color:#007bff!important;}
         .fw-5{font-weight:500;}
         .avatar-sm{height:3rem;width:3rem;}
         .text-primary{color:#7c4fe0!important;}
         .section-heading{margin-bottom:80px;text-align:center;}
         .section-heading > span{color:#222;}
         .section-heading > h2{position:relative;}
         .section-heading > h2::after{position:absolute;content:'';height:2px;width:70px;background-color:#7c4fe0;bottom:-15px;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);}
         .ptb_100{padding:100px 0;}
        .features-area.style-two .image-box {
    min-height: 290px;
    position: relative;
    -webkit-box-shadow: 0 3px 20px 0px rgba(0, 0, 0, 0.12);
    box-shadow: 0 3px 20px 0px rgba(0, 0, 0, 0.12);
    border-radius: 1.5rem;
    -webkit-transition: -webkit-transform 0.3s ease 0s;
    transition: -webkit-transform 0.3s ease 0s;
    transition: transform 0.3s ease 0s;
    transition: transform 0.3s ease 0s, -webkit-transform 0.3s ease 0s;
    padding: 3rem 15px 0 !important;
    background: #fff;
}
             .features-area.style-two .image-box p {
    font-weight: normal;
    margin-bottom: 0;
    font-size: 15px;
    line-height: 1.6;
    margin-top: 15px;
}
         .features-area.style-two .image-box:hover{-webkit-transform:translateY(-10px);transform:translateY(-10px);-webkit-box-shadow:0 1rem 3rem rgba(31, 45, 61, .125)!important;box-shadow:0 1rem 3rem rgba(31, 45, 61, .125)!important;}
         @media only screen and (min-width: 768px) and (max-width: 991px){
         .ptb_100{padding:70px 0;}
         }
         @media (max-width: 991px){
         .res-margin{margin-bottom:45px;}
         }
         @media (max-width: 767px){
         .ptb_100{padding:50px 0;}
         .res-margin{margin-bottom:30px;}
         }
         @media (max-width: 991px){
         h2{font-size:36px;}
         .section-heading{margin-bottom:50px;}
         }
         @media (max-width: 767px){
         h2{font-size:30px;line-height:1.4;}
         }
         @media (max-width: 575px){
         h2{font-size:24px;line-height:1.4;}
         h3{font-size:18px;line-height:1.4;}
         p{line-height:26px;}
         .section-heading > h2{line-height:1.5;}
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes fadeInLeft{0%{-webkit-transform:translate3d(-100%,0,0);opacity:0;transform:translate3d(-100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInLeft{0%{-webkit-transform:translate3d(-100%,0,0);opacity:0;transform:translate3d(-100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @-webkit-keyframes fadeInRight{0%{-webkit-transform:translate3d(100%,0,0);opacity:0;transform:translate3d(100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInRight{0%{-webkit-transform:translate3d(100%,0,0);opacity:0;transform:translate3d(100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @-webkit-keyframes fadeInUp{0%{-webkit-transform:translate3d(0,100%,0);opacity:0;transform:translate3d(0,100%,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInUp{0%{-webkit-transform:translate3d(0,100%,0);opacity:0;transform:translate3d(0,100%,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         .image-box {
         margin-top: 25px;
         }
         /*  */
         pb-70{padding-bottom:70px;}
         .section-title{text-align:center;margin-bottom:40px;}
         .section-title h2{font-size:50px;margin:0 0 0 0;position:relative;line-height:1;}
         .section-title p{max-width:750px;margin:auto;line-height:1.8;}
         .section-title .bar{height:4px;width:85px;background:#7C4CEE;margin:20px auto 20px;position:relative;border-radius:5px;overflow:hidden;}
         .section-title .bar::before{content:'';position:absolute;left:0;top:0;height:100%;width:5px;background:#ffffff;-webkit-animation-duration:2s;animation-duration:2s;-webkit-animation-timing-function:linear;animation-timing-function:linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-name:MOVE-BG;animation-name:MOVE-BG;}
         .default-shape .shape-1{position:absolute;right:2%;bottom:5%;z-index:-1;-webkit-animation:moveBounce 10s linear infinite;animation:moveBounce 10s linear infinite;}
         .default-shape .shape-2{position:absolute;left:10%;top:16%;z-index:-1;}
         .default-shape .shape-2 img{-webkit-animation-name:rotateMe;animation-name:rotateMe;-webkit-animation-duration:10s;animation-duration:10s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-timing-function:linear;animation-timing-function:linear;}
         .default-shape .shape-3{position:absolute;left:35%;bottom:15%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .default-shape .shape-4{position:absolute;right:10%;top:10%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .default-shape .shape-5{position:absolute;left:50%;top:10%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .features-area{position:relative;z-index:1;overflow:hidden;}
         .single-features {
    margin-bottom: 30px;
    position: relative;
    z-index: 1;
    text-align: center;
    padding: 30px 15px 0;
    background-color: #ffffff;
    -webkit-box-shadow: 0 0 1.25rem rgba(108, 118, 134, 0.1);
    box-shadow: 0 0 1.25rem rgba(108, 118, 134, 0.1);
    -webkit-transition: 0.5s;
    transition: 0.5s;
    border-radius: 5px;
    min-height: 335px;
}
         .single-features .icon i{display:inline-block;height:60px;width:60px;line-height:60px;background-color:#7C4CEE;color:#ffffff;font-size:25px;border-radius:50px;-webkit-transition:0.5s;transition:0.5s;}
         .single-features h3{font-size:20px;margin-top:25px;margin-bottom:12px;-webkit-transition:0.5s;transition:0.5s;}
        .single-features p {
    margin-bottom: 0;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    font-size: 14px;
    line-height: 1.6;
}
         .single-features::before{top:50%;left:50%;width:0%;z-index:-1;height:100%;content:"";position:absolute;-webkit-transition:all 0.3s ease-in-out;transition:all 0.3s ease-in-out;background-color:#7C4CEE;-webkit-transform:translate(-50%, -50%);transform:translate(-50%, -50%);border-radius:5px;}
         .single-features:hover{-webkit-transform:translateY(-10px);transform:translateY(-10px);}
         .single-features:hover::before{width:100%;}
         .single-features:hover .icon i{background-color:#ffffff;color:#7C4CEE;}
         .single-features:hover h3{color:#ffffff;}
         .single-features:hover p{color:#ffffff;}
         @media only screen and (max-width: 767px){
         .pb-70{padding-bottom:20px;}
         .section-title{margin-bottom:28px;}
         .section-title h2{font-size:30px;}
         .default-shape .shape-1{display:none;}
         .default-shape .shape-2{display:none;}
         .default-shape .shape-3{display:none;}
         .default-shape .shape-4{display:none;}
         .default-shape .shape-5{display:none;}
         .single-features h3{font-size:22px;}
         }
         @media only screen and (min-width: 768px) and (max-width: 991px){
         .pb-70{padding-bottom:50px;}
         .section-title{margin-bottom:28px;}
         .section-title h2{font-size:40px;}
         .single-features h3{font-size:25px;}
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes MOVE-BG{from{-webkit-transform:translateX(0);}to{-webkit-transform:translateX(85px);}}
         @keyframes MOVE-BG{from{-webkit-transform:translateX(0);transform:translateX(0);}to{-webkit-transform:translateX(85px);transform:translateX(85px);}}
         @-webkit-keyframes moveBounce{0%{-webkit-transform:translateY(0px);transform:translateY(0px);}50%{-webkit-transform:translateY(20px);transform:translateY(20px);}100%{-webkit-transform:translateY(0px);transform:translateY(0px);}}
         @keyframes moveBounce{0%{-webkit-transform:translateY(0px);transform:translateY(0px);}50%{-webkit-transform:translateY(20px);transform:translateY(20px);}100%{-webkit-transform:translateY(0px);transform:translateY(0px);}}
         @-webkit-keyframes rotateMe{from{-webkit-transform:rotate(0deg);transform:rotate(0deg);}to{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
         @keyframes rotateMe{from{-webkit-transform:rotate(0deg);transform:rotate(0deg);}to{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
         @-webkit-keyframes animationFramesOne{0%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}20%{-webkit-transform:translate(73px, -1px) rotate(36deg);transform:translate(73px, -1px) rotate(36deg);}40%{-webkit-transform:translate(141px, 72px) rotate(72deg);transform:translate(141px, 72px) rotate(72deg);}60%{-webkit-transform:translate(83px, 122px) rotate(108deg);transform:translate(83px, 122px) rotate(108deg);}80%{-webkit-transform:translate(-40px, 72px) rotate(144deg);transform:translate(-40px, 72px) rotate(144deg);}100%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}}
         @keyframes animationFramesOne{0%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}20%{-webkit-transform:translate(73px, -1px) rotate(36deg);transform:translate(73px, -1px) rotate(36deg);}40%{-webkit-transform:translate(141px, 72px) rotate(72deg);transform:translate(141px, 72px) rotate(72deg);}60%{-webkit-transform:translate(83px, 122px) rotate(108deg);transform:translate(83px, 122px) rotate(108deg);}80%{-webkit-transform:translate(-40px, 72px) rotate(144deg);transform:translate(-40px, 72px) rotate(144deg);}100%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}}
         .service-text p {
         text-align: justify;
         }
         .service-thumb img {
         width: 100%;
         }
         div#features {
         padding-top: 100px;
         background: #f5f5f5;
         }
.ui-tabs .tab-pane .sub-heading {
    font-weight: 600;
    margin-bottom: 2rem;
    font-size: 17px;
    background-color: #7C4CEE;
    color: #fff;
    padding: 20px;
    text-align: center;
    line-height: 1.7;
    border-radius: 5px;
    max-width: 450px;
    margin: 0 auto;
}
         /*  */
         .banner-btn:hover {
         background: #fff!important;
         color: #782fe5!important;
         }
         .step-number.ui-gradient-green, .step-number.ui-gradient-green::before {
         background: -moz-linear-gradient(45deg, #19d9b4 0%, #92d275 100%);
         background: linear-gradient(45deg, #5129de 0%, #7e30e6 100%);
         }
         .banner-btn{padding:12px 13px!important;}
         .cta {
         background: linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);
         padding: 120px 0;
         margin-bottom: 50px;
         }
         .cta p{font-family:'Open Sans',sans-serif!important;font-size:22px!important;font-weight:700;letter-spacing:normal!important;text-align:justify;margin-bottom:42px;color:#fff;}
         .app_image{position:relative;}
         .app_image .shadow_bottom{content:'';position:absolute;width:80%;height:5px;border-radius:50%/5px;left:10%;bottom:0;-webkit-box-shadow:0 35px 33px 0 rgba(0,0,0,.8);box-shadow:0 35px 33px 0 rgba(0,0,0,.8);-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_two{position:absolute;right:0;top:-130px;width:55%; z-index: 1;}
         .app_image .image_two img{-webkit-animation-name:jump;animation-name:jump;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-duration:3.8s;animation-duration:3.8s;-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_two .shadow_bottom{-webkit-animation:shadow 3.8s infinite linear;animation:shadow 3.8s infinite linear;bottom:10px;}
         .app_image .image_first{position:absolute;left:60px;top:-82px;width:45%;}
         .app_image .image_first img{-webkit-animation-name:jump;animation-name:jump;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-duration:2.8s;animation-duration:2.8s;-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_first .shadow_bottom{-webkit-animation:shadow 2.8s infinite linear;animation:shadow 2.8s infinite linear;}
         @media only screen and (min-width:320px) and (max-width:767px){
         .app_image{display:none;}
         .banner-btn{width:100%;margin:10px 0!important;}
         .cta{padding-top:50px;margin-bottom:0;padding-bottom:20px;}
         .cta p{font-size:18px!important;}
         }
         @media only screen and (min-width:768px) and (max-width:899px){
         .cta{padding:50px;margin-bottom:0;}
         .cta .image_first,.cta .image_two{display:none;}
         }
         .container:after,.container:before,.row:after,.row:before{display:table;content:" ";}
         .container:after,.row:after{clear:both;}
         *:focus,*,*:active{outline:none!important;outline:0!important;}
         a,.btn{-webkit-transition:all .3s ease-out 0s;-moz-transition:all .3s ease-out 0s;-ms-transition:all .3s ease-out 0s;-o-transition:all .3s ease-out 0s;transition:all .3s ease-out 0s;}
         a:hover{text-decoration:none;}
         a:focus,a:hover{color:#101010;}
         a{outline:medium!important;color:#727272;}
         .uppercase{text-transform:uppercase;}
         .btn{padding:10px 36px;margin:10px;box-shadow:none;border-radius:0;}
         .btn{border-radius:0;box-shadow:none;display:inline-block;margin:5px;padding:15px 35px;}
         @media only screen and (max-width:991px){
         .container{width:95%;}
         }
         a:hover{text-decoration:none;}
         a:focus{text-decoration:none;}
         img{max-width:100%!important;}
         .banner-btn{margin-right:15px!important;}
         .banner-btn:last-child{margin-right:0!important;}
         .container:before,.container:after{content:none;}
         .btn{margin:0;}
         .btn:hover{opacity:.8;}
         @media screen and (max-width:767px){
            .actions .btn {
    margin-right: 0;
    margin-bottom: 16px;
    max-width: 230px;
    width: 100%;
}
         .banner-btn{margin-right:0!important;margin-bottom:15px;display:block;}
         .banner-btn:last-child{margin-bottom:0;}
         .features-area.style-two .image-box {
    min-height: auto; padding-bottom: 30px !important; }
         }
         .banner-btn{background:#fff;padding:12px 20px;border-radius:2px;display:-webkit-inline-box;}
         .banner-btn:focus{background:#fff;}
         .banner-btn:hover{background:#fff;z-index:100;color:#3685eb;}
         .anim_btn {
         background: #4500b7;
         background: linear-gradient(135deg,#a27fe6 0%,#662de2 50%,#5b2be0 51%,#4e2ade 71%,#562adf 100%);
         filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4500b7',endColorstr='#4500b7',GradientType=1 );
         background-size: 400% 400%;
         border: medium;
         color: #fff!important;
         font-weight: 700;
         padding: 9px 40px;
         display: inline-block;
         font-size: 17px;
         border-radius: 4px;
         margin-top: 20px;
         text-shadow: none;
         
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes jump{0%{-webkit-transform:translateY(0);transform:translateY(0);}50%{-webkit-transform:translateY(-30px);transform:translateY(-30px);-webkit-transition:-webkit-transform 1.4s .6s linear;transition:-webkit-transform 1.4s .6s linear;-o-transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear,-webkit-transform 1.4s .6s linear;}100%{-webkit-transform:translateY(0);transform:translateY(0);}}
         @keyframes jump{0%{-webkit-transform:translateY(0);transform:translateY(0);}50%{-webkit-transform:translateY(-30px);transform:translateY(-30px);-webkit-transition:-webkit-transform 1.4s .6s linear;transition:-webkit-transform 1.4s .6s linear;-o-transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear,-webkit-transform 1.4s .6s linear;}100%{-webkit-transform:translateY(0);transform:translateY(0);}}
         @-webkit-keyframes shadow{0%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}50%{-webkit-box-shadow:0 35px 35px -2px #000;box-shadow:0 35px 35px -2px #000;width:60%;left:20%;}100%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}}
         @keyframes shadow{0%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}50%{-webkit-box-shadow:0 35px 35px -2px #000;box-shadow:0 35px 35px -2px #000;width:60%;left:20%;}100%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}}
         .ui-gradient-blue {
         background: -moz-linear-gradient(45deg, #7C4CEE 0%, #54ceff 100%);
         background: linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);
         }
         a.btn.ui-gradient-blue.shadow-xl {
         background: #ffffff;
         color: #6e2de3;
         }
         a.btn.ui-gradient-green.shadow-xl {
         background: #fff;
         color: #6e2ee4;
         }
         section.section.service-area.overflow-hidden.ptb_100 {
         background: #f5f5f5;
         }
         .ui-app-icon {
         display: inline-block;
         width: 100px;
         height: 100px;
         margin: 0 auto 2rem auto;
         border-radius: 1.25rem;
         overflow: hidden;
         }
         .ui-app-icon.shadow-lg {
         background: #f1f1f1;
         }
         .btn-download span:nth-child(1) {
         font-size: 70%;
         margin-left: 20px;
         }
         .btn-download span {
         display: block;
         margin-left: 30px;
         }
         .icons svg{
         width: 16px;
         height: 16px;
         fill:#59687c;
         position: relative;
         top: 2px;
         margin-right: 3px;
         }
         .ui-tabs .nav-tabs li a.active svg {
         width: 16px;
         height: 16px;
         fill: #fff;
         position: relative;
         top: 2px;
         margin-right: 3px;
         }
         .featured-img svg {
         width: 48px;
         height: 48px;
         fill: #6d2ee3;
         }
         .icon svg {
         width: 30px;
         height: 30px;
         fill: #fff;
         position: relative;
         top: 4px;
         }
         .single-features:hover .icon svg {
         width: 30px;
         height: 30px;
         fill: #7C4CEE;
         position: relative;
         top: 4px;
         }
         /* New feature css */
         
.streaming-sec-padding{padding:50px 0px;background:#f5f5f5;}
.tiktok-heading-main{text-align:center;}
.tiktok-heading{color:#fb0746;font-weight:600;font-size:35px;margin-bottom:15px;}
.tiktok-heading > span{color:#010101;text-shadow:-2px -2px 2px #69c9d0, 2px 2px 2px rgba(238, 29, 82, 0.97);}
@media (max-width: 767px){
.tiktok-heading{font-size:28px;}
.streaming-sec-padding{padding:20px 0px;}
}
/*! CSS Used from: Embedded */
.inner-streaming-appflow{padding-top:30px;}
.streaming-appflow-single-left{display:flex;align-items:flex-start;justify-content:space-between;margin-bottom:45px;}
.streaming-appflow-single-leftimg{width:40%;text-align:center;}
.streaming-appflow-single-leftimg img{width:100%;}
.streaming-appflow-single-lefttext{width:50%;}
.streaming-appflow-single-lefttext-data{padding:15px 15px 15px 20px;background:linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);position:relative;border-radius:10px;z-index:2;}
.streaming-appflow-single-lefttext-data:before{content:'';position:absolute;border-right:40px solid #5a2be0;border-top:40px solid transparent;border-bottom:40px solid transparent;top:50%;left:-35px;transform:translate(0,-50%);z-index:1;}
.streaming-appflow-single-lefttext-data>h4{color:#ffff;font-size:19px;}
.streaming-appflow-single-lefttext-data>p{color:#ffff;font-size:15px;}
.streaming-appflow-single-right{display:flex;align-items:flex-end;justify-content:space-between;margin-bottom:45px;}
.streaming-appflow-single-rightimg{width:40%;text-align:center;}
.streaming-appflow-single-rightimg img{width:100%;}
.streaming-appflow-single-righttext{width:50%;}
.streaming-appflow-single-righttext-data{padding:15px 15px 15px 15px;background:linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);position:relative;border-radius:10px;z-index:2;}
.streaming-appflow-single-righttext-data:before{content:'';position:absolute;border-left:40px solid #8030e6;border-top:40px solid transparent;border-bottom:40px solid transparent;top:50%;right:-35px;transform:translate(0,-50%);z-index:1;}
.streaming-appflow-single-righttext-data>h4{color:#ffff;font-size:19px;}
.streaming-appflow-single-righttext-data>p{color:#ffff;font-size:15px;}
@media (max-width: 767px){
.streaming-appflow-single-left{display:flex;flex-wrap:wrap-reverse;}
.streaming-appflow-single-lefttext{width:100%;margin-bottom:50px;}
.streaming-appflow-single-leftimg{width:100%;}
.streaming-appflow-single-leftimg img{width:70%;}
.streaming-appflow-single-lefttext-data:before{border-right:40px solid transparent;border-top:40px solid #8030e6;border-left:40px solid transparent;top:auto;left:50%;bottom:-70px;transform:translate(-50%,0px);}
.streaming-appflow-single-right{display:flex;flex-wrap:wrap;}
.streaming-appflow-single-righttext{width:100%;margin-bottom:50px;}
.streaming-appflow-single-rightimg{width:100%;}
.streaming-appflow-single-rightimg img{width:70%;}
.streaming-appflow-single-righttext-data:before{border-right:40px solid transparent;border-top:40px solid #8030e6;border-left:40px solid transparent;top:auto;right:auto;left:50%;bottom:-70px;transform:translate(-50%,0px);}
}
/* end */
      </style>
   </head>
   <body class="ui-transparent-nav" data-fade_in="on-load">
      <!-- Navbar Fixed + Default -->
      <nav class="navbar navbar-fixed-top transparent navbar-light bg-white">
         <div class="container">
            <!-- Navbar Logo -->
            <a class="ui-variable-logo navbar-brand" href="./" title="TikTok Clone">
               <!-- Default Logo -->
               <img class="logo-default" src="assets/img/logo/logo.png" alt="TikTok Clone" data-uhd>
               <!-- Transparent Logo -->
               <img class="logo-transparent" src="assets/img/logo/logo-transparent.png" alt="TikTok Clone" data-uhd>
            </a>
            <!-- .navbar-brand -->
            <!-- Navbar Navigation -->
            <div class="ui-navigation navbar-right">
               <ul class="nav navbar-nav">
                  <!-- Nav Item -->
                  <li>
                     <a href="#" data-scrollto="about">About</a>
                  </li>
                  <li>
                     <a href="#" data-scrollto="feature">Features</a>
                  </li>
                  <!-- Nav Item -->
                  
                  <!-- Nav Item -->
                  <li>
                     <a href="#" data-scrollto="screens">App Screens</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="#" data-scrollto="whychoose">Why Choose</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="#" data-scrollto="faq">FAQ</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="#" data-scrollto="testimonials">Reviews</a>
                  </li>
               </ul>
               <!--.navbar-nav -->
            </div>
            <!--.ui-navigation -->
            <!-- Navbar Button -->
            <a href="#" data-scrollto="contact" class="btn btn-sm ui-gradient-green pull-right">Request Proposal</a>
            <!-- Navbar Toggle -->
            <a href="#" class="ui-mobile-nav-toggle pull-right"></a>
         </div>
         <!-- .container -->
      </nav>
      <!-- nav -->
      <!-- Main Wrapper -->
      <div class="main" role="main">
         <!-- Hero Waves Center -->
         <div class="ui-hero hero-lg hero-center ui-gradient-blue ui-waves hero-svg-layer-3">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <h1 class="heading">
                        Launch Your Own Robust TikTok Clone
                     </h1>
                     <p class="paragraph">
                        At Tiktokcloneapp.com, we strive to build a best-in-class social video-sharing platform like TikTok with cutting-edge technology.
                     </p>
                     <div class="actions">
                        <a class="btn ui-gradient-blue shadow-xl" data-scrollto="contact">Request Demo</a>
                        <a class="btn ui-gradient-green shadow-xl" data-scrollto="contact">Get Free Quote</a>
                     </div>
                  </div>
                  <div class="col-sm-12 text-center">
                     <img src="assets/img/app-screens/banner_screen.png" alt="" width="640" class="responsive-on-md" />
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .ui-hero -->
         <!--  -->
         <section class="section service-area overflow-hidden" id="about">
            <div class="container">
               <div class="row justify-content-between">
                  <div class="col-12 col-lg-6 order-1 order-lg-1">
                     <!-- Service Text -->
                     <div class="service-text pt-3">
                        <h2 class="text-capitalize mb-4">Tiktok Clone - Launch A Social Video-sharing App Like TikTok Instantly!</h2>
                        <!-- Service List -->
                        <!-- Single Service -->
                        <div class="service-text media-body">
                           <p>With the emergence of several social media apps, users are inspired to showcase their talents in various ways. TikTok is one such social video-sharing application that has gained the best interests of millions of users across the globe. Users find it exciting to entertain their followers with captivating videos lasting for minutes or even less.</p>
                        </div>
                        </li>
                        <!-- Single Service -->
                        <div class="service-text media-body">
                           <p>Businesspersons wish to launch their own apps like TikTok to earn a massive user base in the quickest turnaround time. However, building an app from scratch demands a lot of time and effort. To eliminate this, our seasoned experts have developed white-label TikTok clones that are reasonably priced, helping you become an overnight sensation in your industry. Connect with us to discuss further details! </p>
                        </div>
                        </li>
                        </ul>
                        <a href="#" data-scrollto="contact" class="btn btn-bordered mt-4">Connect With Us</a>
                     </div>
                  </div>
                  <div class="col-12 col-lg-6 order-2 order-lg-2 text-center d-lg-block">
                     <!-- Service Thumb -->
                     <div class="mx-auto mt-4">
                        <img src="assets/img/app-screens/double_1.png" width="410" alt=""> 
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--  -->
         <!-- New feature start -->
         <div class="streaming-appflow streaming-sec-padding">
   <div class="container">
   <div class="section-heading center">
                  <h2 class="heading text-indigo">
                     Highlight Features Of Our White-Label TikTok App Clone
                  </h2>
               </div>
      <div class="inner-streaming-appflow">
         <div class="row">
            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                     <img src="assets/img/app-screens/1-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Interactive loading screen</h4>
                        <p>Users are presented with a captivating loading screen as they enter the app.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>User feed</h4>
                         <p>Users can view the videos shared by people they are following on the feed section of the app.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                     <img src="assets/img/app-screens/2-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/3-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Swiping</h4>
                        <p>Users can swipe the videos watched to view the videos shared by the next user.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Like videos</h4>
                         <p>Users can double-tap the videos of other users to like them if they find it engaging and unique.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                     <img src="assets/img/app-screens/4-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/5-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Comment videos</h4>
                        <p>Users can watch videos shared by other users. They can also comment on the post they like.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Share videos</h4>
                         <p>Users can share their own videos with their friends using the share option included in the app. They can also share the videos of other users that they like.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/6-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/7-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>View followed profile</h4>
                        <p> Users can view the profiles of video creators they are already following.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>View profiles</h4>
                         <p>Users can view profiles of other users and follow them if they find their videos entertaining. This way, they will get to see the videos shared by users they follow.
                          </p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/8-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/9-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Explore videos</h4>
                        <p>Users can search for videos that are trending or unique. They can explore them using hashtags, profile names, locations, and other search options.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Profile set up</h4>
                         <p>Users can create their own profiles and manage them. Here, they can add their details such as names, contact details, and more.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/10-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/10-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Edit profiles</h4>
                        <p>Users can edit the details in their profiles, including their usernames, contact details, etc.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>View followers</h4>
                         <p>Users can view the people that follow them in this section. They can also follow them back directly from here.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/11-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/12-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>View people followed</h4>
                        <p>Users can view the list of people that they follow in this section.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Create videos</h4>
                         <p>Users can create their own videos where they lip-sync to audios/music, dance, share expressions, and more as per their skills. </p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/13-screen.png">
                  </div>
               </div>
            </div>

            <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                  <img src="assets/img/app-screens/14-screen.png">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Edit videos with filters</h4>
                        <p>Users can improve the visual effects of their music dubbed videos by adding filters. They can share these videos with special filters to their profiles, presenting them in exciting ways.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Post videos</h4>
                         <p>Users can share their videos on their profiles by adding a relevant description, appropriate hashtags, locations, etc. They can tag other users as well.</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                  <img src="assets/img/app-screens/14-screen.png">
                  </div>
               </div>
            </div>

            <!-- <div class="col-md-6">
               <div class="streaming-appflow-single-left">
                  <div class="streaming-appflow-single-leftimg">
                     <img src="https://www.omninos.in/assets/streaming-images/streaming-screens/17.jpg">
                  </div>
                  <div class="streaming-appflow-single-lefttext">
                     <div class="streaming-appflow-single-lefttext-data">
                        <h4>Channel</h4>
                        <p>User can create his personalised channel and can upload
                          videos on it.</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="streaming-appflow-single-right">
                  <div class="streaming-appflow-single-righttext">
                     <div class="streaming-appflow-single-righttext-data">
                        <h4>Music</h4>
                         <p> Let them dub their own voice for uploaded audio tracks &amp; record the same
                            with their phones</p>
                     </div>
                  </div>
                  <div class="streaming-appflow-single-rightimg">
                     <img src="https://www.omninos.in/assets/streaming-images/streaming-screens/18.jpg">
                  </div>
               </div>
            </div> -->
           
            
         </div>
      </div>
   </div>
</div>

         <div class="cta">
            <div class="container">
               <div class="row">
                  <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                     <h3>Inspire Your Users To Express Themselves With Your Tailor-made TikTok Clone
                     </h3>
                     <div class="banner-btn-sec">
                        <a data-scrollto="contact" class="btn banner-btn uppercase banner-btn js-scroll-trigger anim_btn" href="#">Connect With Us</a>
                     </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 app_image">
                     <div class="image_first">
                        <img src="assets/img/app-screens/2-screen.png">
                        <div class="shadow_bottom"></div>
                     </div>
                     <div class="image_two">
                        <img src="assets/img/app-screens/14-screen.png">
                        <div class="shadow_bottom"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--  -->
         <!-- Process Section -->
         <div id="steps" class="section">
            <div class="container">
               <!-- Section Heading -->
               <div class="section-heading center">
                  <h2 class="heading text-indigo">
                     How Does Our TikTok Clone Roll Out?
                  </h2>
               </div>
               <!-- .section-heading -->
               <!-- UI Steps -->
               <div class="ui-showcase-blocks ui-steps">
                  <!-- Step 1 -->
                  <div class="step-wrapper">
                     <span class="step-number ui-gradient-green">1</span>
                     <div class="row">
                        <div class="col-md-6" data-vertical_center="true">
                           <h4 class="heading text-dark-gray">
                              Step 1
                           </h4>
                           <p class="paragraph">
                              Users can register with the app and create their own user profiles by providing all necessary details.
                           </p>
                        </div>
                        <div class="col-md-6 text-center">
                           <img class="responsive-on-xs" src="assets/img/app-screens/1-screen.png" width="220" data-uhd alt="TikTok Clone" />
                        </div>
                     </div>
                  </div>
                  <!-- Step 2 -->
                  <div class="step-wrapper">
                     <span class="step-number ui-gradient-green">2</span>
                     <div class="row">
                        <div class="col-md-6" data-vertical_center="true">
                           <h4 class="heading text-dark-gray">
                              Step 2
                           </h4>
                           <p class="paragraph">
                              Users can create their music-dubbed videos and share it to their profiles, with relevant hashtags and a short description. They can also share it on other social media platforms.
                           </p>
                        </div>
                        <div class="col-md-6 text-center">
                           <img class="responsive-on-xs" src="assets/img/app-screens/13-screen.png" width="220" data-uhd alt="TikTok Clone" />
                        </div>
                     </div>
                  </div>
                  <!-- Step 2 -->
                  <div class="step-wrapper">
                     <span class="step-number ui-gradient-green">3</span>
                     <div class="row">
                        <div class="col-md-6" data-vertical_center="true">
                           <h4 class="heading text-dark-gray">
                              Step 3
                           </h4>
                           <p class="paragraph">
                              Users can browse for the content of other users. They can view, like, and comment on the videos they find interesting. They can also share these videos with their friends. 
                           </p>
                        </div>
                        <div class="col-md-6 text-center">
                           <img class="responsive-on-xs" src="assets/img/app-screens/7-screen.png" width="220" data-uhd alt="TikTok Clone" />
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!--  -->
         <section class="section service-area overflow-hidden ptb_100">
            <div class="container">
               <div class="row justify-content-between align-items-center">
                  <div class="col-12 col-lg-4  d-none d-lg-block">
                     <!-- Service Thumb -->
                     <div class="service-thumb mx-auto">
                        <img src="assets/img/app-screens/double_2.png" alt="">
                     </div>
                  </div>
                  <div class="col-12 col-lg-6">
                     <!-- Service Text -->
                     <div class="service-text pt-4 pt-lg-0">
                        <h2 class="text-capitalize mb-4">TikTok Clone With Pre-packed Features For A Remarkable Experience</h2>
                        <p>At Tiktokcloneapp.com, we offer top-notch TikTok clone app solutions pre-loaded with all notable features needed for its seamless functionality. With such a fantastic app on board, your business can readily reach out to your customers without any hassle. The integrated features offer an astonishing user-friendly experience. Learn about the vital features below that spice up the app experience of your users.</p>
                        <!-- Check List -->
                        <ul class="check-list">
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Manage profile and privacy settings</span>
                              </div>
                           </li>
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Share videos on other social media platforms</span>
                              </div>
                           </li>
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Edit videos with special filters</span>
                              </div>
                           </li>
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Browse trending content</span>
                              </div>
                           </li>
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Instant alerts via push notifications</span>
                              </div>
                           </li>
                           <li class="py-1">
                              <!-- List Box -->
                              <div class="list-box media">
                                 <span class="media-body pl-4">Like/Comment/Share videos</span>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--  -->
         <!--  -->
         <section id="feature" class="section features-area style-two overflow-hidden ptb_100">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-12 col-md-10 col-lg-8">
                     <!-- Section Heading -->
                     <div class="section-heading text-center">
                        <span class="d-inline-block rounded-pill shadow-sm fw-5 px-4 py-2 mb-3">
                           <svg class="svg-inline--fa fa-lightbulb fa-w-11 text-primary mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="lightbulb" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg="">
                              <path fill="currentColor" d="M176 80c-52.94 0-96 43.06-96 96 0 8.84 7.16 16 16 16s16-7.16 16-16c0-35.3 28.72-64 64-64 8.84 0 16-7.16 16-16s-7.16-16-16-16zM96.06 459.17c0 3.15.93 6.22 2.68 8.84l24.51 36.84c2.97 4.46 7.97 7.14 13.32 7.14h78.85c5.36 0 10.36-2.68 13.32-7.14l24.51-36.84c1.74-2.62 2.67-5.7 2.68-8.84l.05-43.18H96.02l.04 43.18zM176 0C73.72 0 0 82.97 0 176c0 44.37 16.45 84.85 43.56 115.78 16.64 18.99 42.74 58.8 52.42 92.16v.06h48v-.12c-.01-4.77-.72-9.51-2.15-14.07-5.59-17.81-22.82-64.77-62.17-109.67-20.54-23.43-31.52-53.15-31.61-84.14-.2-73.64 59.67-128 127.95-128 70.58 0 128 57.42 128 128 0 30.97-11.24 60.85-31.65 84.14-39.11 44.61-56.42 91.47-62.1 109.46a47.507 47.507 0 0 0-2.22 14.3v.1h48v-.05c9.68-33.37 35.78-73.18 52.42-92.16C335.55 260.85 352 220.37 352 176 352 78.8 273.2 0 176 0z"></path>
                           </svg>
                           <!-- <i class="far fa-lightbulb text-primary mr-1"></i> -->
                           <span class="text-primary">Features</span>
                        </span>
                        <h2>Features Integrated Into Our TikTok Clone App</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                              <g transform="translate(0,-952.36218)">
                                 <path style="text-indent:0;text-transform:none;direction:ltr;block-progression:tb;baseline-shift:baseline;color:#000000;enable-background:accumulate;" d="m 82,957.31902 c -4.934108,0 -9,4.06784 -9,9.00432 l 0,64.03066 a 3.0003,3.0017394 0 0 0 0.25,1.1568 l 6,14.0068 a 3.0003,3.0017394 0 0 0 5.5,0 l 6,-14.0068 A 3.0003,3.0017394 0 0 0 91,1030.354 l 0,-64.03066 c 0,-4.93648 -4.065892,-9.00432 -9,-9.00432 z m -70,4.00192 a 3.0003,3.0017394 0 0 0 -3,3.00144 l 0,76.03642 a 3.0003,3.0017394 0 0 0 3,3.0015 l 54,0 a 3.0003,3.0017394 0 0 0 3,-3.0015 l 0,-76.03642 a 3.0003,3.0017394 0 0 0 -3,-3.00144 l -54,0 z m 70,2.00096 c 1.713892,0 3,1.28673 3,3.00144 l 0,61.02926 -6,0 0,-61.02926 c 0,-1.71471 1.286108,-3.00144 3,-3.00144 z m -67,4.00192 48,0 0,70.03358 -48,0 0,-70.03358 z m 9.6875,8.00384 A 3.0040663,3.0055076 0 1 0 25,981.33054 l 12,0 a 3.0003,3.0017394 0 1 0 0,-6.00288 l -12,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 20,0 A 3.0040663,3.0055076 0 1 0 45,981.33054 l 8,0 a 3.0003,3.0017394 0 1 0 0,-6.00288 l -8,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m -20,10.0048 A 3.0040663,3.0055076 0 1 0 25,991.33533 l 12,0 a 3.0003,3.0017394 0 1 0 0,-6.00287 l -12,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 20,0 A 3.0040663,3.0055076 0 1 0 45,991.33533 l 8,0 a 3.0003,3.0017394 0 1 0 0,-6.00287 l -8,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m -20,14.00671 A 3.0040663,3.0055076 0 1 0 25,1005.3421 l 28,0 a 3.0003,3.0017394 0 1 0 0,-6.00293 l -28,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 0,14.00673 A 3.0040663,3.0055076 0 1 0 25,1019.3488 l 12,0 a 3.0003,3.0017394 0 1 0 0,-6.0029 l -12,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 20,0 A 3.0040663,3.0055076 0 1 0 45,1019.3488 l 8,0 a 3.0003,3.0017394 0 1 0 0,-6.0029 l -8,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m -20,10.0048 A 3.0040663,3.0055076 0 1 0 25,1029.3536 l 12,0 a 3.0003,3.0017394 0 1 0 0,-6.0029 l -12,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 20,0 A 3.0040663,3.0055076 0 1 0 45,1029.3536 l 8,0 a 3.0003,3.0017394 0 1 0 0,-6.0029 l -8,0 a 3.0003,3.0017394 0 0 0 -0.3125,0 z m 35.84375,10.0048 2.9375,0 -1.46875,3.4079 -1.46875,-3.4079 z"  fill-opacity="1" stroke="none" marker="none" visibility="visible" display="inline" overflow="visible"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Registration</h3>
                           <p>Users can sign up with the app using their phone numbers, email IDs, or social media handles like Facebook. The registered credentials are used for logging in.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                              <switch>
                                 <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                 <g i:extraneous="self">
                                    <path  d="M94.435,45.645c-0.023-0.02-0.046-0.04-0.069-0.06c-0.089-0.09-0.183-0.175-0.289-0.25    c-14.21-12.27-28.422-24.539-42.634-36.808c-1.09-0.941-2.445-0.941-3.535,0C33.577,20.9,19.246,33.272,4.916,45.645    c-1.699,1.467-0.296,4.268,1.768,4.268c2.894,0,5.787,0,8.681,0c0,13.237,0,26.473,0,39.71c0,1.363,1.137,2.5,2.5,2.5    c8.148,0,16.296,0,24.444,0c1.363,0,2.5-1.137,2.5-2.5c0-7.314,0-14.629,0-21.943c3.765,0,7.53,0,11.295,0    c0,7.314,0,14.629,0,21.943c0,1.363,1.137,2.5,2.5,2.5c8.148,0,16.296,0,24.444,0c1.363,0,2.5-1.137,2.5-2.5    c0-13.237,0-26.474,0-39.71c2.373,0,4.746,0,7.119,0c1.067,0,1.773-0.551,2.134-1.284C95.426,47.754,95.501,46.565,94.435,45.645z     M85.956,44.913c-0.97,0-1.938,0-2.908,0c-1.363,0-2.5,1.137-2.5,2.5c0,13.237,0,26.473,0,39.71c-6.481,0-12.963,0-19.444,0    c0-7.314,0-14.629,0-21.943c0-1.363-1.137-2.5-2.5-2.5c-5.432,0-10.864,0-16.295,0c-1.363,0-2.5,1.137-2.5,2.5    c0,7.314,0,14.629,0,21.943c-6.481,0-12.962,0-19.444,0c0-13.237,0-26.474,0-39.71c0-1.363-1.137-2.5-2.5-2.5    c-1.49,0-2.98,0-4.47,0C25.488,34.472,37.582,24.03,49.675,13.589C61.769,24.03,73.862,34.472,85.956,44.913z"></path>
                                 </g>
                              </switch>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Home Section</h3>
                           <p>It is the section of the app that follows after the login page of the app. Here, all sections are listed to offer quick entry.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <switch>
                                 <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                 <g i:extraneous="self">
                                    <g>
                                       <path d="M5273.1,2400.1v-2c0-2.8-5-4-9.7-4s-9.7,1.3-9.7,4v2c0,1.8,0.7,3.6,2,4.9l5,4.9c0.3,0.3,0.4,0.6,0.4,1v6.4     c0,0.4,0.2,0.7,0.6,0.8l2.9,0.9c0.5,0.1,1-0.2,1-0.8v-7.2c0-0.4,0.2-0.7,0.4-1l5.1-5C5272.4,2403.7,5273.1,2401.9,5273.1,2400.1z      M5263.4,2400c-4.8,0-7.4-1.3-7.5-1.8v0c0.1-0.5,2.7-1.8,7.5-1.8c4.8,0,7.3,1.3,7.5,1.8C5270.7,2398.7,5268.2,2400,5263.4,2400z"></path>
                                       <path d="M5268.4,2410.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1c0-0.6-0.4-1-1-1H5268.4z"></path>
                                       <path d="M5272.7,2413.7h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2414.1,5273.3,2413.7,5272.7,2413.7z"></path>
                                       <path d="M5272.7,2417h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2417.5,5273.3,2417,5272.7,2417z"></path>
                                    </g>
                                    <g>
                                       <path d="M69.3,62.8l-3.4-4.5c0.5-1.3,0.7-1.6,1.2-2.9l5.6-0.8c0.4-0.1,0.7-0.4,0.7-0.8v-7.6c0-0.4-0.3-0.7-0.7-0.8l-5.6-0.8     c-0.5-1.3-0.7-1.6-1.2-2.9l3.4-4.5c0.2-0.3,0.2-0.8-0.1-1l-5.4-5.4c-0.3-0.3-0.7-0.3-1-0.1l-4.5,3.4c-1.3-0.5-1.6-0.7-2.9-1.2     l-0.8-5.6c-0.1-0.4-0.4-0.7-0.8-0.7h-7.6c-0.4,0-0.7,0.3-0.8,0.7l-0.8,5.6c-1.3,0.5-1.6,0.7-2.9,1.2l-4.5-3.4     c-0.3-0.2-0.8-0.2-1,0.1l-5.4,5.4c-0.3,0.3-0.3,0.7-0.1,1l3.4,4.5c-0.5,1.3-0.7,1.6-1.2,2.9l-5.6,0.8c-0.4,0.1-0.7,0.4-0.7,0.8     v7.6c0,0.4,0.3,0.7,0.7,0.8l5.6,0.8c0.5,1.3,0.7,1.6,1.2,2.9l-3.4,4.5c-0.2,0.3-0.2,0.8,0.1,1l5.4,5.4c0.3,0.3,0.7,0.3,1,0.1     l4.5-3.4c1.3,0.5,1.6,0.7,2.9,1.2l0.8,5.6c0.1,0.4,0.4,0.7,0.8,0.7h7.6c0.4,0,0.7-0.3,0.8-0.7l0.8-5.6c1.3-0.5,1.6-0.7,2.9-1.2     l4.5,3.4c0.3,0.2,0.8,0.2,1-0.1l5.4-5.4C69.5,63.6,69.5,63.2,69.3,62.8z M50,58.8c-4.8,0-8.8-3.9-8.8-8.8c0-4.8,3.9-8.8,8.8-8.8     c4.8,0,8.8,3.9,8.8,8.8C58.8,54.8,54.8,58.8,50,58.8z"></path>
                                       <path d="M45.7,11.2l2.4-2.4v11.4c0,1,0.8,1.8,1.8,1.8c1,0,1.8-0.8,1.8-1.8V8.7l2.4,2.4c0.7,0.7,1.9,0.7,2.6,0     c0.7-0.7,0.7-1.9,0-2.6L51.3,3c-0.7-0.7-1.9-0.7-2.6,0l-5.6,5.6c-0.4,0.4-0.5,0.8-0.5,1.3c0,0.5,0.2,0.9,0.5,1.3     C43.9,11.9,45,11.9,45.7,11.2z"></path>
                                       <path d="M17.7,27.4c1,0,1.8-0.8,1.8-1.8v-3.4l8,8c0.7,0.7,1.9,0.7,2.6,0s0.7-1.9,0-2.6l-8-8h3.4c1,0,1.8-0.8,1.8-1.8     c0-1-0.8-1.8-1.8-1.8h-7.9c-1,0-1.8,0.8-1.8,1.8v7.9c0,0.5,0.2,1,0.5,1.3C16.7,27.2,17.2,27.4,17.7,27.4z"></path>
                                       <path d="M8.7,51.8h11.4c1,0,1.8-0.8,1.8-1.8s-0.8-1.8-1.8-1.8H8.7l2.4-2.4c0.7-0.7,0.7-1.9,0-2.6c-0.7-0.7-1.9-0.7-2.6,0L3,48.7     c-0.7,0.7-0.7,1.9,0,2.6l5.6,5.6c0.4,0.4,0.8,0.5,1.3,0.5c0.5,0,0.9-0.2,1.3-0.5c0.7-0.7,0.7-1.9,0-2.6L8.7,51.8z"></path>
                                       <path d="M27.6,69.8l-8,8v-3.4c0-1-0.8-1.8-1.8-1.8c-1,0-1.8,0.8-1.8,1.8v7.9c0,1,0.8,1.8,1.8,1.8h7.9c0.5,0,1-0.2,1.3-0.5     c0.3-0.3,0.5-0.8,0.5-1.3c0-1-0.8-1.8-1.8-1.8h-3.4l8-8c0.7-0.7,0.7-1.9,0-2.6C29.4,69.1,28.3,69.1,27.6,69.8z"></path>
                                       <path d="M54.3,88.8l-2.4,2.4V79.9c0-1-0.8-1.8-1.8-1.8c-1,0-1.8,0.8-1.8,1.8v11.4l-2.4-2.4c-0.7-0.7-1.9-0.7-2.6,0     c-0.7,0.7-0.7,1.9,0,2.6l5.6,5.6c0.7,0.7,1.9,0.7,2.6,0l5.6-5.6c0.4-0.4,0.5-0.8,0.5-1.3c0-0.5-0.2-0.9-0.5-1.3     C56.1,88.1,55,88.1,54.3,88.8z"></path>
                                       <path d="M82.3,72.6c-1,0-1.8,0.8-1.8,1.8v3.4l-8-8c-0.7-0.7-1.9-0.7-2.6,0c-0.7,0.7-0.7,1.9,0,2.6l8,8h-3.4c-1,0-1.8,0.8-1.8,1.8     c0,1,0.8,1.8,1.8,1.8h7.9c1,0,1.8-0.8,1.8-1.8v-7.9c0-0.5-0.2-1-0.5-1.3C83.3,72.8,82.8,72.6,82.3,72.6z"></path>
                                       <path d="M97,48.7l-5.6-5.6c-0.4-0.4-0.8-0.5-1.3-0.5c-0.5,0-0.9,0.2-1.3,0.5c-0.7,0.7-0.7,1.9,0,2.6l2.4,2.4H79.9     c-1,0-1.8,0.8-1.8,1.8c0,1,0.8,1.8,1.8,1.8h11.4l-2.4,2.4c-0.7,0.7-0.7,1.9,0,2.6c0.7,0.7,1.9,0.7,2.6,0l5.6-5.6     C97.7,50.6,97.7,49.4,97,48.7z"></path>
                                       <path d="M72.4,30.2l8-8v3.4c0,1,0.8,1.8,1.8,1.8c1,0,1.8-0.8,1.8-1.8v-7.9c0-1-0.8-1.8-1.8-1.8h-7.9c-0.5,0-1,0.2-1.3,0.5     c-0.3,0.3-0.5,0.8-0.5,1.3c0,1,0.8,1.8,1.8,1.8h3.4l-8,8c-0.7,0.7-0.7,1.9,0,2.6S71.7,30.9,72.4,30.2z"></path>
                                    </g>
                                 </g>
                              </switch>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Create and Manage Profile</h3>
                           <p>Users can create their profiles by providing their details, such as name, contact details, and many more. They can also manage and edit their profiles if need be.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
                              <path d="M15.9990234,29.9993896c-0.1806641,0-0.3613281-0.0478516-0.5205078-0.1450195l-0.1103516-0.065918  c-2.0087891-1.184082-4.6259766-2.7895508-5.6855469-3.7143555c-0.9189453-0.800293-1.6601563-1.5493164-2.2666016-2.2900391  C5.8124871,21.8138428,5,19.7611084,5,17.6810303V6.5008545c0-0.4248047,0.2685547-0.8037109,0.6699219-0.9438477l10-3.5  c0.2128906-0.0751953,0.4472656-0.0751953,0.6601563,0l10,3.5C26.7314453,5.6971436,27,6.0760498,27,6.5008545v11.1899414  c0,2.0737305-0.8125134,4.1230469-2.4150391,6.0913086c-0.6074219,0.7426758-1.3486328,1.4916992-2.2675781,2.2924805  c-1.0595703,0.9243164-3.6767578,2.5297852-5.6855469,3.7138672l-0.1103516,0.065918  C16.3613281,29.9510498,16.1796875,29.9993896,15.9990234,29.9993896z M7,7.2103271v10.4707031  c0,1.6308594,0.6435547,3.2133789,1.9658203,4.8383789c0.5322266,0.6508789,1.1972656,1.3212891,2.03125,2.0473633  C11.6884766,25.1702881,13.5009766,26.3548584,16,27.8387451c2.4990234-1.4838867,4.3115234-2.668457,5.0029297-3.2714844  c0.8339844-0.7265625,1.4990234-1.3969727,2.0322266-2.0498047C24.3564453,20.8944092,25,19.3148193,25,17.6907959V7.2103271  l-9-3.1499023L7,7.2103271z"></path>
                              <path d="M16,14.5008545c-1.6542969,0-3-1.3457031-3-3s1.3457031-3,3-3s3,1.3457031,3,3S17.6542969,14.5008545,16,14.5008545z   M16,9.5008545c-1.1025391,0-2,0.8969727-2,2s0.8974609,2,2,2s2-0.8969727,2-2S17.1025391,9.5008545,16,9.5008545z"></path>
                              <path d="M21,21.5008545H11c-0.2763672,0-0.5-0.2236328-0.5-0.5c0-3.0327148,2.4677734-5.5,5.5-5.5s5.5,2.4672852,5.5,5.5  C21.5,21.2772217,21.2763672,21.5008545,21,21.5008545z M11.5273438,20.5008545h8.9453125c-0.25-2.2470703-2.1601696-4-4.4726563-4  C13.6874866,16.5008545,11.7773438,18.2537842,11.5273438,20.5008545z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Privacy Settings</h3>
                           <p>Users can change their privacy settings as per their convenience. They can choose to publish their videos publicly or only to their friends and followers.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                              <g>
                                 <g>
                                    <g>
                                       <g>
                                          <path d="M55,80.4v-0.8c0-3-2.5-5.5-5.5-5.5h-0.8c-3,0-5.5,2.5-5.5,5.5v0.8c0,3,2.5,5.5,5.5,5.5h0.8C52.6,85.9,55,83.4,55,80.4z       M74.6,70.6v16.3c0,4.1-3.4,7.5-7.5,7.5H31.2c-4.1,0-7.5-3.4-7.5-7.5V70.6v-5.8v-50c0-4.1,3.4-7.5,7.5-7.5h35.9      c4.1,0,7.5,3.4,7.5,7.5v50V70.6z M69.6,64.7v-50c0-1.4-1.1-2.5-2.5-2.5H31.2c-1.4,0-2.5,1.1-2.5,2.5v50h1.6      c0.6-4.4,2.3-9,4.6-12.6c1.4-2.2,3.2-4.1,5.2-5.6c0.2-0.3,0.3-0.5,0.3-0.7c0.1-0.5,0-1-0.4-1.5c-2-2.1-3.1-5.1-3.1-8.1      c0-6.7,5.5-12.2,12.2-12.2h0.2c6.7,0,12.1,5.5,12.1,12.2c0,3-1.2,6-3.2,8.2c-0.3,0.4-0.4,0.8-0.3,1.3c0,0.4,0.1,0.6,0.3,0.8      c2,1.5,3.8,3.4,5.2,5.6c2.4,3.7,4,8.2,4.6,12.6H69.6z M35.2,64.7h27.8c-1.5-10.3-8.6-16.5-13.9-16.5      C43.9,48.2,36.7,54.4,35.2,64.7z M56.5,36.1c0-4-3.3-7.4-7.4-7.4s-7.3,3.3-7.3,7.4c0,4.1,3.3,7.4,7.3,7.4S56.5,40.1,56.5,36.1z"></path>
                                       </g>
                                       <g>
                                          <path d="M52,79.6v0.8c0,1.4-1.1,2.5-2.5,2.5h-0.8c-1.4,0-2.5-1.1-2.5-2.5v-0.8c0-1.4,1.1-2.5,2.5-2.5h0.8      C50.9,77.1,52,78.2,52,79.6z"></path>
                                       </g>
                                    </g>
                                 </g>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Create Video</h3>
                           <p>Users can create videos on the app by flaunting their skills. They can choose to lip-sync to a song/audio, dance, show expressions, and more as per their preferences.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                              <title>Video and Movie</title>
                              <path d="M62,5.25H2a2,2,0,0,0-2,2v49.5a2,2,0,0,0,2,2H62a2,2,0,0,0,2-2V7.25A2,2,0,0,0,62,5.25Zm-2,49.5H4V21.25H60Zm0-37.5H4v-8H60Z"></path>
                              <circle cx="44" cy="13.25" r="2"></circle>
                              <circle cx="50" cy="13.25" r="2"></circle>
                              <circle cx="56" cy="13.25" r="2"></circle>
                              <path d="M8,15.25H22a2,2,0,0,0,0-4H8a2,2,0,0,0,0,4Z"></path>
                              <path d="M26.37,47.34a1.83,1.83,0,0,0,.93.25,1.77,1.77,0,0,0,.92-.25L41.63,39.6a1.85,1.85,0,0,0,0-3.2L28.22,28.66a1.85,1.85,0,0,0-2.77,1.6V45.74A1.85,1.85,0,0,0,26.37,47.34Zm2.78-13.88L37,38l-7.85,4.54Z"></path>
                              <path d="M32,53.5A15.5,15.5,0,1,0,16.5,38,15.51,15.51,0,0,0,32,53.5Zm0-27A11.5,11.5,0,1,1,20.5,38,11.51,11.51,0,0,1,32,26.5Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Post Video</h3>
                           <p>Users can share the created videos to their profiles. They can post these videos with a short description, relevant hashtags, location tags, etc. They can also tag other users.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                              <title>a</title>
                              <g data-name="04 Spesial effects">
                                 <path d="M46,14H45V13a1,1,0,0,0-2,0v1H42a1,1,0,0,0,0,2h1v1a1,1,0,0,0,2,0V16h1a1,1,0,0,0,0-2Z"></path>
                                 <path d="M21,17H20V16a1,1,0,0,0-2,0v1H17a1,1,0,0,0,0,2h1v1a1,1,0,0,0,2,0V19h1a1,1,0,0,0,0-2Z"></path>
                                 <path d="M56,8H8a5.006,5.006,0,0,0-5,5V43a5.006,5.006,0,0,0,5,5H29v5a3,3,0,0,0,6,0V48H56a5.006,5.006,0,0,0,5-5V13A5.006,5.006,0,0,0,56,8ZM8,46a3,3,0,0,1-3-3V13a3,3,0,0,1,3-3h3V46ZM24.2,30.747l-4.4-4.841a1,1,0,0,1,.683-1.677l5.534-.291a3.01,3.01,0,0,0,2.52-1.665l2.57-5.2a1,1,0,0,1,1.788,0l2.572,5.2a3.009,3.009,0,0,0,2.521,1.661l5.53.291a1,1,0,0,1,.683,1.678l-4.4,4.84-.006.007a3.036,3.036,0,0,0-.73,2.524l1.01,5.9a.993.993,0,0,1-1.448,1.046l-4.145-2.179c-.006,0-.011-.009-.017-.012l-1.07-.56h0l0,0c-.041-.021-.086-.029-.127-.048a3,3,0,0,0-2.656.05l-1.07.56-.014.01-4.151,2.183a.963.963,0,0,1-1.048-.071.98.98,0,0,1-.4-.978l1.011-5.907A3.037,3.037,0,0,0,24.2,30.747ZM33,53a1,1,0,0,1-2,0V39.526l.472-.248.058-.03a1.039,1.039,0,0,1,.936,0l.534.281V53Zm2-12.422,2.691,1.415a2.993,2.993,0,0,0,4.354-3.152l-1.008-5.892a1.037,1.037,0,0,1,.247-.861l4.4-4.834a3,3,0,0,0-2.057-5.023l-5.527-.29a1,1,0,0,1-.839-.554l-2.57-5.2a3,3,0,0,0-5.374,0l-2.568,5.2a1,1,0,0,1-.838.558l-5.531.29a3,3,0,0,0-2.057,5.022l4.395,4.832a1.036,1.036,0,0,1,.251.856l-1.01,5.9A2.989,2.989,0,0,0,26.306,42L29,40.578V46H13V10H51V46H35ZM59,43a3,3,0,0,1-3,3H53V10h3a3,3,0,0,1,3,3Z"></path>
                                 <path d="M8,25a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V26A1,1,0,0,0,8,25Z"></path>
                                 <path d="M8,15a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V16A1,1,0,0,0,8,15Z"></path>
                                 <path d="M8,35a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V36A1,1,0,0,0,8,35Z"></path>
                                 <path d="M56,25a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V26A1,1,0,0,0,56,25Z"></path>
                                 <path d="M56,15a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V16A1,1,0,0,0,56,15Z"></path>
                                 <path d="M56,35a1,1,0,0,0-1,1v4a1,1,0,0,0,2,0V36A1,1,0,0,0,56,35Z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Special Video Filters</h3>
                           <p>Users can apply special filters to their videos to improve their quality. This way, they can make it more appealing, captivating the interests of their followers.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
                              <path d="M48.375,39.646v-23.68c0-0.03-0.014-0.056-0.017-0.084c-0.004-0.029,0.004-0.056-0.003-0.085  c-0.005-0.021-0.02-0.037-0.026-0.057c-0.017-0.051-0.039-0.097-0.065-0.143c-0.022-0.039-0.045-0.076-0.073-0.109  c-0.034-0.04-0.072-0.073-0.114-0.105c-0.034-0.026-0.067-0.051-0.105-0.071c-0.045-0.024-0.092-0.04-0.143-0.054  c-0.046-0.013-0.09-0.023-0.139-0.028c-0.022-0.002-0.041-0.013-0.064-0.013c-0.03,0-0.056,0.014-0.085,0.017  c-0.028,0.003-0.056-0.004-0.084,0.002l-22.58,5.231c-0.021,0.005-0.038,0.02-0.058,0.026c-0.05,0.016-0.094,0.037-0.138,0.063  c-0.042,0.023-0.08,0.047-0.116,0.078c-0.037,0.031-0.066,0.065-0.096,0.103c-0.03,0.038-0.058,0.075-0.08,0.119  c-0.021,0.042-0.036,0.084-0.05,0.13c-0.015,0.049-0.025,0.096-0.03,0.148c-0.002,0.022-0.013,0.041-0.013,0.063v20.079  c-0.92-0.917-2.188-1.485-3.587-1.485c-2.805,0-5.087,2.282-5.087,5.087s2.282,5.087,5.087,5.087s5.087-2.282,5.087-5.087V27.025  l21.08-4.884v13.903c-0.92-0.917-2.188-1.484-3.586-1.484c-2.806,0-5.088,2.281-5.088,5.086s2.282,5.087,5.088,5.087  C46.094,44.733,48.375,42.451,48.375,39.646z"></path>
                              <path d="M12.987,16.712c2.367,0,4.734-0.901,6.536-2.703c0.293-0.293,0.293-0.768,0-1.061s-0.768-0.293-1.061,0  c-3.02,3.02-7.932,3.019-10.952,0c-2.356-2.356-2.356-6.191,0-8.549c0.885-0.885,2.062-1.373,3.313-1.373  c1.252,0,2.429,0.487,3.314,1.373c1.403,1.403,1.403,3.687,0,5.09c-0.293,0.293-0.293,0.768,0,1.061s0.768,0.293,1.061,0  c1.988-1.988,1.988-5.223,0-7.211c-2.412-2.413-6.336-2.412-8.749,0c-2.941,2.942-2.941,7.729,0,10.67  C8.253,15.811,10.62,16.712,12.987,16.712z"></path>
                              <path d="M18.993,21c-6.158,0-11.167,4.535-11.167,10.108c0,0.414,0.336,0.75,0.75,0.75s0.75-0.336,0.75-0.75  c0-4.747,4.337-8.608,9.667-8.608c0.414,0,0.75-0.336,0.75-0.75S19.407,21,18.993,21z"></path>
                              <path d="M58.134,54.475l-4.067-2.225l-2.225-4.067c-0.264-0.482-1.053-0.482-1.316,0l-2.225,4.067l-4.067,2.225  c-0.24,0.132-0.39,0.384-0.39,0.658s0.149,0.526,0.391,0.658l4.066,2.224l2.225,4.067c0.132,0.241,0.384,0.391,0.658,0.391  s0.526-0.149,0.658-0.391l2.225-4.067l4.066-2.224c0.241-0.132,0.391-0.384,0.391-0.658S58.374,54.606,58.134,54.475z"></path>
                              <path d="M54.965,11.576c2.5,0,4.533-2.034,4.533-4.534c0-2.499-2.033-4.532-4.533-4.532c-2.499,0-4.531,2.033-4.531,4.532  C50.434,9.542,52.466,11.576,54.965,11.576z"></path>
                              <path d="M14.29,50.247l-3.269-1.788L9.233,45.19c-0.264-0.48-1.053-0.48-1.316,0l-1.788,3.269l-3.268,1.788  c-0.241,0.132-0.39,0.384-0.39,0.658s0.15,0.526,0.39,0.658l3.268,1.786l1.788,3.269c0.132,0.241,0.384,0.391,0.658,0.391  s0.526-0.149,0.658-0.391l1.788-3.269l3.269-1.786c0.24-0.132,0.39-0.384,0.39-0.658S14.53,50.379,14.29,50.247z"></path>
                              <path d="M32.881,48.466c-0.414,0-0.75,0.336-0.75,0.75c0,4.462-3.783,8.093-8.432,8.093c-0.414,0-0.75,0.336-0.75,0.75  s0.336,0.75,0.75,0.75c5.477,0,9.933-4.304,9.933-9.593C33.632,48.802,33.296,48.466,32.881,48.466z"></path>
                              <path d="M61.485,35.87l-5.814-16.245c-0.14-0.39-0.568-0.593-0.959-0.453c-0.39,0.14-0.593,0.569-0.453,0.959l5.814,16.245  c0.109,0.307,0.398,0.497,0.706,0.497c0.084,0,0.169-0.014,0.253-0.044C61.422,36.689,61.625,36.261,61.485,35.87z"></path>
                              <path d="M30.409,11.576c0.151,0,0.305-0.046,0.437-0.141l8.542-6.132c0.336-0.242,0.413-0.71,0.172-1.047s-0.709-0.412-1.047-0.172  l-8.542,6.132c-0.336,0.242-0.413,0.71-0.172,1.047C29.946,11.468,30.176,11.576,30.409,11.576z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">My Favourite Sounds</h3>
                           <p>Users can add their favorite audio/music tracks to this section of the app to access them quickly and use them in their videos</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <switch>
                                 <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                 <g i:extraneous="self">
                                    <g>
                                       <path d="M76.9,64.2c-2.3-2.8-3.5-6.3-3.5-9.9v-8.2c0-11.9-8.9-21.7-20.4-23.2v-4.1c0-1.6-1.3-2.9-2.9-2.9s-2.9,1.3-2.9,2.9V23     c-11.5,1.4-20.4,11.3-20.4,23.2v8.2c0,3.6-1.2,7.1-3.5,9.9l-3.6,4.4c-1.9,2.3-2.3,5.4-1,8.1c1.3,2.7,3.9,4.4,6.9,4.4h11.2     C37.3,87.9,43,93.3,50,93.3s12.7-5.4,13.3-12.2h11.2c3,0,5.6-1.7,6.9-4.4c1.3-2.7,0.9-5.8-1-8.1L76.9,64.2z M50,87.4     c-3.7,0-6.8-2.7-7.4-6.3h14.8C56.8,84.7,53.7,87.4,50,87.4z M76.2,74.3c-0.1,0.3-0.6,1-1.6,1H25.5c-1,0-1.5-0.7-1.6-1     c-0.1-0.3-0.4-1.1,0.2-1.9l3.6-4.4c3.1-3.8,4.8-8.6,4.8-13.5v-8.2c0-9.7,7.9-17.5,17.5-17.5s17.5,7.9,17.5,17.5v8.2     c0,4.9,1.7,9.7,4.8,13.5v0l3.6,4.4C76.6,73.1,76.3,73.9,76.2,74.3z"></path>
                                       <path d="M71.7,17.8c-1.3-1-3.1-0.7-4.1,0.5c-1,1.3-0.7,3.1,0.5,4.1c5.1,4,8.9,9.6,10.7,15.8c0.4,1.3,1.5,2.1,2.8,2.1     c0.3,0,0.5,0,0.8-0.1c1.6-0.4,2.5-2,2-3.6C82.4,29.2,77.9,22.5,71.7,17.8z"></path>
                                       <path d="M21.2,38.2c1.7-6.2,5.5-11.8,10.7-15.8c1.3-1,1.5-2.8,0.5-4.1c-1-1.3-2.8-1.5-4.1-0.5c-6.2,4.7-10.7,11.4-12.8,18.8     c-0.4,1.6,0.5,3.2,2,3.6c0.3,0.1,0.5,0.1,0.8,0.1C19.6,40.3,20.8,39.5,21.2,38.2z"></path>
                                       <path d="M97.5,37.7c-2.2-12.2-8.9-23-19-30.4c-1.3-1-3.1-0.7-4.1,0.6c-1,1.3-0.7,3.1,0.6,4.1c8.9,6.5,14.8,16,16.7,26.7     c0.3,1.4,1.5,2.4,2.9,2.4c0.2,0,0.3,0,0.5,0C96.7,40.8,97.7,39.3,97.5,37.7z"></path>
                                       <path d="M25,12c1.3-1,1.6-2.8,0.6-4.1c-1-1.3-2.8-1.6-4.1-0.6c-10.1,7.4-16.8,18.2-19,30.4c-0.3,1.6,0.8,3.1,2.4,3.4     c0.2,0,0.3,0,0.5,0c1.4,0,2.6-1,2.9-2.4C10.2,28,16.2,18.5,25,12z"></path>
                                    </g>
                                 </g>
                              </switch>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">In-app Pop-up Alerts</h3>
                           <p>Users will be updated on the trending hashtags, user profiles, videos, etc., via the in-app push notifications. They can also be notified about the app updates.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
                              <g>
                                 <path d="M511.737,189.898H170.41l340.546-78.653l-17.374-75.372c-4.875-21.125-25.718-34.343-46.56-29.53L30.043,102.652   c-17.719,4.093-29.749,19.905-29.968,37.561H0.013l0.094,327.152c0,21.687,17.563,39.28,39.217,39.28h433.447   c21.654,0,39.217-17.596,39.217-39.28L511.737,189.898z M402.368,47.186l67.436,44.03l-51.997,12L350.37,59.216L402.368,47.186z    M297.466,71.435l67.436,44.03l-51.998,12L245.47,83.465L297.466,71.435z M195.097,95.089l67.435,44.03l-51.998,12l-67.434-44.03   L195.097,95.089z M90.477,119.246l67.403,44.029l-51.998,12l-67.402-43.998L90.477,119.246z M472.739,467.365H39.229V229.084   h433.51V467.365z"></path>
                                 <polygon points="215.096,286.091 215.096,413.617 317.122,349.838  "></polygon>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Watch Video</h3>
                           <p>Users can watch videos of other users that appear on the TikTok clone app. They can also view the locations and other user profiles tagged in the videos.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                              <title>Video and Movie</title>
                              <path d="M18,64H46a6,6,0,0,0,6-6V6a6,6,0,0,0-6-6H18a6,6,0,0,0-6,6V58A6,6,0,0,0,18,64ZM26.08,4H37.92V5H26.08ZM16,6a2,2,0,0,1,2-2h4.08V7a2,2,0,0,0,2,2H39.92a2,2,0,0,0,2-2V4H46a2,2,0,0,1,2,2V58a2,2,0,0,1-2,2H18a2,2,0,0,1-2-2Z"></path>
                              <path d="M26.37,41.34a1.83,1.83,0,0,0,.93.25,1.77,1.77,0,0,0,.92-.25L41.63,33.6a1.85,1.85,0,0,0,0-3.2L28.22,22.66a1.85,1.85,0,0,0-2.77,1.6V39.74A1.85,1.85,0,0,0,26.37,41.34Zm2.78-13.88L37,32l-7.85,4.54Z"></path>
                              <path d="M32,47.5A15.5,15.5,0,1,0,16.5,32,15.51,15.51,0,0,0,32,47.5Zm0-27A11.5,11.5,0,1,1,20.5,32,11.51,11.51,0,0,1,32,20.5Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Discover Videos</h3>
                           <p>Users can browse for trending videos by searching through the platform using profile names, hashtags, locations, and more.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 100 100" x="0px" y="0px">
                              <title>Artboard 3</title>
                              <path d="M38.11,41a3.15,3.15,0,1,1,3.15,3.15A3.15,3.15,0,0,1,38.11,41ZM59,44.12A3.15,3.15,0,1,0,55.89,41,3.15,3.15,0,0,0,59,44.12ZM1,94.73c0-17.2,21.69-24.06,31-26.22a.25.25,0,0,0,.19-.25V64.62a28.42,28.42,0,0,1-4.74-9.45C25,52.77,23.92,49,23.34,46.9c-.95-3.34-1.9-8.19.71-11.64l.12-.15c-.17-1-.3-2-.38-3-1.18-7.84-.27-13.69,2.71-17.35A9.89,9.89,0,0,1,33,11.08c1.52-5.25,8.94-10.37,15.67-9C55.2,3.48,68.83,1.94,69,1.92a.68.68,0,0,1,.48.14.64.64,0,0,1,.23.45,6.72,6.72,0,0,1-1.92,4.89c10.9,7.27,9.33,21.62,8.07,27.69l.11.15c2.61,3.46,1.66,8.3.72,11.64-.58,2.06-1.66,5.88-4.1,8.29a28.29,28.29,0,0,1-4.74,9.38v3.7a.26.26,0,0,0,.19.25c9.33,2.15,31,9,31,26.22a3.39,3.39,0,0,1-3.38,3.38H4.35A3.39,3.39,0,0,1,1,94.73ZM64.66,74.39a7,7,0,0,1-3.2-3.84A23.19,23.19,0,0,1,56.58,73a19.73,19.73,0,0,1-13.16,0,22.7,22.7,0,0,1-4.9-2.44,7,7,0,0,1-3.19,3.79A17.89,17.89,0,0,0,50,81.68,17.89,17.89,0,0,0,64.66,74.39ZM32.32,51.21a.64.64,0,0,1,.59.54c.88,5.67,5.17,13,12.54,15.55a13.55,13.55,0,0,0,9.08,0c7.07-2.55,11.68-9.74,12.57-15.54a.64.64,0,0,1,.59-.54c.35,0,1.6-.54,3.14-6,1.2-4.24.69-5.78.29-6.32a1.55,1.55,0,0,0-1.45-.5.65.65,0,0,1-.57-.24.64.64,0,0,1-.1-.61l0-.13c.3-1,1.56-6.18-2.82-9.42-3.85.86-10.16,1.09-18.64-2.53-4.14-1.77-7.62-1.83-10.32-.18-5.42,3.3-6.16,12.43-6.17,12.52a.64.64,0,0,1-.68.59h-.29a1.38,1.38,0,0,0-1.19.52c-.41.54-.91,2.08.29,6.32C30.72,50.67,32,51.19,32.32,51.21Zm8.64,2a9.21,9.21,0,0,0,18.08,0,1.5,1.5,0,0,0-1.49-1.77H42.45A1.5,1.5,0,0,0,41,53.18Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">View User Profile</h3>
                           <p>Users can go through the profiles of other users registered with the social video-sharing platform. They can also view their videos seamlessly.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <switch>
                                 <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                 <g i:extraneous="self">
                                    <path class="st0" d="M88.4,57.7c-0.1-2.5-1.3-4.5-2.2-5.7c1.1-2.6,1.6-6.8-2-10.1c-5.9-5.4-19.8-2.5-21.3-2.1    c0,0,0,0,0,0c-0.9,0.2-1.8,0.3-2.8,0.6c-0.1-1,0-3.6,2.1-9.9c2.3-7,2.2-12.4-0.4-15.9c-2.7-3.7-7-4-8.3-4c-1.2,0-2.3,0.5-3.1,1.4    C49.4,13,49,14.5,48.9,16c-0.8,0-1.6,0.2-2.4,0.6c-0.8,0.3-1.4,0.8-2,1.3c-2.2-1.9-5.1-2.6-8.3-2.1c-2.7,0.5-4.6,1.7-4.7,1.7    c-0.7,0.5-1,1.4-0.5,2.1c1,1.7,2.1,3,3.4,4c-2.4,1.9-3.8,4.1-3.9,4.2c-0.2,0.4-0.3,0.8-0.2,1.3c0.1,0.4,0.4,0.8,0.8,1    c2.6,1.3,5,1.8,7,1.8c2.7,0,4.8-0.9,6.3-1.8c0.1-0.1,0.2-0.1,0.3-0.2c-2.7,5.8-6.2,11.8-9.3,14.2c-0.1,0.1-0.2,0.1-0.2,0.2    c-1.6,1.6-2.6,3.4-3.3,5c-1-0.6-2.1-0.9-3.4-0.9H18.4c-3.7,0-6.8,3-6.8,6.8v26.9c0,3.7,3,6.8,6.8,6.8h10.1c1.5,0,2.9-0.5,4.1-1.4    l3.8,0.6c0,0,0,0,0,0c0.5,0.1,11.1,1.4,22.1,1.2c2,0.2,3.8,0.2,5.5,0.2c3,0,5.5-0.2,7.7-0.7c5-1.1,8.5-3.2,10.2-6.3    c1.3-2.4,1.3-4.8,1.1-6.3c4.5-4.1,4.2-8.9,3.1-11.9C87.7,62.2,88.5,60,88.4,57.7z M22.8,82.9c-1.9,0-3.5-1.6-3.5-3.5    c0-1.9,1.6-3.5,3.5-3.5c1.9,0,3.5,1.6,3.5,3.5C26.3,81.4,24.8,82.9,22.8,82.9z M51,26l5.8,4.6l-7.9,0.1C49.7,29,50.4,27.4,51,26z     M58.5,28l-5.7-4.5l6.7-2.1C59.6,23.2,59.3,25.4,58.5,28z M53.4,14.7c0.8,0,3.4,0.2,4.9,2.3c0.3,0.4,0.5,0.8,0.7,1.3l-5.8,1.8    c0-0.1,0-0.3,0-0.4C52.8,17.7,53,15.3,53.4,14.7z M47.7,19.4c0.4-0.2,0.8-0.3,1.2-0.3c0,0.2,0,0.4,0.1,0.6    c-0.2,0.6-0.5,1.4-0.8,2.2c-0.8-0.4-1.5-0.7-2.3-0.9C46.3,20.3,46.9,19.8,47.7,19.4z M42.6,20.4C42.6,20.4,42.6,20.5,42.6,20.4    c0,0.1,0,0.1,0,0.1c-1.8,0-3.7,0.4-5.4,1.3c-0.9-0.5-1.8-1.3-2.6-2.3C36.5,18.7,40,18,42.6,20.4z M34.2,28    c0.7-0.8,1.7-1.7,2.9-2.6c2.9-2,5.8-2.3,8.8-1.1C45,25.6,41.2,30.6,34.2,28z M82.1,62.4c-0.7,0.7-0.8,1.7-0.3,2.5    c0.3,0.5,2.7,4.9-2.4,8.7c-0.7,0.5-1,1.5-0.7,2.4c0,0,0.7,2.2-0.5,4.4c-1.1,2-3.6,3.5-7.4,4.3c-3,0.6-7.1,0.8-12.1,0.4    c-0.1,0-0.1,0-0.2,0c-10.5,0.2-21-1.1-21.6-1.2l-1.9-0.3c0.1-0.5,0.2-1,0.2-1.5V55.2c0-0.7-0.1-1.4-0.3-2.1    c0.3-1.2,1.2-3.7,3.1-5.8c3.3-2.6,6.7-8.1,9.3-13.5l9.4-0.1c-1.4,5.5-1.2,8.6,0.5,10.1c1.1,0.9,2.5,1,3.5,0.6c1-0.2,2-0.4,2.9-0.6    c0.1,0,0.1,0,0.2,0C69,42.7,78.1,42,81.4,45c2.8,2.5,0.8,5.9,0.6,6.2c-0.6,0.9-0.4,2,0.4,2.7c0,0,1.8,1.7,1.9,4    C84.3,59.4,83.6,60.9,82.1,62.4z"></path>
                                 </g>
                              </switch>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">View and Like Post</h3>
                           <p>Users can view video posts shared by other users. If they find it interesting, they can like the videos by double-tapping on it. </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              <g>
                                 <path d="M446.1,27H65.9C34.4,27,9,52.2,9,83.7v243.8C9,358.9,34.4,384,65.9,384H75v84.3c0,6.7,4,12.8,10.1,15.5   c2.2,1,4.6,1.5,6.9,1.5c4.1,0,8.2-1.8,11.4-4.6L209.9,384h236.3c31.4,0,56.9-25.1,56.9-56.5V83.7C503,52.2,477.6,27,446.1,27z    M469,327.5c0,12.7-10.2,22.5-22.9,22.5H203.3c-4.2,0-8.3,1.8-11.4,4.6L109,430v-62.5c0-9.4-7.6-17.5-17-17.5H65.9   c-12.7,0-22.9-9.8-22.9-22.5V83.7C43,71,53.2,61,65.9,61h380.3c12.7,0,22.9,10,22.9,22.7V327.5z"></path>
                                 <path d="M381.6,249H130.4c-9.4,0-17,7.6-17,17s7.6,17,17,17h251.1c9.4,0,17-7.6,17-17S391,249,381.6,249z"></path>
                                 <path d="M381.6,183H130.4c-9.4,0-17,7.6-17,17s7.6,17,17,17h251.1c9.4,0,17-7.6,17-17S391,183,381.6,183z"></path>
                                 <path d="M130.4,150h95.9c9.4,0,17-7.6,17-17s-7.6-17-17-17h-95.9c-9.4,0-17,7.6-17,17S121,150,130.4,150z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Comment on Videos</h3>
                           <p>Users can share their feedback on videos shared by other users, using the comment feature included in the app.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 32 32" x="0px" y="0px">
                              <path d="M18,31H14a1,1,0,0,1-.9614-.7256l-1.2051-4.2158-3.833,2.1289a.9985.9985,0,0,1-1.1929-.167L3.98,25.1924A.9993.9993,0,0,1,3.8125,24l2.1294-3.834L1.7251,18.9619A1.0009,1.0009,0,0,1,1,18V14a1.0009,1.0009,0,0,1,.7251-.9619L5.9419,11.834,3.8125,8A.9993.9993,0,0,1,3.98,6.8076L6.8076,3.98a.9972.9972,0,0,1,1.1929-.167l3.833,2.1289,1.2051-4.2158A1,1,0,0,1,14,1h4a1.0007,1.0007,0,0,1,.9619.7256L20.166,5.9414,24,3.8125a.9984.9984,0,0,1,1.1924.167l2.8281,2.8281A1,1,0,0,1,28.1875,8l-2.1289,3.834,4.2158,1.2041A1.0007,1.0007,0,0,1,31,14v4a1.0007,1.0007,0,0,1-.7256.9619L26.0586,20.166,28.1875,24a1,1,0,0,1-.167,1.1924l-2.8281,2.8281A.9969.9969,0,0,1,24,28.1875l-3.834-2.1289-1.2041,4.2158A1.0007,1.0007,0,0,1,18,31Zm-3.2456-2h2.4917l1.3428-4.7021a1,1,0,0,1,1.4472-.6l4.2754,2.375,1.7617-1.7617-2.375-4.2754a1,1,0,0,1,.6-1.4472L29,17.2461V14.7539l-4.7021-1.3428a1,1,0,0,1-.6-1.4472l2.375-4.2754L24.3115,5.9268l-4.2754,2.375a1,1,0,0,1-1.4472-.6L17.2461,3H14.7544L13.4106,7.7021a1,1,0,0,1-1.4472.6L7.6885,5.9268,5.9268,7.6885l2.375,4.2754a1,1,0,0,1-.5992,1.4472L3,14.7539v2.4922l4.7026,1.3428a1,1,0,0,1,.5992,1.4472l-2.375,4.2754,1.7617,1.7617,4.2749-2.375a1,1,0,0,1,1.4472.6Z"></path>
                              <path d="M16,21a5,5,0,1,1,5-5A5.0059,5.0059,0,0,1,16,21Zm0-8a3,3,0,1,0,3,3A3.0033,3.0033,0,0,0,16,13Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">General settings</h3>
                           <p>Users can manage their general settings like view app content, share with the public or friends, etc.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--  -->
         <!--  -->
         <section id="features" class="section features-area grey_bg style-two overflow-hidden ptb_100">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-12 col-md-10 col-lg-7">
                     <!-- Section Heading -->
                     <div class="section-heading text-center">
                        <span class="d-inline-block rounded-pill shadow-sm fw-5 px-4 py-2 mb-3">
                           <svg class="svg-inline--fa fa-lightbulb fa-w-11 text-primary mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="lightbulb" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg="">
                              <path fill="currentColor" d="M176 80c-52.94 0-96 43.06-96 96 0 8.84 7.16 16 16 16s16-7.16 16-16c0-35.3 28.72-64 64-64 8.84 0 16-7.16 16-16s-7.16-16-16-16zM96.06 459.17c0 3.15.93 6.22 2.68 8.84l24.51 36.84c2.97 4.46 7.97 7.14 13.32 7.14h78.85c5.36 0 10.36-2.68 13.32-7.14l24.51-36.84c1.74-2.62 2.67-5.7 2.68-8.84l.05-43.18H96.02l.04 43.18zM176 0C73.72 0 0 82.97 0 176c0 44.37 16.45 84.85 43.56 115.78 16.64 18.99 42.74 58.8 52.42 92.16v.06h48v-.12c-.01-4.77-.72-9.51-2.15-14.07-5.59-17.81-22.82-64.77-62.17-109.67-20.54-23.43-31.52-53.15-31.61-84.14-.2-73.64 59.67-128 127.95-128 70.58 0 128 57.42 128 128 0 30.97-11.24 60.85-31.65 84.14-39.11 44.61-56.42 91.47-62.1 109.46a47.507 47.507 0 0 0-2.22 14.3v.1h48v-.05c9.68-33.37 35.78-73.18 52.42-92.16C335.55 260.85 352 220.37 352 176 352 78.8 273.2 0 176 0z"></path>
                           </svg>
                           <!-- <i class="far fa-lightbulb text-primary mr-1"></i> -->
                           <span class="text-primary">Add-ons</span>
                        </span>
                        <h2>Customizable Add-ons Of Our TikTok Clone</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                              <title>list, checkbox, news, feed</title>
                              <g>
                                 <path d="M20.7715,9.0889H12.0859a1.5,1.5,0,0,0-1.5,1.5v8.6845a1.5,1.5,0,0,0,1.5,1.5h8.6856a1.5,1.5,0,0,0,1.5-1.5V10.5889A1.5,1.5,0,0,0,20.7715,9.0889Zm-1.5,8.6845H13.5859V12.0889h5.6856Zm12.2724-4.8418a1.5,1.5,0,0,1,1.5-1.5h18.87a1.5,1.5,0,0,1,0,3h-18.87A1.5,1.5,0,0,1,31.5439,12.9316Zm21.87,6.3418a1.5,1.5,0,0,1-1.5,1.5h-22.87a1.5,1.5,0,0,1,0-3h22.87A1.5,1.5,0,0,1,53.4141,19.2734ZM20.7715,26.1572H12.0859a1.5,1.5,0,0,0-1.5,1.5v8.6856a1.5,1.5,0,0,0,1.5,1.5h8.6856a1.5,1.5,0,0,0,1.5-1.5V27.6572A1.5,1.5,0,0,0,20.7715,26.1572Zm-1.5,8.6856H13.5859V29.1572h5.6856ZM53.4141,30a1.5,1.5,0,0,1-1.5,1.5h-18.87a1.5,1.5,0,0,1,0-3h18.87A1.5,1.5,0,0,1,53.4141,30Zm0,6.3428a1.5,1.5,0,0,1-1.5,1.5h-22.87a1.5,1.5,0,1,1,0-3h22.87A1.5,1.5,0,0,1,53.4141,36.3428ZM20.7715,43.2266H12.0859a1.5,1.5,0,0,0-1.5,1.5v8.6845a1.5,1.5,0,0,0,1.5,1.5h8.6856a1.5,1.5,0,0,0,1.5-1.5V44.7266A1.5,1.5,0,0,0,20.7715,43.2266Zm-1.5,8.6845H13.5859V46.2266h5.6856Zm34.1426-4.8427a1.5,1.5,0,0,1-1.5,1.5h-18.87a1.5,1.5,0,0,1,0-3h18.87A1.5,1.5,0,0,1,53.4141,47.0684Zm0,6.3427a1.5,1.5,0,0,1-1.5,1.5h-22.87a1.5,1.5,0,0,1,0-3h22.87A1.5,1.5,0,0,1,53.4141,53.4111Z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Personalized News Feed</h3>
                           <p>The news feed of users will be customized as per their watch history and preferences. It will be updated continuously with the videos shared by their preferred users or channels.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 512 512" x="0px" y="0px">
                              <title>Song</title>
                              <path d="M86.754,409.816a8,8,0,0,0-11.313,0l-9.952,9.952-9.952-9.952a8,8,0,1,0-11.313,11.313l9.951,9.952-9.951,9.952a8,8,0,1,0,11.313,11.313l9.952-9.952,9.952,9.952a8,8,0,0,0,11.313-11.313L76.8,431.081l9.952-9.952A8,8,0,0,0,86.754,409.816Z"></path>
                              <path d="M469.174,419.66l-26.077-3.8L431.43,392.136a8,8,0,0,0-14.357,0l-11.667,23.722-26.078,3.8a8,8,0,0,0-4.439,13.635l18.886,18.473-4.46,26.092a8,8,0,0,0,11.619,8.423l23.317-12.3,23.318,12.3a8,8,0,0,0,11.619-8.423l-4.46-26.092L473.614,433.3a8,8,0,0,0-4.44-13.635Z"></path>
                              <path d="M58,85V99a8,8,0,0,0,16,0V85H88a8,8,0,0,0,0-16H74V55a8,8,0,0,0-16,0V69H44a8,8,0,0,0,0,16Z"></path>
                              <path d="M353.775,142.794A164.958,164.958,0,0,0,341,131.289v65.243a40.1,40.1,0,1,1-16-32V119.843C261.721,80.13,177,87.778,121.985,142.794c-63.9,63.905-63.9,167.886,0,231.791a164.085,164.085,0,0,0,231.79,0h0C417.68,310.68,417.68,206.7,353.775,142.794Zm-23.238,115.9a8,8,0,1,1,16,0A108.78,108.78,0,0,1,237.88,367.346a8,8,0,0,1,0-16A92.762,92.762,0,0,0,330.537,258.689Zm-36.447,0a8,8,0,1,1,16,0,72.291,72.291,0,0,1-72.21,72.21,8,8,0,0,1,0-16A56.273,56.273,0,0,0,294.09,258.689Zm-179.522,0a8,8,0,0,1-16,0c0-76.816,62.495-139.312,139.312-139.312a8,8,0,0,1,0,16A123.452,123.452,0,0,0,114.568,258.689Zm22.656,8a8,8,0,0,1-8-8A108.779,108.779,0,0,1,237.88,150.033a8,8,0,0,1,0,16,92.761,92.761,0,0,0-92.656,92.656A8,8,0,0,1,137.224,266.689Zm44.447-8a8,8,0,0,1-16,0A72.291,72.291,0,0,1,237.88,186.48a8,8,0,0,1,0,16A56.272,56.272,0,0,0,181.671,258.689Zm22.414,0a33.8,33.8,0,1,1,33.795,33.8A33.833,33.833,0,0,1,204.085,258.689ZM237.88,398a8,8,0,0,1,0-16A123.453,123.453,0,0,0,361.193,258.689a8,8,0,0,1,16,0C377.193,335.507,314.7,398,237.88,398Z"></path>
                              <path d="M255.676,258.689a17.8,17.8,0,1,0-17.8,17.8A17.816,17.816,0,0,0,255.676,258.689Z"></path>
                              <path d="M341,113.136A50.786,50.786,0,0,1,409.688,98.68a50.709,50.709,0,0,1,23.955,43.164A8.152,8.152,0,0,0,441.651,150h.006A8.6,8.6,0,0,0,450,141.664v-.013h0c0-64.438-52.511-116.858-116.791-116.858a8.181,8.181,0,0,0-8.209,8v87.051a162.976,162.976,0,0,1,16,11.446Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Song/Audio Selection</h3>
                           <p>Users can select their favorite soundtrack from the app to integrate into their songs. They can also upload their own tracks to create unique dubsmash content.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <path d="M50.002,13.754c-4.844-3.622-10.747-5.601-16.846-5.601c-7.521,0-14.592,2.928-19.909,8.246  C2.816,26.83,2.234,43.697,11.916,54.79c0.417,0.48,0.864,0.96,1.331,1.428l33.688,33.688c0.002,0.002,0.004,0.004,0.006,0.007  c0.003,0.002,0.006,0.005,0.008,0.007c0.266,0.265,0.564,0.473,0.874,0.655c0.081,0.144,0.178,0.282,0.29,0.411  c0.494,0.569,1.189,0.861,1.889,0.861c0.581,0,1.164-0.201,1.638-0.612c0.478-0.415,0.949-0.855,1.4-1.306  c0.004-0.004,0.008-0.006,0.012-0.01c0.005-0.005,0.009-0.009,0.014-0.014l33.688-33.688c0.465-0.464,0.913-0.945,1.333-1.429  c0.002-0.002,0.003-0.005,0.005-0.008C92.547,49.668,95,43.108,95,36.308c0-7.522-2.929-14.592-8.246-19.909  C76.825,6.469,61.069,5.471,50.002,13.754z M84.307,51.516c-0.342,0.395-0.704,0.784-1.088,1.167L50.001,85.9L16.783,52.684  c-0.382-0.383-0.745-0.772-1.094-1.175c-7.966-9.126-7.485-22.996,1.093-31.574c4.373-4.373,10.188-6.781,16.374-6.781  c5.597,0,10.995,2.02,15.201,5.688c0.021,0.018,0.045,0.03,0.066,0.047c0.07,0.057,0.144,0.106,0.219,0.155  c0.065,0.042,0.129,0.085,0.196,0.121c0.075,0.04,0.153,0.071,0.231,0.102c0.074,0.03,0.146,0.06,0.222,0.083  c0.077,0.023,0.155,0.037,0.233,0.052c0.08,0.015,0.158,0.032,0.239,0.039c0.079,0.008,0.157,0.006,0.237,0.006  c0.08,0,0.16,0.001,0.24-0.007c0.08-0.008,0.158-0.024,0.237-0.039c0.079-0.015,0.157-0.03,0.234-0.052  c0.076-0.022,0.148-0.053,0.222-0.083c0.078-0.031,0.155-0.062,0.23-0.102c0.068-0.036,0.132-0.079,0.197-0.121  c0.075-0.049,0.148-0.097,0.218-0.155c0.021-0.017,0.045-0.029,0.066-0.047c9.127-7.965,22.995-7.485,31.574,1.094  C87.592,24.307,90,30.122,90,36.308C90,41.905,87.98,47.304,84.307,51.516z"></path>
                              <path d="M42.069,37.458c1.668-1.668,1.668-4.396,0-6.064v0c-1.668-1.668-4.396-1.668-6.064,0l-0.434,0.434l-0.434-0.434  c-1.668-1.668-4.396-1.668-6.064,0v0c-1.668,1.668-1.668,4.396,0,6.064l6.498,6.498L42.069,37.458z"></path>
                              <path d="M70.927,31.393c-1.668-1.668-4.396-1.668-6.064,0l-0.434,0.434l-0.434-0.434c-1.668-1.668-4.396-1.668-6.064,0v0  c-1.668,1.668-1.668,4.396,0,6.064l6.498,6.498l6.498-6.498C72.594,35.79,72.594,33.061,70.927,31.393L70.927,31.393z"></path>
                              <path d="M61.667,51.598c-6.543,6.234-16.79,6.234-23.333,0c-1.06-1.012-2.742-0.973-3.755,0.09c-1.013,1.061-0.972,2.743,0.09,3.755  c4.299,4.096,9.815,6.145,15.332,6.145s11.033-2.048,15.332-6.145c1.063-1.012,1.103-2.694,0.09-3.755  C64.409,50.626,62.729,50.584,61.667,51.598z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Stickers</h3>
                           <p>Users can include texts and stickers to their music-dubbed videos to make the content interesting and unique.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                              <g transform="translate(0,-952.36218)">
                                 <path d="m 28.503299,958.40873 0,3.0333 c -0.01,0.5342 0.4718,1.0254 1.0003,1.0254 0.5284,0 1.0077,-0.4912 1.0003,-1.0254 l 0,-3.0333 c 0.02,-0.573 -0.5295,-1.0967 -1.0941,-1.0427 -0.6125,0.083 -0.8675,0.5446 -0.9065,1.0427 z m -5.1765,0.2199 c -0.3743,0.3876 -0.4526,1.1046 -0.044,1.4863 l 2.0005,2.0222 c 0.3687,0.3789 1.0447,0.3855 1.4205,0.014 0.3758,-0.3716 0.3841,-1.055 0.017,-1.4358 l -2.0005,-2.0222 c -0.61,-0.4912 -1.0559,-0.331 -1.3943,-0.064 z m 10.9593,0.064 -2.0005,2.0222 c -0.3674,0.3807 -0.3595,1.0648 0.017,1.4366 0.3761,0.3719 1.0528,0.3649 1.4212,-0.015 l 2.000599,-2.0222 c 0.3825,-0.3725 0.2891,-1.0229 -0.048,-1.414 -0.535399,-0.4571 -1.019599,-0.2924 -1.389399,-0.01 z m -15.2853,5.7823 c -1.6451,0 -3.0008,1.3704 -3.0008,3.0334 l 0,66.73367 c 0,1.663 1.3557,3.0333 3.0008,3.0333 l 33.039899,0 c 3.9663,3.0976 10.4856,6.9285 15.4729,9.9533 0.3797,0.2269 0.9082,0.1584 1.219,-0.1581 l 15.004,-15.1667 c 0.3385,-0.36 0.3528,-0.9831 0.031,-1.3586 l -6.8456,-7.8996 -4.9388,-24.01397 c -0.026,-0.1354 -0.079,-0.2653 -0.1563,-0.3791 l -9.815,-14.4399 0,-16.3043 c 0,-1.663 -1.3557,-3.0334 -3.0008,-3.0334 -13.3368,0 -26.673899,0 -40.010499,0 z m 0,2.0223 40.010499,0 c 0.5715,0 1.0003,0.4335 1.0003,1.0111 l 0,14.1556 c -1.3895,0 -2.3896,0.9659 -3.0008,2.0538 l 0,-10.1427 c 0,-0.5294 -0.4765,-1.011 -1.0003,-1.0111 l -34.008899,0.031 c -0.5237,2e-4 -1.0002,0.4817 -1.0002,1.0111 l 0,49.51307 c 0,0.5294 0.4765,1.011 1.0002,1.0111 l 34.008899,0 c 0.5238,-10e-5 1.0003,-0.4817 1.0003,-1.0111 l 0,-30.74417 c 0.3293,1.0719 0.694,2.1844 1.0628,3.3492 0.9979,3.1524 1.938,6.49857 1.938,8.18377 l 0,26.289 c -0.5562,2.444 -1.258,3.7158 -3.7197,5.0556 l -37.291099,0 c -0.5715,0 -1.0002,-0.4335 -1.0002,-1.0111 l 0,-66.73367 c 0.078,-0.7602 0.4754,-0.9974 1.0002,-1.0105 z m 10.5028,1.9907 c -0.8287,0 -1.5004,0.679 -1.5004,1.5166 0,0.8376 0.6717,1.5168 1.5004,1.5168 0.8286,0 1.5004,-0.6792 1.5004,-1.5168 0,-0.8376 -0.6718,-1.5166 -1.5004,-1.5166 z m 25.506699,6.0982 0,42.27727 c -0.3072,-1.6081 -1.2864,-2.9629 -2.4382,-3.6021 -1.9009,-0.9574 -3.8336,-1.8793 -5.7515,-2.8116 0.3742,-0.4484 1.2472,-1.1572 1.7817,-1.6115 0.5268,-0.4926 1.1253,-1.1352 1.1253,-2.0854 0,-0.8841 -0.2305,-1.5758 -0.4689,-2.0855 -0.1813,-0.3877 -0.3206,-0.6223 -0.4376,-0.8847 1.3399,-2.7867 2.3444,-5.31757 2.3444,-8.31007 0,-5.8793 -4.2529,-11.3118 -10.1589,-11.3118 -3.7219,-0.069 -7.096499,1.1034 -8.377199,4.2024 -1.7579,0.6763 -3.1327,1.9193 -3.9698,3.602 -0.7873,1.6292 -1.227,4.5187 -1.1566,6.4769 0.017,4.70797 3.7332,8.17587 7.0331,10.64827 -2.9421,1.4005 -5.9202,2.7461 -8.8461,4.1703 -2.0199,1.1625 -2.4016,2.8607 -2.6882,4.7712 l 0,-43.41487 32.008399,-0.031 z m 4.5949,9.258 10.4715,15.4195 4.9701,24.04567 c 0.03,0.1736 0.1067,0.3388 0.2188,0.4738 l 6.3767,7.3623 -13.7849,13.9345 -11.8504,-7.7073 c 1.3129,-0.4731 2.1905,-0.8442 3.1919,-1.7719 1.6488,-1.5278 2.8132,-3.4372 2.8132,-5.4032 l 0,-26.289 c 0,-2.3593 -1.0604,-5.61687 -2.063,-8.78407 -0.7633,-2.5449 -1.5881,-4.784 -1.938,-7.3938 0.097,-1.612 0.4904,-2.8823 1.5941,-3.8865 z m -22.3808,11.2171 c 1.055,1.1633 2.2999,1.5695 3.6884,1.7695 -0.5175,0.013 -0.7796,0.1249 -0.8753,0.5055 l -1.094,0 c -0.1604,-0.628 -0.7178,-0.474 -2.2506,-0.474 -0.2034,0 -0.4255,0.014 -0.6251,0.031 0.3669,-0.6757 0.7994,-1.3044 1.1566,-1.832 z m -1.3754,0.1896 c -0.2725,0.6114 -0.4903,1.2006 -0.6252,1.7695 -0.949499,0.2167 -1.740899,0.6376 -2.062999,1.2322 -0.1235,-0.629 -0.1951,-1.2714 -0.2188,-1.9274 0.9226,-0.8174 1.8147,-0.9244 2.906999,-1.0743 z m 3.0946,3.0965 1.1253,0 c 0.2608,1.1764 1.3946,2.52787 3.032,2.52787 0.6889,0 1.2678,-0.1232 1.7192,-0.3161 -0.3382,0.9747 -0.8293,1.9605 -1.4379,2.8754 -1.0687,1.6066 -2.4969,2.8675 -3.8135,3.5705 -1.3837,-0.714 -3.0299,-2.0247 -4.3136,-3.8548 -0.597199,-0.8515 -1.082899,-1.8003 -1.469099,-2.8122 0.4908,0.337 1.2038,0.5372 2.125499,0.5372 1.6374,0 2.7712,-1.35147 3.0321,-2.52787 z m -1.3129,4.96077 c 0.4814,1.4709 3.3946,1.4709 3.876,0 z m -0.9065,4.3289 c 0.8087,0.6464 1.6275,1.1629 2.4382,1.5166 0.2549,0.1145 0.5578,0.1145 0.8127,0 0.6846,-0.2987 1.3387,-0.7079 1.9692,-1.2006 -0.1664,1.2349 0.1384,2.1937 0.4377,3.2861 0.4321,1.2845 1.3855,2.6968 3.1258,3.3177 0.7682,0.2428 1.5704,-0.7872 1.1565,-1.485 -0.1263,-0.206 -0.2095,-0.4035 -0.2813,-0.5688 l 2.8758,1.3902 c -7.1895,2.3849 -12.7762,2.3542 -19.567699,-0.2843 l 6.439199,-3.0018 c 0.3488,-0.1567 0.591,-0.5305 0.5939,-0.9162 z m 2.2819,18.0105 c -2.2097,0 -4.001099,1.8108 -4.001099,4.0445 0,2.2336 1.791399,4.0445 4.001099,4.0445 2.2097,0 4.001,-1.8109 4.001,-4.0445 0,-2.2337 -1.7913,-4.0445 -4.001,-4.0445 z" style="text-indent:0;text-transform:none;direction:ltr;block-progression:tb;baseline-shift:baseline;color:#000000;enable-background:accumulate;"  fill-opacity="1" stroke="none" marker="none" visibility="visible" display="inline" overflow="visible"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Dubbing and Video Selfie </h3>
                           <p>Users can create their own custom audio-dubbed content using this notable feature.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <g>
                                 <path d="M23.5,65.5c3.7,0,7.1-1.3,9.8-3.5l25.8,14.8C59,77.4,59,77.9,59,78.5C59,87,66,94,74.5,94S90,87,90,78.5S83,63,74.5,63   c-5.1,0-9.6,2.5-12.5,6.3L38,55.5c0.7-1.7,1-3.6,1-5.5c0-1.6-0.3-3.2-0.7-4.7l24.3-13.9c2.8,3.4,7.1,5.6,11.9,5.6   C83,37,90,30,90,21.5S83,6,74.5,6S59,13,59,21.5c0,0.9,0.1,1.7,0.2,2.6L34,38.6c-2.8-2.5-6.4-4.1-10.5-4.1C15,34.5,8,41.5,8,50   S15,65.5,23.5,65.5z M74.5,71c4.1,0,7.5,3.4,7.5,7.5S78.6,86,74.5,86S67,82.6,67,78.5S70.4,71,74.5,71z M74.5,14   c4.1,0,7.5,3.4,7.5,7.5S78.6,29,74.5,29S67,25.6,67,21.5S70.4,14,74.5,14z M23.5,42.5c4.1,0,7.5,3.4,7.5,7.5s-3.4,7.5-7.5,7.5   S16,54.1,16,50S19.4,42.5,23.5,42.5z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Multi-Video/Picture Sharing </h3>
                           <p>Users can share their media or music-dubbed videos posted on their profiles to other social media platforms using this sharing option.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                              <rect x="68.417" y="11.576" width="14.721" height="6.232"></rect>
                              <rect x="47.019" y="11.558" width="16.148" height="18.972"></rect>
                              <rect x="68.417" y="24.254" width="14.721" height="6.23"></rect>
                              <path d="M94.394,2.586V0H36.342v2.261c-0.016,0.114-0.068,0.217-0.068,0.335v32.385H14.706v0.002  C9.084,35.004,4.534,39.682,4.519,45.462L4.5,45.49v50.639c0,1.761,1.172,3.192,2.617,3.192h0.002v0.307H59.9  c1.445,0,2.617-1.161,2.617-2.595V79.851h21.28c5.852,0,10.598-4.705,10.598-10.513V2.597C94.396,2.592,94.394,2.59,94.394,2.586z   M58.85,40.188c-0.916,1.559-1.481,3.347-1.481,5.28l-0.032,1.701h-6.697v-1.913c0.078-2.592,2.024-4.698,4.555-5.068H58.85z   M9.736,64.394h6.631v6.581H9.736V64.394z M50.639,64.394h6.626l0.007,6.581h-6.633V64.394z M57.255,52.379l0.004,6.807h-6.62  v-6.807H57.255z M16.367,59.186H9.736v-6.807h6.631V59.186z M9.736,76.186h6.631v6.579H9.736V76.186z M16.367,87.975v6.441H9.736  v-6.441H16.367z M50.639,87.975h6.645v6.441h-6.645V87.975z M57.281,82.765h-6.643v-6.579h6.638L57.281,82.765z M14.726,40.188  h1.641v6.98H9.736v-2.95C10.292,41.907,12.307,40.188,14.726,40.188z M62.518,50.19l0.084-4.723c0-2.927,2.396-5.302,5.346-5.302  c2.954,0,5.343,2.375,5.343,5.302l-0.084,23.871c0,1.951,0.569,3.755,1.504,5.32H62.518V50.19z M89.13,69.338  c0,2.929-2.391,5.302-5.343,5.302s-5.348-2.373-5.348-5.302l0.021-5.607c0.003-0.023,0.014-0.044,0.014-0.069l0.084-18.194  c0-5.706-4.591-10.326-10.308-10.481l0.003-0.004h-0.051c-0.079-0.005-0.157-0.025-0.238-0.025c-0.083,0-0.161,0.021-0.24,0.025  H55.982v-0.081c-0.27,0-0.522,0.06-0.787,0.081H41.512V5.193H89.16v64.145H89.13z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Attach Media</h3>
                           <p>Users can share their pictures or videos with other users, groups, or channels using this media attachment feature.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  fill="#000000" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 32 32" x="0px" y="0px">
                              <title>expand-media</title>
                              <path d="M16.09,16.55a2,2,0,0,0-3.35,1.39V23a2,2,0,0,0,3.35,1.39l2.53-2.53a2,2,0,0,0,0-2.78Zm-1.27,6.16V18.23l2.24,2.24Z"></path>
                              <path d="M23.4,12.44H17.76L22,7.49a2,2,0,1,0-2-2,2.05,2.05,0,0,0,.22.89l-4.67,5.45L11.88,7.18A1.93,1.93,0,0,0,12,6.49a2,2,0,1,0-2,2l.23,0,3.12,4H8.6a3.13,3.13,0,0,0-3.13,3.13v9.81A3.13,3.13,0,0,0,8.6,28.5H23.4a3.13,3.13,0,0,0,3.13-3.12V15.57A3.13,3.13,0,0,0,23.4,12.44Zm1,12.94a1,1,0,0,1-1,1H8.6a1,1,0,0,1-1-1V15.57a1,1,0,0,1,1-1.05H23.4a1,1,0,0,1,1,1.05Z"></path>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Channel Creation </h3>
                           <p>Users can create their own channels where they can post their videos regularly. It can either be set for public viewing or restricted access to friends or followers.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" x="0px" y="0px" viewBox="0 0 100 100">
                              <g transform="translate(0,-952.36218)">
                                 <path style="opacity:1;stroke:none;stroke-width:6;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate" d="M 38 14 C 34.17383 14 31 17.17383 31 21 L 31 24 L 12 24 C 8.1738269 24 5 27.17383 5 31 L 5 67 C 5 70.8262 8.1738269 74 12 74 L 17 74 L 17 83 A 3.0003 3.0003 0 0 0 21.8125 85.40625 L 37.03125 74 L 62 74 C 65.347901 74 68.183949 71.57699 68.84375 68.40625 L 78.1875 75.40625 A 3.0003 3.0003 0 0 0 83 73 L 83 64 L 88 64 C 91.82617 64 95 60.8262 95 57 L 95 21 C 95 17.17383 91.82617 14 88 14 L 38 14 z M 38 20 L 88 20 C 88.605624 20 89 20.39437 89 21 L 89 57 C 89 57.6056 88.605624 58 88 58 L 80 58 A 3.0003 3.0003 0 0 0 77 61 L 77 67 L 69 61 L 69 31 C 69 27.17383 65.826173 24 62 24 L 37 24 L 37 21 C 37 20.39437 37.394376 20 38 20 z M 12 30 L 62 30 C 62.605621 30 63 30.39438 63 31 L 63 67 C 63 67.6056 62.605621 68 62 68 L 36 68 A 3.0003 3.0003 0 0 0 34.1875 68.59375 L 23 77 L 23 71 A 3.0003 3.0003 0 0 0 20 68 L 12 68 C 11.394379 68 11 67.6056 11 67 L 11 31 C 11 30.39438 11.394379 30 12 30 z M 23 45 C 20.790911 45 19 46.790911 19 49 C 19 51.209089 20.790911 53 23 53 C 25.209089 53 27 51.209089 27 49 C 27 46.790911 25.209089 45 23 45 z M 37 45 C 34.790911 45 33 46.790911 33 49 C 33 51.209089 34.790911 53 37 53 C 39.209089 53 41 51.209089 41 49 C 41 46.790911 39.209089 45 37 45 z M 51 45 C 48.790911 45 47 46.790911 47 49 C 47 51.209089 48.790911 53 51 53 C 53.209089 53 55 51.209089 55 49 C 55 46.790911 53.209089 45 51 45 z " transform="translate(0,952.36218)"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Group Chat  </h3>
                           <p>Users can form groups with other users who share the same interests. They can send and receive messages, media content like photos, and videos.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                              <switch>
                                 <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                 <g i:extraneous="self">
                                    <g>
                                       <path d="M5273.1,2400.1v-2c0-2.8-5-4-9.7-4s-9.7,1.3-9.7,4v2c0,1.8,0.7,3.6,2,4.9l5,4.9c0.3,0.3,0.4,0.6,0.4,1v6.4     c0,0.4,0.2,0.7,0.6,0.8l2.9,0.9c0.5,0.1,1-0.2,1-0.8v-7.2c0-0.4,0.2-0.7,0.4-1l5.1-5C5272.4,2403.7,5273.1,2401.9,5273.1,2400.1z      M5263.4,2400c-4.8,0-7.4-1.3-7.5-1.8v0c0.1-0.5,2.7-1.8,7.5-1.8c4.8,0,7.3,1.3,7.5,1.8C5270.7,2398.7,5268.2,2400,5263.4,2400z"></path>
                                       <path d="M5268.4,2410.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1c0-0.6-0.4-1-1-1H5268.4z"></path>
                                       <path d="M5272.7,2413.7h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2414.1,5273.3,2413.7,5272.7,2413.7z"></path>
                                       <path d="M5272.7,2417h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2417.5,5273.3,2417,5272.7,2417z"></path>
                                    </g>
                                    <g>
                                       <path d="M56.3,57c2.8,0,5-2.3,5-5V31.2c0-2.8-2.3-5-5-5c-2.8,0-5,2.3-5,5V52C51.3,54.8,53.5,57,56.3,57z"></path>
                                       <circle cx="56.3" cy="67.7" r="6.1"></circle>
                                       <path d="M84.4,19.9C75.5,11.5,63.4,7.6,51.1,9.1c-17.3,2.1-31.8,15.5-35.3,32.5c-1.9,9.7-0.6,19.2,4,27.8L5,80.1     c-2.2,1.6-3,4.4-2.2,6.9s3.2,4.2,5.9,4.2h46.2c22.7,0,41.3-17.1,42.5-39C98.1,40,93.4,28.2,84.4,19.9z M54.9,83.7H12.8L24.8,75     c2.8-2,3.6-5.8,1.9-8.9c-3.9-7-5.1-15-3.4-23.1C26,29.1,37.9,18.2,52,16.5c10-1.2,20,2,27.3,8.8c7.3,6.8,11.2,16.5,10.6,26.5     C89,69.7,73.6,83.7,54.9,83.7z"></path>
                                    </g>
                                 </g>
                              </switch>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Report User</h3>
                           <p>Users can report users, profiles, or channels that are suspicious or share inappropriate content on the TikTok clone app.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
               </div>
            </div>
         </section>
         <!--  -->
         <!--  -->
         <section id="features" class="section features-area style-two overflow-hidden pb_100">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col-12 col-md-10 col-lg-7">
                     <!-- Section Heading -->
                     <div class="section-heading text-center">
                        <span class="d-inline-block rounded-pill shadow-sm fw-5 px-4 py-2 mb-3">
                           <svg class="svg-inline--fa fa-lightbulb fa-w-11 text-primary mr-1" aria-hidden="true" focusable="false" data-prefix="far" data-icon="lightbulb" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512" data-fa-i2svg="">
                              <path fill="currentColor" d="M176 80c-52.94 0-96 43.06-96 96 0 8.84 7.16 16 16 16s16-7.16 16-16c0-35.3 28.72-64 64-64 8.84 0 16-7.16 16-16s-7.16-16-16-16zM96.06 459.17c0 3.15.93 6.22 2.68 8.84l24.51 36.84c2.97 4.46 7.97 7.14 13.32 7.14h78.85c5.36 0 10.36-2.68 13.32-7.14l24.51-36.84c1.74-2.62 2.67-5.7 2.68-8.84l.05-43.18H96.02l.04 43.18zM176 0C73.72 0 0 82.97 0 176c0 44.37 16.45 84.85 43.56 115.78 16.64 18.99 42.74 58.8 52.42 92.16v.06h48v-.12c-.01-4.77-.72-9.51-2.15-14.07-5.59-17.81-22.82-64.77-62.17-109.67-20.54-23.43-31.52-53.15-31.61-84.14-.2-73.64 59.67-128 127.95-128 70.58 0 128 57.42 128 128 0 30.97-11.24 60.85-31.65 84.14-39.11 44.61-56.42 91.47-62.1 109.46a47.507 47.507 0 0 0-2.22 14.3v.1h48v-.05c9.68-33.37 35.78-73.18 52.42-92.16C335.55 260.85 352 220.37 352 176 352 78.8 273.2 0 176 0z"></path>
                           </svg>
                           <!-- <i class="far fa-lightbulb text-primary mr-1"></i> -->
                           <span class="text-primary">Admin</span>
                        </span>
                        <h2>Admin Features</h2>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                              <g>
                                 <path d="M5.1,5v90h90V5H5.1z M91.1,9v36.3h-23h-4h-55V9H91.1z M9.1,49.3h55V91h-55V49.3z M68.1,91V49.3h23V91H68.1z"></path>
                                 <path d="M35,65.2c-2.7,0.4-5.7,2-7.6,4.4l-9.8-5.7c3.8-5.7,10.2-9.6,17.3-10.1V65.2z M57,66.4l-9.9,5.7c0.6,1.4,0.9,2.8,0.9,4.4   c0,1.6-0.3,3-0.9,4.4l9.9,5.7c1.5-3,2.4-6.5,2.4-10.1C59.4,72.8,58.5,69.4,57,66.4z M26.2,72.1l-9.9-5.7c-1.5,3-2.4,6.5-2.4,10.1   c0,3.6,0.9,7.1,2.4,10.1l9.9-5.7c-0.6-1.4-0.9-2.8-0.9-4.4C25.3,74.9,25.7,73.4,26.2,72.1z M44.1,67.7c0.2-0.3,0.3-0.7,0.5-1   c0-0.1,0.1-0.2,0.1-0.3c0.4-0.9,0.8-1.8,1.2-2.6c0-0.1,0.1-0.1,0.1-0.2c0.2-0.4,0.3-0.7,0.5-1.1c0-0.1,0.1-0.2,0.1-0.2   c0.2-0.4,0.4-0.9,0.6-1.3c0,0,0,0,0,0c0.2-0.4,0.3-0.8,0.5-1.1c0-0.1,0.1-0.2,0.1-0.2c0.2-0.4,0.3-0.7,0.4-1c0,0,0-0.1,0-0.1   c0.2-0.4,0.3-0.7,0.4-1.1c0,0,0,0,0-0.1l0,0c0.6-1.5,1.1-2.7,1.3-3.4c-0.5,0.6-1.2,1.6-2.2,2.9c0,0,0,0,0,0c0,0,0,0-0.1,0.1   c-0.2,0.3-0.4,0.6-0.7,0.9c0,0,0,0,0,0c-0.2,0.3-0.4,0.6-0.7,0.9c-0.1,0.1-0.1,0.2-0.2,0.2c-0.2,0.3-0.4,0.6-0.6,0.9   c0,0.1-0.1,0.1-0.1,0.2c-0.3,0.4-0.5,0.7-0.8,1.1c0,0.1-0.1,0.1-0.1,0.2c-0.2,0.3-0.4,0.6-0.6,0.9c-0.1,0.1-0.2,0.2-0.2,0.3   c-0.6,0.8-1.1,1.5-1.7,2.3c-0.1,0.1-0.1,0.2-0.2,0.2c-0.2,0.3-0.4,0.6-0.6,0.9C41,65.8,41,65.9,40.9,66l0,0   c-3.4,4.6-6.4,8.9-6.8,9.5c-0.8,1.4-0.3,3.1,1,3.9c1.4,0.8,3.1,0.3,3.9-1c0.4-0.6,2.6-5.3,4.9-10.5l0,0C44,67.8,44,67.7,44.1,67.7z    M49.5,57.7c-1.2,3-3,7-4.7,10.8c0.3,0.3,0.6,0.7,0.9,1.1l9.8-5.7C53.9,61.5,51.8,59.4,49.5,57.7z M47,56.2c-2.7-1.4-5.9-2.2-9-2.4   v11.4c0.7,0.1,1.4,0.2,2,0.4C42.5,62.3,45.1,58.8,47,56.2z"></path>
                                 <path d="M19.5,41h-6V17.3h6V41z M29,13.2h-6V41h6V13.2z M38.5,25.7h-6V41h6V25.7z M48,21h-6v20h6V21z M57.5,17.3h-6V41h6V17.3z    M67,25.7h-6V41h6V25.7z M76.5,32.1h-6V41h6V32.1z M86,29h-6v12h6V29z"></path>
                                 <rect x="71" y="53.5" width="17" height="4.9"></rect>
                                 <rect x="71" y="63.2" width="12.8" height="4.9"></rect>
                                 <rect x="71" y="72.5" width="9.1" height="4.9"></rect>
                                 <rect x="71" y="81.7" width="12.8" height="4.9"></rect>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Intuitive dashboard</h3>
                           <p>The admin can view and manage the entire business processes taking place in the app via the powerful panel.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75.1 91.62" x="0px" y="0px">
                              <title>Livello 5</title>
                              <g data-name="Livello 2">
                                 <g data-name="Livello 5">
                                    <g data-name="Livello 2">
                                       <g data-name="Livello 8">
                                          <path d="M67.12,49.61a2.2,2.2,0,0,0-3,.89,2.13,2.13,0,0,0-.27,1v3.37a28,28,0,0,0-20.78-9.45H27.75A27.78,27.78,0,0,0,0,73.21v5.41a7.5,7.5,0,0,0,7.49,7.47h45.7v5.49h2.5V77a2.19,2.19,0,0,0-1.29-2,13,13,0,0,1-1.23-23V64.26l6.6,3.82,6.6-3.82V52.06A13,13,0,0,1,65.13,75a2.22,2.22,0,0,0-1.26,2V91.62h2.5V77.13a15.49,15.49,0,0,0,.75-27.48ZM44.19,63.17a15.48,15.48,0,0,0,9,14v4.89H7.49A3.49,3.49,0,0,1,4,78.62V73.21A23.78,23.78,0,0,1,27.75,49.47H43.09a23.26,23.26,0,0,1,7.44,1.23A15.44,15.44,0,0,0,44.19,63.17Zm15.58,2-4.1-2.36V53.13a24.14,24.14,0,0,1,8.2,8.63v1Z"></path>
                                          <path d="M35.42,42.14c9.82,0,17.74-11.57,17.74-21.14V17.74a17.74,17.74,0,1,0-35.48,0V21C17.69,30.57,25.6,42.14,35.42,42.14ZM21.68,17.74a13.75,13.75,0,0,1,27.49,0V21c0,7.71-6.35,17.14-13.75,17.14S21.68,28.71,21.68,21Z"></path>
                                       </g>
                                    </g>
                                 </g>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Manage users</h3>
                           <p>The admin can manage users registered with their platform. They can block or suspend users’ profiles if other users report them.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 58 53.27" enable-background="new 0 0 58 53.27" xml:space="preserve">
                              <g>
                                 <path d="M49.28,27.87c-1.13-1.13-3.09-1.13-4.22,0L29.66,43.26l-2.12,7.78l-0.52,0.52   c-0.39,0.39-0.39,1.01-0.01,1.41l-0.01,0.02l0.02-0.01c0.19,0.19,0.45,0.29,0.7,0.29c0.26,0,0.51-0.1,0.71-0.29l0.52-0.52   l7.78-2.12l15.39-15.39c1.16-1.16,1.16-3.06,0-4.22L49.28,27.87z M31.97,43.78l10.24-10.24l4.24,4.24L36.22,48.03L31.97,43.78z    M31.07,45.71l3.22,3.22l-4.43,1.21L31.07,45.71z M50.71,33.53l-2.84,2.84l-4.24-4.24l2.84-2.84c0.37-0.37,1.02-0.37,1.39,0   l2.85,2.85C51.1,32.52,51.1,33.15,50.71,33.53z"></path>
                                 <path d="M37,23c0-0.34-0.17-0.66-0.46-0.84l-11-7c-0.31-0.2-0.7-0.21-1.02-0.03C24.2,15.3,24,15.63,24,16v14   c0,0.37,0.2,0.7,0.52,0.88C24.67,30.96,24.83,31,25,31c0.19,0,0.37-0.05,0.54-0.16l11-7C36.83,23.66,37,23.34,37,23z M26,28.18   V17.82L34.14,23L26,28.18z"></path>
                                 <path d="M57,0H47H11H1C0.45,0,0,0.45,0,1v11v11v11v11c0,0.55,0.45,1,1,1h10h12c0.55,0,1-0.45,1-1s-0.45-1-1-1H12V34   V23V12V2h34v10v11c0,0.55,0.45,1,1,1h9v8c0,0.55,0.45,1,1,1s1-0.45,1-1v-9V12V1C58,0.45,57.55,0,57,0z M2,24h8v9H2V24z M10,22H2v-9   h8V22z M2,44v-9h8v9H2z M10,11H2V2h8V11z M56,2v9h-8V2H56z M48,22v-9h8v9H48z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Manage channels</h3>
                           <p>The admin can manage the channels created on the app. They can also block or suspend them if they find it suspicious.</p>
                        </div>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                              <g>
                                 <path d="M37.9,24.8c-1.8,0-3.5,0.4-5,1.2c0-13.5,0-14.4,0-22c0-1.7-1.3-3-3-3H4C2.3,1,1,2.3,1,4c0,13.4,0,29.3,0,42   c0,1.7,1.3,3,3,3h26c1.7,0,3-1.3,3-3v-0.2c1.5,0.8,3.2,1.2,5,1.2c6,0,11-5,11-11.1S44,24.8,37.9,24.8z M26.9,34.7   c-0.8-0.5-1.7-1-2.7-1.3c-1.9-0.6-3.1-1.9-3.1-3.2v-0.9c2.1-2,3.5-5.4,3.5-8.6c0-4.6-3-7.5-7.6-7.5s-7.6,3-7.6,7.5   c0,3.2,1.3,6.6,3.5,8.6v0.9c0,1.3-1.2,2.6-3.1,3.2C6.3,34.6,4,37,3.7,39.9H3V9.1h28v18.2C28.8,29.1,27.3,31.7,26.9,34.7z    M19.5,28.1C19.5,28.1,19.4,28.1,19.5,28.1c-0.8,0.7-1.6,1-2.5,1s-1.7-0.4-2.4-1c0,0-0.1,0-0.1-0.1c-1.8-1.6-3.1-4.7-3.1-7.4   c0-5,3.9-5.5,5.6-5.5s5.6,0.5,5.6,5.5C22.6,23.4,21.3,26.5,19.5,28.1z M14.8,30.6c0.7,0.3,1.4,0.5,2.2,0.5s1.5-0.2,2.2-0.5   c0.2,2,1.9,3.8,4.5,4.7c1.4,0.5,2.5,1.1,3.3,2c0.1,0.9,0.3,1.8,0.6,2.6h-22c0.3-1.9,2.1-3.7,4.7-4.6S14.6,32.7,14.8,30.6z M4,3h26   c0.6,0,1,0.4,1,1v3.1H3V4C3,3.4,3.4,3,4,3z M31,46c0,0.6-0.4,1-1,1H4c-0.6,0-1-0.4-1-1v-4.1h25.6c0.6,1,1.4,1.9,2.3,2.6V46H31z    M37.9,45c-5,0-9.1-4.1-9.1-9.1s4.1-9.1,9.1-9.1s9.1,4.1,9.1,9.1S42.9,45,37.9,45z"></path>
                                 <path d="M41.2,32.8L37,36.9l-2.3-2.3c-0.4-0.4-1-0.4-1.4,0s-0.4,1,0,1.4l3,3c0.2,0.2,0.4,0.3,0.7,0.3s0.5-0.1,0.7-0.3l4.9-4.9   c0.4-0.4,0.4-1,0-1.4C42.2,32.4,41.6,32.4,41.2,32.8z"></path>
                                 <path d="M19.2,43.4h-4.4c-0.6,0-1,0.4-1,1s0.4,1,1,1h4.4c0.6,0,1-0.4,1-1S19.7,43.4,19.2,43.4z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Verify profiles </h3>
                           <p>The admin can verify the profiles of users who request for the verified badge. They can either accept or reject the requests after a thorough analysis.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4 res-margin">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px'   xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                              <title>Music</title>
                              <g>
                                 <path d="M19.86,33.13A7.34,7.34,0,0,0,14.65,31c-.22,0-.43,0-.65,0V26.28A15.61,15.61,0,0,0,20,14a4,4,0,0,0-1.17-2.83,4.1,4.1,0,0,0-5.66,0A4,4,0,0,0,12,14V25.26a11.72,11.72,0,0,1-1.5.87L7.61,27.58A10.08,10.08,0,0,0,2,36.65v.2A10.16,10.16,0,0,0,12,47v3.58A1.44,1.44,0,0,1,10.51,52,1.51,1.51,0,0,1,9,50.5a1,1,0,0,0-2,0A3.52,3.52,0,0,0,10.57,54,3.43,3.43,0,0,0,14,50.58V47a8.56,8.56,0,0,0,4.34-1.59,1,1,0,0,0-1.16-1.62A6.69,6.69,0,0,1,14,45V33c.21,0,.43,0,.65,0a5.35,5.35,0,0,1,3.8,1.55,5.27,5.27,0,0,1,1.44,2.66,1,1,0,0,0,2-.4A7.28,7.28,0,0,0,19.86,33.13ZM14,14a2,2,0,0,1,4,0,13.63,13.63,0,0,1-4,9.62ZM4,36.85v-.2a8.09,8.09,0,0,1,4.51-7.28l2.89-1.45.6-.31V31.5a7.63,7.63,0,0,0-5,7.15V39a1,1,0,0,0,2,0v-.35a5.64,5.64,0,0,1,3-5V45A8.17,8.17,0,0,1,4,36.85Z"></path>
                                 <path d="M20.88,40a1,1,0,0,0-1.29.59,4.69,4.69,0,0,1-.18.45,1,1,0,0,0,.44,1.34.93.93,0,0,0,.45.11,1,1,0,0,0,.89-.55,6.06,6.06,0,0,0,.28-.65A1,1,0,0,0,20.88,40Z"></path>
                                 <path d="M61,29H35V23h3a4,4,0,0,0,8,0H61a1,1,0,0,0,0-2H46V11a1,1,0,0,0-.49-.86,1,1,0,0,0-1,0l-11,6A1,1,0,0,0,33,17v4H25a1,1,0,0,0,0,2h8v2.56A3.91,3.91,0,0,0,31,25a4,4,0,0,0-4,4H25a1,1,0,0,0,0,2h2.56a4,4,0,0,0,6.88,0H61a1,1,0,0,0,0-2ZM42,25a2,2,0,1,1,2-2A2,2,0,0,1,42,25Zm-7-7.41,9-4.91v6.88A3.91,3.91,0,0,0,42,19a4,4,0,0,0-3.44,2H35ZM31,31a2,2,0,1,1,2-2A2,2,0,0,1,31,31Z"></path>
                                 <path d="M61,39a1,1,0,0,0,0-2H55v-.43l4.51-2.71a1,1,0,0,0-1-1.72l-5,3A1,1,0,0,0,53,36v1H25a1,1,0,0,0,0,2H53v5.56A3.91,3.91,0,0,0,51,44a4,4,0,0,0-2.62,1H25a1,1,0,0,0,0,2H47.14A4.09,4.09,0,0,0,47,48a4,4,0,0,0,8,0V47h6a1,1,0,0,0,0-2H55V39ZM51,50a2,2,0,1,1,2-2A2,2,0,0,1,51,50Z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Upload music tracks </h3>
                           <p>The admin can upload an unlimited number of songs and audios to the platform for user access. </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-12 col-md-6 col-lg-4">
                     <!-- Image Box -->
                     <div class="image-box text-center icon-1 p-5 wow fadeInRight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInRight;">
                        <!-- Featured Image -->
                        <div class="featured-img mb-3">
                           <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 100 100" x="0px" y="0px">
                              <title>06</title>
                              <g data-name="Group">
                                 <path data-name="Path" d="M11.09,23.15a38.81,38.81,0,0,0-1.64,31l3.75-1.41a34.81,34.81,0,0,1,1.46-27.79Z"></path>
                                 <path data-name="Path" d="M88.91,23.15l-3.57,1.79A34.81,34.81,0,0,1,86.8,52.73l3.75,1.41a38.81,38.81,0,0,0-1.64-31Z"></path>
                                 <path data-name="Path" d="M18.5,33.47A30.3,30.3,0,0,0,18.17,46l3.93-.72a26.28,26.28,0,0,1,.29-10.83Z"></path>
                                 <path data-name="Path" d="M81.83,46a30.29,30.29,0,0,0-.34-12.49l-3.89.93a26.28,26.28,0,0,1,.29,10.83Z"></path>
                                 <path data-name="Compound Path" d="M84.29,63.14H78.41c-4.61-4.87-6.78-10-6.78-16.1V40.51A21.44,21.44,0,0,0,60.21,21.59V18.27a10.21,10.21,0,1,0-20.42,0v3.45A22.57,22.57,0,0,0,28.37,41.24V47c0,6.09-2.16,11.23-6.78,16.1H15.71V81.45H39.07v10.5H60.93V81.45H84.29ZM43.79,18.27a6.21,6.21,0,1,1,12.42,0V20c-.26-.08-.52-.17-.78-.24A20.46,20.46,0,0,0,50.06,19H50c-.7,0-1.4,0-2.07.11a16.79,16.79,0,0,0-3.35.65c-.26.07-.52.16-.78.24ZM32.37,47v-5.8A18.28,18.28,0,0,1,45.63,23.63l.1,0a12.84,12.84,0,0,1,2.58-.5A16.86,16.86,0,0,1,50,23a17.06,17.06,0,0,1,4.4.58A17.5,17.5,0,0,1,67.63,40.51V47a25.37,25.37,0,0,0,5.51,16.1H26.86A25.37,25.37,0,0,0,32.37,47Zm24.56,40.9H43.07v-6.5H56.93Zm23.36-10.5H19.71V67.14H80.29Z"></path>
                              </g>
                           </svg>
                        </div>
                        <!-- Icon Text -->
                        <div class="icon-text">
                           <h3 class="mb-2">Manage notifications</h3>
                           <p>The admin can manage the push notifications that are shared with the app users concerning the trending videos, app updates, and more.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--  -->
         <!-- Call To Action Section -->
         <div class="section bg-indigo demo_sec ui-section-tilt ui-action-section">
            <div class="container">
               <div class="row">
                  <!-- Text Column -->
                  <div class="col-md-6 col-sm-7 text-block">
                     <!-- Section Heading -->
                     <div class="section-heading">
                        <h2 class="heading">
                           Demo Of Our White-Label TikTok Clone
                        </h2>
                        <h3>Available on iOS and Android</h3>
                        <div class="actions">
                           <a href="#" data-scrollto="contact"  class="btn ui-gradient-blue btn-app-store btn-download shadow-lg"><span>Request Demo</span> <span>App Store</span></a>
                           <a href="#" data-scrollto="contact"  class="btn ui-gradient-green btn-google-play btn-download shadow-lg"><span>Request Demo </span> <span>Google Play</span></a>
                        </div>
                     </div>
                     <!-- .section-heading -->
                  </div>
                  <!-- Image Column -->
                  <div class="col-md-6 col-sm-5 img-block animate" data-show="fade-in-left">
                     <img src="assets/img/app-screens/double_3.png" alt="TikTok Clone" data-uhd class="responsive-on-sm" width="450" />
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!-- App Screens Section -->
         <div class="section" id="screens">
            <div class="container">
               <!-- Dection Heading -->
               <div class="section-heading center">
                  <h2 class="heading text-indigo">
                     A Glimpse Of Our TikTok Clone
                  </h2>
                  <p class="paragraph">
                     Take a look at the various screens of our TikTok clone app solution. It will get you a clear understanding of the app functionality.
                  </p>
               </div>
               <!-- .section-heading -->
               <!-- App Screens Carousel -->
               <div class="ui-app-screens owl-carousel owl-theme animate" data-show="fade-in">
                  <!-- App Screen 1 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/1-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 2 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/2-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 3 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/13-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 4 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/3-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 5 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/5-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 6 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/7-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <!-- App Screen 8 -->
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/9-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
                  <div class="ui-card shadow-lg">
                     <img src="assets/img/app-screens/11-screen.png" alt="TikTok Clone" data-uhd/>
                  </div>
               </div>
               <!-- .ui-app-screens -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!--  -->
         <!-- why choose -->
         <section id="whychoose" class="features-area section pb-70">
            <div class="container">
               <div class="section-heading text-center">
                  <h2 class="heading text-indigo">
                     Why Choose Our Custom-Built TikTok Clone Solution
                  </h2>
               </div>
               <div class="row">
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i class="icon-s">
                              <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enable-background="new 0 0 24 24" x="0px" y="0px">
                                 <g>
                                    <path d="M21.536 21h-19.072c-1.359 0-2.464-1.088-2.464-2.426v-12.148c0-1.338 1.105-2.426 2.464-2.426h10.27c.553 0 1 .448 1 1s-.447 1-1 1h-10.27c-.256 0-.464.191-.464.426v12.148c0 .235.208.426.464.426h19.072c.256 0 .464-.191.464-.426v-10.717c0-.552.447-1 1-1s1 .448 1 1v10.717c0 1.338-1.105 2.426-2.464 2.426zM12 23.267c-.552 0-1-.447-1-1v-1.467c0-.553.448-1 1-1s1 .447 1 1v1.467c0 .552-.448 1-1 1zM16 24h-8c-.552 0-1-.447-1-1s.448-1 1-1h8c.553 0 1 .447 1 1s-.447 1-1 1zM8.698 17h-3.698c-.552 0-1-.447-1-1s.448-1 1-1h.765c.218 0 .355-.287.666-1.396.469-1.672 1.254-4.471 5.201-4.471.265 0 .52.105.707.293l2.2 2.2c.162.162.264.374.287.602.019.168.14 1.68-1.047 3-1.059 1.175-2.768 1.772-5.081 1.772zm-.602-2h.602c1.678 0 2.913-.375 3.571-1.086.387-.417.512-.88.552-1.177l-1.591-1.591c-1.964.129-2.381 1.245-2.873 2.999-.077.27-.159.563-.261.855zM14 13.2c-.254 0-.507-.096-.702-.288-.393-.388-.398-1.021-.01-1.415l8.572-8.697c.184-.184.184-.486.005-.665-.18-.179-.48-.179-.66 0l-8.703 8.578c-.393.388-1.026.382-1.415-.01-.388-.394-.383-1.027.01-1.415l8.698-8.572c.956-.957 2.521-.957 3.483.005s.962 2.526 0 3.488l-8.567 8.693c-.194.199-.452.298-.711.298z"></path>
                                 </g>
                              </svg>
                           </i>
                        </div>
                        <h3>Customizable solution</h3>
                        <p>Our TikTok clone app can be readily customized to suit your business needs efficiently. You can change its features, integrated third-party APIs, modify its functionality, and more with ease.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                                 <switch>
                                    <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                    <g i:extraneous="self">
                                       <g>
                                          <path d="M5273.1,2400.1v-2c0-2.8-5-4-9.7-4s-9.7,1.3-9.7,4v2c0,1.8,0.7,3.6,2,4.9l5,4.9c0.3,0.3,0.4,0.6,0.4,1v6.4     c0,0.4,0.2,0.7,0.6,0.8l2.9,0.9c0.5,0.1,1-0.2,1-0.8v-7.2c0-0.4,0.2-0.7,0.4-1l5.1-5C5272.4,2403.7,5273.1,2401.9,5273.1,2400.1z      M5263.4,2400c-4.8,0-7.4-1.3-7.5-1.8v0c0.1-0.5,2.7-1.8,7.5-1.8c4.8,0,7.3,1.3,7.5,1.8C5270.7,2398.7,5268.2,2400,5263.4,2400z"></path>
                                          <path d="M5268.4,2410.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1c0-0.6-0.4-1-1-1H5268.4z"></path>
                                          <path d="M5272.7,2413.7h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2414.1,5273.3,2413.7,5272.7,2413.7z"></path>
                                          <path d="M5272.7,2417h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2417.5,5273.3,2417,5272.7,2417z"></path>
                                       </g>
                                       <g>
                                          <path d="M86.1,45c-0.6,1.9-1.8,4.7-3.2,7.8c0.8,2.1,0.8,4.8-0.8,7.9c-0.4,0.7-0.8,1.5-1.4,2.2c-1.4,1.9-3.3,3.9-5.3,6.2     c-0.1,0.2-0.3,0.3-0.4,0.5c-0.5,0.5-1.3,0.7-2,0.3c-0.8-0.5-0.9-1.6-0.3-2.3c0.2-0.2,0.3-0.4,0.5-0.5c2.7-2.9,5.2-5.7,6.3-7.8     c2-3.9,0.7-6.5-1.3-7.7c-1.6-0.9-3.8-0.9-5,0.9c-1.7,2.6-4.1,5.3-6.4,7.9c-2.7,3-5.3,6.2-6.8,8.7c-1.7,2.8-3.8,9.5-4.2,16.5     c-0.2,3.4-0.3,7.4-0.4,9.7c0,1.2,0.9,2.2,2.2,2.2l13.9,0c1.2,0,2.1-1,2.1-2.2l0-3.9c0-0.9,0.5-1.8,1.2-2.3     c4.9-3.4,9.6-7.5,12.1-10.5c5.6-6.6,7.7-26.1,8-31.9C95.1,40.9,87.8,39.6,86.1,45z"></path>
                                          <path d="M40,69c-1.5-2.5-4.1-5.7-6.8-8.7c-2.3-2.6-4.7-5.3-6.4-7.9c-1.2-1.8-3.4-1.8-5-0.9c-1.9,1.1-3.3,3.7-1.3,7.7     c1.1,2.1,3.6,4.9,6.3,7.8c0.2,0.2,0.3,0.4,0.5,0.5c0.6,0.7,0.5,1.8-0.3,2.3c-0.6,0.4-1.5,0.2-2-0.3c-0.1-0.2-0.3-0.3-0.4-0.5     c-2.1-2.3-4-4.3-5.3-6.2c-0.6-0.8-1-1.5-1.4-2.2c-1.6-3-1.6-5.7-0.8-7.9c-1.4-3.1-2.6-5.9-3.2-7.8c-1.7-5.4-9.1-4.1-8.8,1.7     c0.3,5.8,2.4,25.3,8,31.9c2.6,3,7.2,7.1,12.1,10.5c0.8,0.5,1.2,1.4,1.2,2.3l0,3.9c0,1.2,1,2.2,2.1,2.2l13.9,0     c1.2,0,2.2-1,2.2-2.2c-0.1-2.4-0.2-6.3-0.4-9.7C43.9,78.5,41.7,71.9,40,69z"></path>
                                          <path d="M66.2,45.8l-2.8-11.7c-0.3-1.1,0.1-2.2,0.9-2.9l9.1-7.8c2-1.7,0.9-4.9-1.7-5.2l-12-1c-1.1-0.1-2-0.8-2.5-1.8L52.7,4.3     c-1-2.4-4.4-2.4-5.4,0l-4.6,11.1c-0.4,1-1.4,1.7-2.5,1.8l-12,1c-2.6,0.2-3.7,3.5-1.7,5.2l9.1,7.8c0.8,0.7,1.2,1.8,0.9,2.9     l-2.8,11.7c-0.6,2.5,2.2,4.5,4.4,3.2l10.3-6.3c0.9-0.6,2.1-0.6,3.1,0L61.8,49C64,50.3,66.8,48.3,66.2,45.8z"></path>
                                       </g>
                                    </g>
                                 </switch>
                              </svg>
                           </i>
                        </div>
                        <h3>Pre-packed features</h3>
                        <p>Our ready-made application is loaded with all necessary and advanced features needed to ensure its hassle-free operation. We conduct a thorough competitive analysis to finalize the feature set to integrate into the app.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve">
                                 <path d="M46.229,41.442c-0.026-0.012-2.552-1.104-2.087-3.018c0.492-2.024-0.28-5.328-2.388-7.135  c-1.194-1.023-3.26-1.959-6.431-0.849l-3.813-3.815c-0.019-0.05-0.028-0.102-0.055-0.149c-0.442-0.798-0.901-1.578-1.361-2.343  L43.82,10.408c0.64-0.641,0.992-1.492,0.992-2.397c0-0.906-0.354-1.756-0.993-2.395l-3.386-3.388c-1.281-1.279-3.514-1.279-4.793,0  L31.929,5.94c-0.001,0-0.001,0-0.003,0.001c0,0.001,0,0.002-0.002,0.002l-8.741,8.742c-4.23-4.853-8.73-8.443-13.749-10.943  C5.962,2.017,4.635,2.416,4.226,2.669C4.103,2.745,4,2.848,3.925,2.97C3.672,3.38,3.272,4.709,5,8.179  c2.377,4.776,6.16,9.506,10.945,13.744L3.963,33.905c-0.078,0.076-0.133,0.17-0.178,0.27c-0.011,0.023-0.021,0.046-0.031,0.07  c-0.007,0.021-0.02,0.04-0.026,0.062l-2.675,9.558c-0.089,0.319,0,0.662,0.235,0.896c0.174,0.175,0.408,0.27,0.649,0.27  c0.082,0,0.165-0.011,0.247-0.034l9.558-2.676c0.021-0.007,0.04-0.02,0.062-0.025c0.024-0.01,0.046-0.02,0.071-0.031  c0.099-0.045,0.191-0.102,0.269-0.178l13.243-13.244c0.773,0.476,1.377,0.821,2.233,1.297l4.072,4.074  c-0.157,1.076-0.181,3.52,1.915,6.561c1.738,2.527,4.61,3.625,7.467,3.625c1.86,0,3.712-0.465,5.24-1.307  c0.308-0.17,0.493-0.5,0.474-0.852C46.77,41.889,46.553,41.579,46.229,41.442z M32.69,32.614l-3.215-3.217l1.105-1.105l3.216,3.218  L32.69,32.614z M11.494,40.138l-2.142-2.143L36.017,11.33l2.143,2.144L11.494,40.138z M5.91,34.554L32.575,7.889l2.144,2.144  L8.054,36.697L5.91,34.554z M36.938,3.526c0.587-0.586,1.609-0.586,2.198,0l3.386,3.388c0.293,0.292,0.454,0.682,0.454,1.097  c0,0.415-0.161,0.806-0.455,1.099l-3.064,3.065l-2.791-2.792V9.383c-0.001,0-0.001,0-0.001-0.001l-2.792-2.792L36.938,3.526z   M6.644,7.361C5.848,5.764,5.611,4.806,5.55,4.295c0.511,0.061,1.468,0.297,3.066,1.092c4.832,2.405,9.174,5.882,13.266,10.599  l-4.637,4.637C12.594,16.523,8.931,11.955,6.644,7.361z M5.074,36.313l2.332,2.332l0,0l0,0l2.33,2.33L3.26,42.787L5.074,36.313z   M28.751,25.477c0.256,0.43,0.513,0.849,0.763,1.288l-1.495,1.494c-0.462-0.26-0.86-0.491-1.296-0.753L28.751,25.477z   M35.117,39.731c-1.893-2.75-1.709-4.754-1.595-5.352l2.079-2.079c2.047-0.805,3.717-0.682,4.958,0.384  c1.647,1.411,2.109,4.034,1.799,5.307c-0.438,1.811,0.484,3.223,1.559,4.141C40.871,43.111,37.067,42.563,35.117,39.731z"></path>
                              </svg>
                           </i>
                        </div>
                        <h3>Sleek design</h3>
                        <p>The user interface should be sleek and streamlined to allow your users to access the app with convenience. Our skilled design team takes the utmost care in this part of the app development.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                 <g>
                                    <path d="M494.9381714,315.7192688l-32.8100586-36.380127l-36.369873,32.8100586   c-4.1000977,3.7001953-4.4299316,10.0200195-0.7299805,14.1201172c3.6999512,4.1098633,10.0200195,4.4299316,14.1298828,0.7299805   l11.7200928-10.5700684c-13.3699951,92.6999512-98.0999756,159.170166-192,149.1901855   c-5.4899902-0.6000977-10.4100342,3.3898926-11,8.8798828c-0.5799561,5.5,3.4000244,10.4199219,8.8900146,11   c6.9799805,0.75,13.9199219,1.1098633,20.8000488,1.1098633c96.2399902,0,179.5100098-71.1899414,193.1799316-167.8498535   l9.3400879,10.3498535c1.9699707,2.1901855,4.6999512,3.3100586,7.4299316,3.3100586   c2.3898926,0,4.7800293-0.8498535,6.6899414-2.5800781C498.3082886,326.1394348,498.6383667,319.8193665,494.9381714,315.7192688z"></path>
                                    <path d="M268.4882202,256.4992981v10h210v-10c0-48.489975-33.0400391-89.410141-77.7900391-101.4199066   c20.0100098-10.0100098,33.7900391-30.7202148,33.7900391-54.5800781c0-33.6401367-27.3699951-61.0000038-61-61.0000038   c-33.6400146,0-61,27.3598671-61,61.0000038c0,23.8598633,13.7700195,44.5600586,33.7800293,54.5800781   C301.5182495,167.099411,268.4882202,208.0093231,268.4882202,256.4992981z"></path>
                                    <path d="M289.5783081,63.0493584l-32.0100098-37.0800781c-3.6101074-4.1799316-9.9200439-4.6499023-14.1000977-1.0400391   c-4.1799316,3.6101074-4.6499023,9.9299297-1.0400391,14.1101055l8.6000977,9.9599609   c-105.5-7.8000488-198.6700439,70.4399414-208.7399902,176.2399902c-0.5200195,5.5,3.5100098,10.380127,9.0100098,10.9099121   c0.3199463,0.0300293,0.6398926,0.0400391,0.9599609,0.0400391c5.0999756,0,9.4499512-3.8798828,9.9499512-9.0498047   c9.1001015-95.7602539,94.0500488-166.3701172,189.7099609-158l-12.4899902,10.7797852   c-4.1799316,3.6101074-4.6398926,9.9199219-1.0299072,14.1000977c1.9799805,2.2900391,4.7698975,3.4699707,7.5699463,3.4699707   c2.3200684,0,4.6500244-0.8000488,6.5300293-2.4299316L289.5783081,63.0493584z"></path>
                                    <path d="M146.6981812,378.0793762c20.0100098-10.0100098,33.7900391-30.7202148,33.7900391-54.5800781   c0-33.6401367-27.3699951-61-61-61c-33.6400146,0-61.0000038,27.3598633-61.0000038,61   c0,23.8598633,13.7700233,44.5600586,33.7800331,54.5800781c-44.7500038,12.0200195-77.7800293,52.9299316-77.7800293,101.4199219   v10h210v-10C224.4882202,431.0093079,191.4481812,390.0891418,146.6981812,378.0793762z"></path>
                                 </g>
                              </svg>
                           </i>
                        </div>
                        <h3>Referral program</h3>
                        <p>Business owners can offer reward points to users for every successful app referral. It is one of the great ways to boost your app reach via word of mouth marketing.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                                 <switch>
                                    <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                    <g i:extraneous="self">
                                       <g>
                                          <path d="M5273.1,2400.1v-2c0-2.8-5-4-9.7-4s-9.7,1.3-9.7,4v2c0,1.8,0.7,3.6,2,4.9l5,4.9c0.3,0.3,0.4,0.6,0.4,1v6.4     c0,0.4,0.2,0.7,0.6,0.8l2.9,0.9c0.5,0.1,1-0.2,1-0.8v-7.2c0-0.4,0.2-0.7,0.4-1l5.1-5C5272.4,2403.7,5273.1,2401.9,5273.1,2400.1z      M5263.4,2400c-4.8,0-7.4-1.3-7.5-1.8v0c0.1-0.5,2.7-1.8,7.5-1.8c4.8,0,7.3,1.3,7.5,1.8C5270.7,2398.7,5268.2,2400,5263.4,2400z"></path>
                                          <path d="M5268.4,2410.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1c0-0.6-0.4-1-1-1H5268.4z"></path>
                                          <path d="M5272.7,2413.7h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2414.1,5273.3,2413.7,5272.7,2413.7z"></path>
                                          <path d="M5272.7,2417h-4.3c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4.3c0.6,0,1-0.4,1-1C5273.7,2417.5,5273.3,2417,5272.7,2417z"></path>
                                       </g>
                                       <g>
                                          <path d="M25.8,51.1c0.7,0.7,1.6,1,2.4,1c0.9,0,1.8-0.3,2.4-1c1.3-1.4,1.3-3.5,0-4.9l-4.6-4.6h47.9l-4.6,4.6     c-1.3,1.4-1.3,3.5,0,4.9c1.4,1.4,3.5,1.4,4.9,0l10.5-10.5c1.4-1.4,1.4-3.5,0-4.9L74.2,25.2c-0.7-0.7-1.6-1-2.4-1     c-0.9,0-1.8,0.3-2.4,1c-1.4,1.4-1.3,3.5,0,4.9l4.6,4.6l-47.9,0l4.6-4.6c1.3-1.4,1.3-3.5,0-4.9c-1.4-1.4-3.5-1.4-4.9,0L15.3,35.7     c-1.4,1.4-1.4,3.5,0,4.9L25.8,51.1z"></path>
                                          <path d="M96,68.9h-8.6V56.6c0-0.8-0.7-1.5-1.5-1.5h-4c-0.8,0-1.5,0.7-1.5,1.5v12.3H70.4v-7.4c0-0.8-0.7-1.5-1.5-1.5h-4     c-0.8,0-1.5,0.7-1.5,1.5v7.4H53.5v-7.4c0-0.8-0.7-1.5-1.5-1.5h-4c-0.8,0-1.5,0.7-1.5,1.5v7.4H36.5v-7.4c0-0.8-0.7-1.5-1.5-1.5h-4     c-0.8,0-1.5,0.7-1.5,1.5v7.4H19.5V56.6c0-0.8-0.7-1.5-1.5-1.5h-4c-0.8,0-1.5,0.7-1.5,1.5v12.3H4c-0.8,0-1.5,0.7-1.5,1.5v4     c0,0.8,0.7,1.5,1.5,1.5H96c0.8,0,1.5-0.7,1.5-1.5v-4C97.5,69.6,96.8,68.9,96,68.9z"></path>
                                       </g>
                                    </g>
                                 </switch>
                              </svg>
                           </i>
                        </div>
                        <h3>Scalable product</h3>
                        <p>The TikTok clone can be readily modified in the future as per changing user preferences, owing to the highly scalable nature.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px'  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                                 <title>language, translate, translation, translator</title>
                                 <g>
                                    <path d="M55.63,22.25H39.44V11.06a6.818,6.818,0,0,0-6.81-6.81H8.37a6.818,6.818,0,0,0-6.81,6.81V26.23a6.733,6.733,0,0,0,1.15,3.78c2.4,4.62,13.47,11.45,13.59,11.52a1.559,1.559,0,0,0,.78.22,1.492,1.492,0,0,0,1.39-2.06,41.784,41.784,0,0,1-1.86-6.66h7.95v11.2a6.809,6.809,0,0,0,6.81,6.8H47.39a41.784,41.784,0,0,1-1.86,6.66,1.492,1.492,0,0,0,1.39,2.06,1.559,1.559,0,0,0,.78-.22c.12-.07,11.19-6.9,13.59-11.52a6.733,6.733,0,0,0,1.15-3.78V29.06A6.818,6.818,0,0,0,55.63,22.25ZM20.71,26.01a11.979,11.979,0,0,1-6.35,1.6h-.03a1.5,1.5,0,1,1,0-3h.03a9.83,9.83,0,0,0,3.87-.7,13.448,13.448,0,0,1-2.34-4.01,1.5,1.5,0,1,1,2.8-1.08,9.948,9.948,0,0,0,2.02,3.35,9.887,9.887,0,0,0,2.2-5.21H14.33a1.5,1.5,0,1,1,0-3h4.71V11.33a1.5,1.5,0,1,1,3,0v2.63h4.7a1.5,1.5,0,1,1,0,3h-.81a13.063,13.063,0,0,1-2.77,6.96,8.585,8.585,0,0,0,3.08.68,6.589,6.589,0,0,0-1.48,2.86A10.61,10.61,0,0,1,20.71,26.01ZM59.44,44.23a3.649,3.649,0,0,1-.68,2.16,1.428,1.428,0,0,0-.15.27c-1.18,2.24-5.61,5.65-8.97,7.99.67-2.58,1.01-4.75,1.03-4.89a1.493,1.493,0,0,0-.34-1.2,1.538,1.538,0,0,0-1.14-.53H31.37a3.807,3.807,0,0,1-3.81-3.8V29.06a3.815,3.815,0,0,1,3.81-3.81H55.63a3.815,3.815,0,0,1,3.81,3.81ZM48.92,38.639l0-.005-3.989-9.971-.008-.016a1.353,1.353,0,0,0-.084-.16c-.019-.034-.034-.07-.055-.1a1.413,1.413,0,0,0-.1-.121,1.216,1.216,0,0,0-.086-.1,1.318,1.318,0,0,0-.107-.088,1.337,1.337,0,0,0-.118-.1c-.034-.023-.073-.04-.109-.06a1.46,1.46,0,0,0-.152-.079l-.017-.009c-.037-.015-.074-.021-.111-.032a1.354,1.354,0,0,0-.154-.044,1.458,1.458,0,0,0-.148-.018,1.376,1.376,0,0,0-.143-.011c-.052,0-.1.007-.155.012s-.091.007-.136.016a1.435,1.435,0,0,0-.162.046c-.035.012-.071.017-.106.031l-.017.009a1.359,1.359,0,0,0-.151.079c-.037.02-.076.037-.11.06s-.076.061-.114.092-.077.058-.111.092-.054.066-.082.1a1.369,1.369,0,0,0-.1.126c-.021.031-.036.067-.055.1a1.665,1.665,0,0,0-.085.162l-.008.016-3.988,9.971,0,.005-2.01,5.024a1.5,1.5,0,1,0,2.785,1.114l1.623-4.057h5.969l1.623,4.057a1.5,1.5,0,1,0,2.785-1.114Zm-7.168-.919,1.785-4.461,1.784,4.461Z"></path>
                                 </g>
                              </svg>
                           </i>
                        </div>
                        <h3>Multilingual, Multi-currency</h3>
                        <p>The solutions we built extend multilingual support, allowing users to view the app content in their preferred languages. Also, they can pay for the services through various currencies. Both these features enable business owners to level their app on a global scale.</p>
                     </div>
                  </div>
                  <!--  -->
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" x="0px" y="0px">
                                 <title>LEGAL_1</title>
                                 <g data-name="Layer 8">
                                    <path d="M43,38a1,1,0,0,1-1,1H29a1,1,0,0,1,0-2H42A1,1,0,0,1,43,38Zm-1,7H29a1,1,0,0,0,0,2H42a1,1,0,0,0,0-2Zm0-16H29a1,1,0,0,0,0,2H42a1,1,0,0,0,0-2ZM59,16V45a1,1,0,0,1-1,1H49v7a9,9,0,0,1-9,9H14a9,9,0,0,1-9-9,1,1,0,0,1,1-1h7V11a1,1,0,0,1,1-1h9V3a1,1,0,0,1,1-1H45a1,1,0,0,1,.67.26h0l0,0,13,13h0a1,1,0,0,1,.19.29l0,.09a1,1,0,0,1,.05.26S59,16,59,16ZM46,15h9.59L46,5.41ZM34.36,60a9,9,0,0,1-3.31-6h-24A7,7,0,0,0,14,60ZM47,25H35a1,1,0,0,1-1-1V12H15V52H32a1,1,0,0,1,1,1,7,7,0,0,0,14,0Zm-1.41-2L36,13.41V23ZM57,44V17H45a1,1,0,0,1-1-1V4H25v6H35a1,1,0,0,1,.67.26h0l0,0,13,13h0a1,1,0,0,1,.19.29l0,.09a1,1,0,0,1,.05.26s0,0,0,.06V44ZM24.29,27.29,21,30.59l-1.29-1.29a1,1,0,0,0-1.41,1.41l2,2a1,1,0,0,0,1.41,0l4-4a1,1,0,0,0-1.41-1.41Zm0,8L21,38.59l-1.29-1.29a1,1,0,0,0-1.41,1.41l2,2a1,1,0,0,0,1.41,0l4-4a1,1,0,0,0-1.41-1.41Zm0,8L21,46.59l-1.29-1.29a1,1,0,0,0-1.41,1.41l2,2a1,1,0,0,0,1.41,0l4-4a1,1,0,0,0-1.41-1.41ZM40,57.5A4.5,4.5,0,0,1,35.5,53a.5.5,0,0,0-1,0A5.51,5.51,0,0,0,40,58.5a.5.5,0,0,0,0-1Z"></path>
                                 </g>
                              </svg>
                           </i>
                        </div>
                        <h3>Regulatory compliance</h3>
                        <p>We ensure that our TikTok clone apps comply with all regulatory requirements issued by the concerned authorities in your industry.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 64 64" x="0px" y="0px">
                                 <title>team_14</title>
                                 <path d="M22.66,14.4A9.34,9.34,0,1,1,32,23.74,9.34,9.34,0,0,1,22.66,14.4Zm30,9.46a7.91,7.91,0,1,0-7.9-7.9A7.91,7.91,0,0,0,52.69,23.86ZM11.31,8.05A7.91,7.91,0,1,0,19.21,16,7.91,7.91,0,0,0,11.31,8.05ZM52.69,27.66a13.68,13.68,0,0,0-6.32,1.41,15.71,15.71,0,0,1,1.8,2.58c3.31,6,1.84,14.92.06,21.3H60.64s5.79-13.73,2.17-20.23C59.89,27.46,53.42,27.66,52.69,27.66Zm-41.38,0c-.73,0-7.2-.2-10.12,5.06C-2.43,39.22,3.36,53,3.36,53H15.77c-1.78-6.38-3.25-15.34.06-21.3a15.71,15.71,0,0,1,1.8-2.58A13.68,13.68,0,0,0,11.31,27.66ZM32,27.26c-.91,0-9-.25-12.67,6.34-3.58,6.43.2,18.53,1.93,23.31a3.09,3.09,0,0,0,2.9,2H39.84a3.09,3.09,0,0,0,2.9-2C44.47,52.13,48.25,40,44.67,33.6,41,27,32.91,27.26,32,27.26Z"></path>
                              </svg>
                           </i>
                        </div>
                        <h3>Seasoned team</h3>
                        <p>Our app development team comprises experts from various domains, including business analysts, skilled designers, proficient developers, and qualified testers.</p>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-6">
                     <div class="single-features">
                        <div class="icon">
                           <i>
                              <svg height='300px' width='300px' xmlns:x="http://ns.adobe.com/Extensibility/1.0/" xmlns:i="http://ns.adobe.com/AdobeIllustrator/10.0/" xmlns:graph="http://ns.adobe.com/Graphs/1.0/" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">
                                 <switch>
                                    <foreignObject requiredExtensions="http://ns.adobe.com/AdobeIllustrator/10.0/" x="0" y="0" width="1" height="1"></foreignObject>
                                    <g i:extraneous="self">
                                       <g>
                                          <path d="M55.1,84.7c-1.8-0.2-4.6-0.9-7.7-1.7c-1.8,1.1-4.3,1.5-7.3,0.5c-0.7-0.2-1.5-0.5-2.3-0.9c-1.9-1-4.1-2.4-6.5-3.9     c-0.2-0.1-0.3-0.2-0.5-0.3c-0.6-0.4-0.9-1.1-0.6-1.8c0.3-0.8,1.3-1.1,2-0.6c0.2,0.1,0.4,0.2,0.6,0.4c3.1,2,6,3.9,8.1,4.6     c3.9,1.2,6.1-0.4,6.8-2.4c0.6-1.6,0.3-3.6-1.6-4.4c-2.6-1.2-5.5-2.9-8.2-4.6c-3.2-2-6.5-3.9-9-4.9c-2.8-1.1-9.3-2-15.8-1.3     c-3.2,0.3-6.8,0.9-9,1.2c-1.1,0.2-1.9,1.2-1.7,2.3l2.2,12.7C4.9,80.5,5.9,81.2,7,81l3.6-0.6c0.9-0.1,1.7,0.1,2.3,0.8     c3.9,4,8.4,7.6,11.5,9.5c6.9,4.1,25.1,3,30.5,2.4C60.3,92.4,60.4,85.4,55.1,84.7z"></path>
                                          <path d="M95.3,20.6c-0.2-1.1-1.2-1.8-2.3-1.6l-3.6,0.6c-0.9,0.1-1.7-0.1-2.3-0.8c-3.9-4-8.4-7.6-11.5-9.5     c-6.9-4.1-25.1-3-30.5-2.4c-5.4,0.6-5.4,7.6-0.2,8.3c1.8,0.2,4.6,0.9,7.7,1.7c1.8-1.1,4.3-1.5,7.3-0.5c0.7,0.2,1.5,0.5,2.3,0.9     c1.9,1,4.1,2.4,6.5,3.9c0.2,0.1,0.3,0.2,0.5,0.3c0.6,0.4,0.9,1.1,0.6,1.8c-0.3,0.8-1.3,1.1-2,0.6c-0.2-0.1-0.4-0.2-0.6-0.4     c-3.1-2-6-3.9-8.1-4.6c-3.9-1.2-6.1,0.4-6.8,2.4c-0.6,1.6-0.3,3.6,1.6,4.4c2.6,1.2,5.5,2.9,8.2,4.6c3.2,2,6.5,3.9,9,4.9     c2.8,1.1,9.3,2,15.8,1.3c3.2-0.3,6.8-0.9,9-1.2c1.1-0.2,1.9-1.2,1.7-2.3L95.3,20.6z"></path>
                                          <path d="M50,32.7c-9.5,0-17.3,7.8-17.3,17.3c0,9.5,7.8,17.3,17.3,17.3c9.5,0,17.3-7.8,17.3-17.3C67.3,40.5,59.5,32.7,50,32.7z      M51.5,58.6v2.2c0,0.2-0.2,0.4-0.4,0.4h-2.1c-0.2,0-0.4-0.2-0.4-0.4v-2.1c-2.2-0.1-3.9-1.8-4.1-4c0-0.2,0.2-0.4,0.4-0.4H47     c0.2,0,0.4,0.1,0.4,0.3c0.1,0.7,0.7,1.2,1.4,1.2h1.9c1.1,0,2.1-0.8,2.2-1.9c0.1-1.3-0.9-2.3-2.1-2.3h-1.4c-2.6,0-5-1.9-5.3-4.6     c-0.3-2.8,1.7-5.2,4.4-5.5v-2.1c0-0.2,0.2-0.4,0.4-0.4h2.1c0.2,0,0.4,0.2,0.4,0.4v2.1c2.2,0.1,3.9,1.8,4.1,4     c0,0.2-0.2,0.4-0.4,0.4H53c-0.2,0-0.4-0.1-0.4-0.3c-0.1-0.7-0.7-1.2-1.4-1.2h-1.9c-1.1,0-2.1,0.8-2.2,1.9     c-0.1,1.3,0.9,2.3,2.1,2.3h1.6c3,0,5.3,2.6,5,5.6C55.6,56.4,53.7,58.2,51.5,58.6z"></path>
                                       </g>
                                    </g>
                                 </switch>
                              </svg>
                           </i>
                        </div>
                        <h3>Cost-effective, Time-efficient</h3>
                        <p>Our TikTok clone solutions are reasonably priced, compared to apps built from the ground up. Also, business owners can customize and launch these apps in a short period.</p>
                     </div>
                  </div>
               </div>
            </div>
            
         </section>
         <!--  -->
         <!-- FAQ Section -->
         <div id="faq" class="section bg-light">
            <div class="container">
               <div class="section-heading center">
                  <h2 class="heading text-indigo">
                     Frequently Asked
                  </h2>
               </div>
               <!-- .section-heading -->
               <div class="row">
                  <div class="col-md-12" data-vertical_center="true">
                     <!-- Accordion -->
                     <div class="ui-accordion-panel">
                        <!-- Accordion 1  -->
                        <div class="ui-card shadow-sm ui-accordion">
                           <h6 class="toggle" data-toggle="accordion-one">1. Will you offer assistance if my app gets rejected during app submission?</h6>
                           <div class="body in" data-accord="accordion-one">
                              <p>Yes. We do provide app rejection support as part of our app development process. If your app gets rejected for any reason, our expert team will immediately look into the issue, fix it, and resubmit the app on the app platforms. We will extend our complete assistance during the entire process until your app is up and running.</p>
                           </div>
                        </div>
                        <!-- Accordion 2  -->
                        <div class="ui-card shadow-sm ui-accordion">
                           <h6 class="toggle" data-toggle="accordion-two">2. What owes to the popularity of social video-sharing apps like TikTok?</h6>
                           <div class="body" data-accord="accordion-two">
                              <p>App solutions like TikTok enable users to flaunt their skills and entertain their followers. Also, some users register with the app to view the interesting video content shared by other users. Both these segments of the audience find these apps fascinating as it helps them in pursuing their passion, along with entertaining them.</p>
                           </div>
                        </div>
                        <!-- Accordion 3  -->
                        <div class="ui-card shadow-sm ui-accordion">
                           <h6 class="toggle" data-toggle="accordion-three">3. Can I modify the app features in the future?</h6>
                           <div class="body" data-accord="accordion-three">
                              <p>Yes. Our TikTok clone is a scalable solution that enables you to make any future enhancements with ease. For this, you can choose our app development package that includes the 100% customizable source codes.</p>
                           </div>
                        </div>
                        <!-- Accordion 4  -->
                        <div class="ui-card shadow-sm ui-accordion">
                           <h6 class="toggle" data-toggle="accordion-four">4. What are clone applications?</h6>
                           <div class="body" data-accord="accordion-four">
                              <p>Clone apps are ready-made solutions integrated with all necessary and advanced features needed for their seamless functionality. These apps can be used without any hassle, similar to apps built from scratch.
                                 The major benefit of clone apps is that they save time and money, allowing you to launch your app in the quickest time possible at reasonable prices. Businesses can opt for these solutions for the immediate launch of their services in their niche.
                              </p>
                           </div>
                        </div>
                        <!-- Accordion 5  -->
                        <div class="ui-card shadow-sm ui-accordion">
                           <h6 class="toggle" data-toggle="accordion-five">5. How can I reach out to your business?</h6>
                           <div class="body" data-accord="accordion-five">
                              <p>
                                You can connect with our support team via email - <a href="mailto:info@tiktokcloneapp.com"> info@tiktokcloneapp.com </a>. They will take you through the entire app development process, resolve your queries, and help you start right away.  
                              </p>
                           </div>
                        </div>
                     </div>
                     <!-- Accordion -->
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!--  Testimonial Section -->
         <div id="testimonials" class="section">
            <div class="container">
               <!-- Card Heading -->
               <div class="section-heading center">
                  <h2 class="heading text-indigo">
                     What Do Our Clients Say?
                  </h2>
               </div>
               <!-- .section-heading -->
               <!-- Slider  -->
               <div class="ui-testimonials slider owl-carousel owl-theme">
                  <!-- Testimonials Item 1 -->
                  <div class="item">
                     <div class="star_sec"></div>
                     <div class="ui-card shadow-md">
                        <p>“Tiktokcloneapp.com went above and beyond our expectations. They developed a remarkable video-sharing app like TikTok for our business. Throughout the entire process, the project manager helped us resolve our doubts and updated us on each stage of the project. Their responsiveness and exceptional service are their best qualities.”</p>
                     </div>
                     <!-- User -->
                     <div class="user">
                        <div class="star_sec"> <img src="assets/img/5-star.png" width="220"> </div>
                     </div>
                  </div>
                  <!-- Testimonials Item 2 -->
                  <div class="item">
                     <!-- Card -->
                     <div class="ui-card shadow-md">
                        <p> “After spending a lot of time surfing online, I chose Tiktokappclone.com for my app development project. Their innovative approach mitigated the communication barriers, right from the project start. By opting for their TikTok clone solution, my business saved a lot of money and effort, which I used for its promotion. Thanks to the entire team for their support!”</p>
                     </div>
                     <!-- User -->
                     <div class="user">
                        <div class="star_sec"> <img src="assets/img/4-star.png" width="220"> </div>
                     </div>
                  </div>
                  <!-- Testimonials Item 3 -->
                  <div class="item">
                     <!-- Card -->
                     <div class="ui-card shadow-md">
                        <p>“We wanted to launch a video-sharing platform in the shortest time possible. We spoke with several app developers, but they were not able to meet our expectations. This was when we got on board with TikTokcloneapp.com. They understood our requirements and strived to launch a fully functional app quickly at reasonable costs. We are looking forward to collaborating with them for our future projects as well.”</p>
                     </div>
                     <!-- User -->
                     <div class="user">
                        <div class="star_sec"> <img src="assets/img/5-star.png" width="220"> </div>
                     </div>
                  </div>
               </div>
               <!-- .ui-testimonials  -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
        <div class="section bg-indigo" id="contact">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <div class="section-heading ">
                        <h2 class="heading">About Us</h2>
                        <p class="paragraph">
                           TikTok Clone is a social video-sharing platform that enables its users to flaunt their skills and entertain their followers across the globe. At Tiktokcloneapp.com, we offer our ready-made solutions at reasonable prices and assist you to launch in your industry in the shortest turnaround time. Connect with us to become an instant hit in your niche!
                        </p>
                        <p class="mail_id"> <a href="mailto:info@tiktokcloneapp.com"> <i class="fa fa-phone-square"></i> info@tiktokcloneapp.com</a></p>
                        <p class="mail_id"> <a href="tel:+916369250989"> <i class="fa fa-phone-square"></i> +91 6369 250 989</a></p>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <form id="contact_form" method="post" class="form_field">
                     <div class="row">
                        <div class="col-md-12">
                            <div class="section-heading cont">
                                <h2 class="heading">Contact Us</h2>
                            </div>
                        </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input type="hidden" name="form-title" value="Contact Us">
                                  <input class="form-control" id="name" type="text" name="Name" placeholder="Name" required="">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" id="email" type="email" name="Email" placeholder="Email" required="">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                <input class="form-control" id="phone" type="tel" name="phone_dummy" placeholder="Phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                              <input type="hidden" class="country_code1">
                              <input type="hidden" name="Phone" class="phone_val1">
                              <input type="hidden" name="form-title" value="Contact Form">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" type="text" required="required" placeholder="Subject" id="subject" class="form-control" name="Subject">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <textarea required="required" placeholder="Message *" id="description" class="form-control" name="Message" rows="4"></textarea>
                              </div>
                          </div>
                          <div class="col-md-12 text-center">
                               <input type='hidden' value='<?php echo $_SERVER["REMOTE_ADDR"]; ?>' name='IP'>
                               <input type="hidden" name="Page_URL" id="url" value="<?php echo $_SERVER['REQUEST_URI']?>">
                          <!-- <button type="button" value="Send Message" class="btn-send submit_send submit-botton">Submit</button> -->
                             <button type="submit" class="submit_send btn btn-sm ui-gradient-green" data-form="contact_form"><span>Send Message</span></button>
                          </div>
                     
                     </div>
                    </form>
                  </div>
               </div>
               <!-- .section-heading -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!-- Basic Footer -->
         <footer class="ui-footer">
            <!-- Footer Copyright -->
            <div class="footer-copyright bg-dark-gray">
               <div class="container">
                  <div class="row">
                     <!-- Copyright -->
                     <div class="col-12 text-center">
                        <p>
                           &copy; 2020 <a href="#" target="_blank" title="Codeytech">TikTok Clone App</a> All Rights Reserved
                        </p>
                     </div>
                  </div>
               </div>
               <!-- .container -->
            </div>
            <!-- .footer-copyright -->
         </footer>
        <div class="sticky_icons">
          <ul>
             <li><a href="https://api.whatsapp.com/send?l=en&amp;text=Hi!%20I%27m%20interested%20in%20tiktok%20&amp;phone=916369250989" target="_blank"><i class="fa shake fa-whatsapp"></i></a></li>
              <li><a href="tel:+916369250989" target="_blank"><i class="fa fa-phone"></i></a></li>
          </ul>
        </div>
         <!-- .ui-footer -->
      </div>
      <!-- .main -->
      <!-- Scripts -->
      <script type="text/javascript" src="assets/js/libs/jquery/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/slider-pro/jquery.sliderPro.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/owl.carousel/owl.carousel.min.js"></script>    
      <script type="text/javascript" src="assets/js/libs/form-validator/form-validator.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/bootstrap.js"></script>
      <script type="text/javascript" src="assets/js/applify/build/applify.js"></script>
      <script src="build/js/intlTelInput.js" type="text/javascript"></script>
    <script src="build/js/utils.js" type="text/javascript"></script>
    <script>
        $("#phone").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone1").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone2").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone3").intlTelInput({
            utilsScript: "build/js/utils.js"
        });

    </script>
    <script language="javascript" type="text/javascript">
        var maxLength = 200;
        $('textarea').keyup(function() {
        
        var msg=$(this).val();
        var urltext=msg.indexOf('http')!== -1 || msg.indexOf('www.')!== -1 || msg.indexOf('www,')!== -1;
        if(urltext){
        alert('url not allowed');
        $('textarea').val('');
        }
        else{
        var textlen = maxLength - $(this).val().length;
        $('.rchars').text(textlen);
        }
        });
        $('textarea').focusout(function() {
        
        var msg=$(this).val();
        var urltext=msg.indexOf('http')!== -1 || msg.indexOf('www.')!== -1 || msg.indexOf('www,')!== -1;
        if(urltext){
        alert('url not allowed');
        $('textarea').val('');
        }
        
        });
    </script>
    <script>
        $(".submit_send").click(function(){

            function loader(){ 
                $( '<div id="mloader"></div>' ).insertAfter( ".submit_send" );
                $(".submit_send").next().addClass("loading");
                setTimeout(function() {
                $(".submit_send").next().remove();
                }, 7000);
                
            }

            var formname = $(this).attr('data-form');
            

          $(".ajax-loader").hide();

          var name = $(this).parent().parent().parent().find('input[name="Name"]').val();
          var email = $(this).parent().parent().parent().find('input[name="Email"]').val();
          var phone = $(this).parent().parent().parent().find('input[name="phone_dummy"]').val();

          
        // console.log(name, email, phone);

          if(name!=''){

            if( (email!='') && (validateEmail(email)) ){
            if((phone!='') && (validatePhone(phone)) ){  

                $(this).attr("disabled", true);
                $(this).addClass('disabled');
                loader();    
                $(".ajax-loader").hide();
                
                
                var phone_title=$("#"+formname+" "+".selected-flag").attr("title").replace(/ *\([^)]*\) */g, "");
                $(this).parent().parent().parent().find('input[name="Phone"]').val(phone_title+" "+phone);

                var formdata=$("#"+formname).serialize()+'&_token='+$('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: "ajaxmail.php",
                    type: "POST",
                    data: formdata,
                    success: function(result){
                        console.log(result);
                        if(result == 1)
                        {
                            // alert("Mail Sent Successfully");
                            window.location.href="thank-you";
                            return false;
                        }
                        else
                        {
                            alert("Something went wrong, please try again later.");
                            return false;
                        }
                        $(".ajax-loader").hide();
                    }
                });
                } 
                else if(!validatePhone(phone)) {
                    alert("Please type correct mobile number format");
                }
                else {
                    alert("please type your phone number");
                }
            }
            else if(!validateEmail(email)) {
                alert("Please type correct email format");
            }
            else {
              alert("please type your email");
            }
          }
          else{
              alert("please fill all fields");
          } 
        }); 

        function validateEmail($email) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test($email);
        }
        function validatePhone($phone) {
            var pattern1 = new RegExp("[0-9]+");
            return pattern1.test($phone);
        }
        var url = window.location.href;
        $('#url').val(url);
    </script>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 10340807;
(function() {
var lc = document.createElement('script');
lc.type = 'text/javascript';
lc.async = true;
lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/10340807/">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
</noscript>
<!-- Start of LiveChat End -->
   </body>
</html>


