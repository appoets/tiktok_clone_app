<?php include("csrfhandler.lib.php"); ?>
<!doctype html>
<html lang="zxx">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development</title>
    <meta name="description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! "/>
    <meta name="keywords" content="Tiktok clone, Tiktok clone app, Tiktok clone script, social video dubbing app development, video sharing app development, Tiktok like app development, app like Tiktok, Tiktok app clone, Tiktok app clone development, social video sharing app development, white label Tiktok clone, white-label Tiktok app clone, Tiktok alternative app development" />

    <link rel="canonical" href="https://www.tiktokcloneapp.com/" />

    <meta property="og:title" content="Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.tiktokcloneapp.com/" />
                 
    <meta property="og:image:width" content="600" />
    <meta property="og:image:height" content="400" />
    <meta property="og:site_name" content="Cryptocurrency Exchange Script" />
    <meta property="og:description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! " />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Cryptocurrency Exchange Script"/>
    <meta name="twitter:title" content="Tiktok Clone, Tiktok Clone App, Social Video Sharing App Development" />
    <meta name="twitter:description" content="TikTok clone is a ready-made video-sharing application that can be quickly customized and launched in the market in a short period. These solutions are offered at budget-friendly prices. Contact us to get started! " />
    <meta name="twitter:image" content="" />
    <meta itemprop="image" content="" />
    <meta property="og:image" content="" /> 
    
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:600|Source+Sans+Pro:600,700" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/style.min.css"/>
      <link rel="stylesheet" href="build/css/intlTelInput.css">
      <link rel="shortcut icon" href="assets/img/favicon.png" sizes="32x32" />
        <link rel="icon" href="assets/img/favicon.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed" href="assets/img/favicon.png" />
        <meta name="csrf-token" content="<?php echo csrf::setToken();?>">
      <style>
         svg:not(:root).svg-inline--fa{overflow:visible;}
         .svg-inline--fa{display:inline-block;font-size:inherit;height:1em;overflow:visible;vertical-align:-.125em;}
         .svg-inline--fa.fa-w-12{width:.75em;}
         .svg-inline--fa.fa-w-14{width:.875em;}
         .svg-inline--fa.fa-w-18{width:1.125em;}
         .order-1{-ms-flex-order:1;order:1;}
         .order-2{-ms-flex-order:2;order:2;}
         @media (min-width:992px){
         .order-lg-1{-ms-flex-order:1;order:1;}
         .order-lg-2{-ms-flex-order:2;order:2;}
         }
         .media{display:-ms-flexbox;display:flex;-ms-flex-align:start;align-items:flex-start;}
         .media-body{-ms-flex:1;flex:1;}
         .d-none{display:none!important;}
         @media (min-width:992px){
         .d-lg-block{display:block!important;}
         }
         .cta h3 {
    color: #fff;
    font-size: 40px;
    line-height: 1.3;
}
         .justify-content-between{-ms-flex-pack:justify!important;justify-content:space-between!important;}
         .overflow-hidden{overflow:hidden!important;}
         .mt-4{margin-top:1.5rem!important;}
         .mb-4{margin-bottom:1.5rem!important;}
         .py-2{padding-top:.5rem!important;}
         .py-2{padding-bottom:.5rem!important;}
         .pt-4{padding-top:1.5rem!important;}
         .pr-4{padding-right:1.5rem!important;}
         .mx-auto{margin-right:auto!important;}
         .mx-auto{margin-left:auto!important;}
         @media (min-width:992px){
         .pt-lg-0{padding-top:0!important;}
         }
         .text-capitalize{text-transform:capitalize!important;}
         .btn{font-size:15px;font-weight:500;color:#fff;line-height:1;text-align:center;padding:18px 30px;border:0 none;border-radius:6px;outline:0 none;position:relative;z-index:1;}
         .btn:hover,.btn:focus,.btn:active{color:#fff;-webkit-box-shadow:0 10px 25px rgba(0, 0, 0, 0.1);box-shadow:0 10px 25px rgba(0, 0, 0, 0.1);}
         .btn,.btn:active{background:linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);}
         .service-icon span{height:54px;width:54px;display:inline-block;text-align:center;line-height:54px;font-size:24px;color:#7c4fe0;border-radius:4px;background-color:#fff;border:1px solid rgba(0, 0, 0, 0.1);-webkit-box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);box-shadow:0 1px 3px rgba(0, 0, 0, 0.1);-webkit-transition:all 0.3s ease 0s;transition:all 0.3s ease 0s;}
         @media (max-width: 991px){
         h2{font-size:36px;}
         }
         @media (max-width: 767px){
         h2{font-size:30px;line-height:1.4;}
         }
         @media (max-width: 575px){
         h2{font-size:24px;line-height:1.4;}
         p{line-height:26px;}
         }
         /*  */
         .rounded-pill{border-radius:50rem!important;}
         .justify-content-center{-ms-flex-pack:center!important;justify-content:center!important;}
         .overflow-hidden{overflow:hidden!important;}
         .shadow-sm{box-shadow:0 .125rem .25rem rgba(0,0,0,.075)!important;}
         .mr-1{margin-right:.25rem!important;}
         .mb-2{margin-bottom:.5rem!important;}
         .mb-3{margin-bottom:1rem!important;}
         .mt-4{margin-top:1.5rem!important;}
         .py-2{padding-top:.5rem!important;}
         .py-2{padding-bottom:.5rem!important;}
         .px-4{padding-right:1.5rem!important;}
         .px-4{padding-left:1.5rem!important;}
         .p-5{padding:3rem!important;}
         .text-center{text-align:center!important;}
         .text-primary{color:#007bff!important;}
         .fw-5{font-weight:500;}
         .avatar-sm{height:3rem;width:3rem;}
         .text-primary{color:#7c4fe0!important;}
         .section-heading{margin-bottom:80px;}
         .section-heading > span{color:#222;}
         .section-heading > h2{position:relative;}
         .section-heading > h2::after{position:absolute;content:'';height:2px;width:70px;background-color:#7c4fe0;bottom:-15px;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%);}
         .ptb_100{padding:100px 0;}
        .features-area.style-two .image-box {
    min-height: 290px;
    position: relative;
    -webkit-box-shadow: 0 3px 20px 0px rgba(0, 0, 0, 0.12);
    box-shadow: 0 3px 20px 0px rgba(0, 0, 0, 0.12);
    border-radius: 1.5rem;
    -webkit-transition: -webkit-transform 0.3s ease 0s;
    transition: -webkit-transform 0.3s ease 0s;
    transition: transform 0.3s ease 0s;
    transition: transform 0.3s ease 0s, -webkit-transform 0.3s ease 0s;
    padding: 3rem 15px 0 !important;
    background: #fff;
}
             .features-area.style-two .image-box p {
    font-weight: normal;
    margin-bottom: 0;
    font-size: 15px;
    line-height: 1.6;
    margin-top: 15px;
}
         .features-area.style-two .image-box:hover{-webkit-transform:translateY(-10px);transform:translateY(-10px);-webkit-box-shadow:0 1rem 3rem rgba(31, 45, 61, .125)!important;box-shadow:0 1rem 3rem rgba(31, 45, 61, .125)!important;}
         @media only screen and (min-width: 768px) and (max-width: 991px){
         .ptb_100{padding:70px 0;}
         }
         @media (max-width: 991px){
         .res-margin{margin-bottom:45px;}
         }
         @media (max-width: 767px){
         .ptb_100{padding:50px 0;}
         .res-margin{margin-bottom:30px;}
         }
         @media (max-width: 991px){
         h2{font-size:36px;}
         .section-heading{margin-bottom:50px;}
         }
         @media (max-width: 767px){
         h2{font-size:30px;line-height:1.4;}
         }
         @media (max-width: 575px){
         h2{font-size:24px;line-height:1.4;}
         h3{font-size:18px;line-height:1.4;}
         p{line-height:26px;}
         .section-heading > h2{line-height:1.5;}
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes fadeInLeft{0%{-webkit-transform:translate3d(-100%,0,0);opacity:0;transform:translate3d(-100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInLeft{0%{-webkit-transform:translate3d(-100%,0,0);opacity:0;transform:translate3d(-100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @-webkit-keyframes fadeInRight{0%{-webkit-transform:translate3d(100%,0,0);opacity:0;transform:translate3d(100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInRight{0%{-webkit-transform:translate3d(100%,0,0);opacity:0;transform:translate3d(100%,0,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @-webkit-keyframes fadeInUp{0%{-webkit-transform:translate3d(0,100%,0);opacity:0;transform:translate3d(0,100%,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         @keyframes fadeInUp{0%{-webkit-transform:translate3d(0,100%,0);opacity:0;transform:translate3d(0,100%,0);}to{-webkit-transform:translateZ(0);opacity:1;transform:translateZ(0);}}
         .image-box {
         margin-top: 25px;
         }
         /*  */
         pb-70{padding-bottom:70px;}
         .section-title{text-align:center;margin-bottom:40px;}
         .section-title h2{font-size:50px;margin:0 0 0 0;position:relative;line-height:1;}
         .section-title p{max-width:750px;margin:auto;line-height:1.8;}
         .section-title .bar{height:4px;width:85px;background:#7C4CEE;margin:20px auto 20px;position:relative;border-radius:5px;overflow:hidden;}
         .section-title .bar::before{content:'';position:absolute;left:0;top:0;height:100%;width:5px;background:#ffffff;-webkit-animation-duration:2s;animation-duration:2s;-webkit-animation-timing-function:linear;animation-timing-function:linear;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-name:MOVE-BG;animation-name:MOVE-BG;}
         .default-shape .shape-1{position:absolute;right:2%;bottom:5%;z-index:-1;-webkit-animation:moveBounce 10s linear infinite;animation:moveBounce 10s linear infinite;}
         .default-shape .shape-2{position:absolute;left:10%;top:16%;z-index:-1;}
         .default-shape .shape-2 img{-webkit-animation-name:rotateMe;animation-name:rotateMe;-webkit-animation-duration:10s;animation-duration:10s;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-timing-function:linear;animation-timing-function:linear;}
         .default-shape .shape-3{position:absolute;left:35%;bottom:15%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .default-shape .shape-4{position:absolute;right:10%;top:10%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .default-shape .shape-5{position:absolute;left:50%;top:10%;z-index:-1;-webkit-animation:animationFramesOne 30s infinite linear;animation:animationFramesOne 30s infinite linear;}
         .features-area{position:relative;z-index:1;overflow:hidden;}
         .single-features {
    margin-bottom: 30px;
    position: relative;
    z-index: 1;
    text-align: center;
    padding: 30px 15px 0;
    background-color: #ffffff;
    -webkit-box-shadow: 0 0 1.25rem rgba(108, 118, 134, 0.1);
    box-shadow: 0 0 1.25rem rgba(108, 118, 134, 0.1);
    -webkit-transition: 0.5s;
    transition: 0.5s;
    border-radius: 5px;
    min-height: 335px;
}
         .single-features .icon i{display:inline-block;height:60px;width:60px;line-height:60px;background-color:#7C4CEE;color:#ffffff;font-size:25px;border-radius:50px;-webkit-transition:0.5s;transition:0.5s;}
         .single-features h3{font-size:20px;margin-top:25px;margin-bottom:12px;-webkit-transition:0.5s;transition:0.5s;}
        .single-features p {
    margin-bottom: 0;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    font-size: 14px;
    line-height: 1.6;
}
         .single-features::before{top:50%;left:50%;width:0%;z-index:-1;height:100%;content:"";position:absolute;-webkit-transition:all 0.3s ease-in-out;transition:all 0.3s ease-in-out;background-color:#7C4CEE;-webkit-transform:translate(-50%, -50%);transform:translate(-50%, -50%);border-radius:5px;}
         .single-features:hover{-webkit-transform:translateY(-10px);transform:translateY(-10px);}
         .single-features:hover::before{width:100%;}
         .single-features:hover .icon i{background-color:#ffffff;color:#7C4CEE;}
         .single-features:hover h3{color:#ffffff;}
         .single-features:hover p{color:#ffffff;}
         @media only screen and (max-width: 767px){
         .pb-70{padding-bottom:20px;}
         .section-title{margin-bottom:28px;}
         .section-title h2{font-size:30px;}
         .default-shape .shape-1{display:none;}
         .default-shape .shape-2{display:none;}
         .default-shape .shape-3{display:none;}
         .default-shape .shape-4{display:none;}
         .default-shape .shape-5{display:none;}
         .single-features h3{font-size:22px;}
         }
         @media only screen and (min-width: 768px) and (max-width: 991px){
         .pb-70{padding-bottom:50px;}
         .section-title{margin-bottom:28px;}
         .section-title h2{font-size:40px;}
         .single-features h3{font-size:25px;}
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes MOVE-BG{from{-webkit-transform:translateX(0);}to{-webkit-transform:translateX(85px);}}
         @keyframes MOVE-BG{from{-webkit-transform:translateX(0);transform:translateX(0);}to{-webkit-transform:translateX(85px);transform:translateX(85px);}}
         @-webkit-keyframes moveBounce{0%{-webkit-transform:translateY(0px);transform:translateY(0px);}50%{-webkit-transform:translateY(20px);transform:translateY(20px);}100%{-webkit-transform:translateY(0px);transform:translateY(0px);}}
         @keyframes moveBounce{0%{-webkit-transform:translateY(0px);transform:translateY(0px);}50%{-webkit-transform:translateY(20px);transform:translateY(20px);}100%{-webkit-transform:translateY(0px);transform:translateY(0px);}}
         @-webkit-keyframes rotateMe{from{-webkit-transform:rotate(0deg);transform:rotate(0deg);}to{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
         @keyframes rotateMe{from{-webkit-transform:rotate(0deg);transform:rotate(0deg);}to{-webkit-transform:rotate(360deg);transform:rotate(360deg);}}
         @-webkit-keyframes animationFramesOne{0%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}20%{-webkit-transform:translate(73px, -1px) rotate(36deg);transform:translate(73px, -1px) rotate(36deg);}40%{-webkit-transform:translate(141px, 72px) rotate(72deg);transform:translate(141px, 72px) rotate(72deg);}60%{-webkit-transform:translate(83px, 122px) rotate(108deg);transform:translate(83px, 122px) rotate(108deg);}80%{-webkit-transform:translate(-40px, 72px) rotate(144deg);transform:translate(-40px, 72px) rotate(144deg);}100%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}}
         @keyframes animationFramesOne{0%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}20%{-webkit-transform:translate(73px, -1px) rotate(36deg);transform:translate(73px, -1px) rotate(36deg);}40%{-webkit-transform:translate(141px, 72px) rotate(72deg);transform:translate(141px, 72px) rotate(72deg);}60%{-webkit-transform:translate(83px, 122px) rotate(108deg);transform:translate(83px, 122px) rotate(108deg);}80%{-webkit-transform:translate(-40px, 72px) rotate(144deg);transform:translate(-40px, 72px) rotate(144deg);}100%{-webkit-transform:translate(0px, 0px) rotate(0deg);transform:translate(0px, 0px) rotate(0deg);}}
         .service-text p {
         text-align: justify;
         }
         .service-thumb img {
         width: 100%;
         }
         div#features {
         padding-top: 100px;
         background: #f5f5f5;
         }
.ui-tabs .tab-pane .sub-heading {
    font-weight: 600;
    margin-bottom: 2rem;
    font-size: 17px;
    background-color: #7C4CEE;
    color: #fff;
    padding: 20px;
    text-align: center;
    line-height: 1.7;
    border-radius: 5px;
    max-width: 450px;
    margin: 0 auto;
}
         /*  */
         .banner-btn:hover {
         background: #fff!important;
         color: #782fe5!important;
         }
         .step-number.ui-gradient-green, .step-number.ui-gradient-green::before {
         background: -moz-linear-gradient(45deg, #19d9b4 0%, #92d275 100%);
         background: linear-gradient(45deg, #5129de 0%, #7e30e6 100%);
         }
         .banner-btn{padding:12px 13px!important;}
         .cta {
         background: linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);
         padding: 120px 0;
         margin-bottom: 50px;
         }
         .cta p{font-family:'Open Sans',sans-serif!important;font-size:22px!important;font-weight:700;letter-spacing:normal!important;text-align:justify;margin-bottom:42px;color:#fff;}
         .app_image{position:relative;}
         .app_image .shadow_bottom{content:'';position:absolute;width:80%;height:5px;border-radius:50%/5px;left:10%;bottom:0;-webkit-box-shadow:0 35px 33px 0 rgba(0,0,0,.8);box-shadow:0 35px 33px 0 rgba(0,0,0,.8);-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_two{position:absolute;right:0;top:-100px;width:55%;}
         .app_image .image_two img{-webkit-animation-name:jump;animation-name:jump;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-duration:3.8s;animation-duration:3.8s;-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_two .shadow_bottom{-webkit-animation:shadow 3.8s infinite linear;animation:shadow 3.8s infinite linear;bottom:10px;}
         .app_image .image_first{position:absolute;left:60px;top:-50px;width:45%;}
         .app_image .image_first img{-webkit-animation-name:jump;animation-name:jump;-webkit-animation-iteration-count:infinite;animation-iteration-count:infinite;-webkit-animation-duration:2.8s;animation-duration:2.8s;-webkit-animation-timing-function:cubic-bezier(.54,.085,.5,.92);animation-timing-function:cubic-bezier(.54,.085,.5,.92);}
         .app_image .image_first .shadow_bottom{-webkit-animation:shadow 2.8s infinite linear;animation:shadow 2.8s infinite linear;}
         @media only screen and (min-width:320px) and (max-width:767px){
         .app_image{display:none;}
         .banner-btn{width:100%;margin:10px 0!important;}
         .cta{padding-top:50px;margin-bottom:0;padding-bottom:20px;}
         .cta p{font-size:18px!important;}
         }
         @media only screen and (min-width:768px) and (max-width:899px){
         .cta{padding:50px;margin-bottom:0;}
         .cta .image_first,.cta .image_two{display:none;}
         }
         .container:after,.container:before,.row:after,.row:before{display:table;content:" ";}
         .container:after,.row:after{clear:both;}
         *:focus,*,*:active{outline:none!important;outline:0!important;}
         a,.btn{-webkit-transition:all .3s ease-out 0s;-moz-transition:all .3s ease-out 0s;-ms-transition:all .3s ease-out 0s;-o-transition:all .3s ease-out 0s;transition:all .3s ease-out 0s;}
         a:hover{text-decoration:none;}
         a:focus,a:hover{color:#101010;}
         a{outline:medium!important;color:#727272;}
         .uppercase{text-transform:uppercase;}
         .btn{padding:10px 36px;margin:10px;box-shadow:none;border-radius:0;}
         .btn{border-radius:0;box-shadow:none;display:inline-block;margin:5px;padding:15px 35px;}
         @media only screen and (max-width:991px){
         .container{width:95%;}
         }
         a:hover{text-decoration:none;}
         a:focus{text-decoration:none;}
         img{max-width:100%!important;}
         .banner-btn{margin-right:15px!important;}
         .banner-btn:last-child{margin-right:0!important;}
         .container:before,.container:after{content:none;}
         .btn{margin:0;}
         .btn:hover{opacity:.8;}
         @media screen and (max-width:767px){
         .banner-btn{margin-right:0!important;margin-bottom:15px;display:block;}
         .banner-btn:last-child{margin-bottom:0;}
         .features-area.style-two .image-box {
    min-height: auto; padding-bottom: 30px !important; }
         }
         .banner-btn{background:#fff;padding:12px 20px;border-radius:2px;display:-webkit-inline-box;}
         .banner-btn:focus{background:#fff;}
         .banner-btn:hover{background:#fff;z-index:100;color:#3685eb;}
         .anim_btn {
         background: #4500b7;
         background: linear-gradient(135deg,#a27fe6 0%,#662de2 50%,#5b2be0 51%,#4e2ade 71%,#562adf 100%);
         filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4500b7',endColorstr='#4500b7',GradientType=1 );
         background-size: 400% 400%;
         border: medium;
         color: #fff!important;
         font-weight: 700;
         padding: 9px 40px;
         display: inline-block;
         font-size: 17px;
         border-radius: 4px;
         margin-top: 20px;
         text-shadow: none;
         
         }
         /*! CSS Used keyframes */
         @-webkit-keyframes jump{0%{-webkit-transform:translateY(0);transform:translateY(0);}50%{-webkit-transform:translateY(-30px);transform:translateY(-30px);-webkit-transition:-webkit-transform 1.4s .6s linear;transition:-webkit-transform 1.4s .6s linear;-o-transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear,-webkit-transform 1.4s .6s linear;}100%{-webkit-transform:translateY(0);transform:translateY(0);}}
         @keyframes jump{0%{-webkit-transform:translateY(0);transform:translateY(0);}50%{-webkit-transform:translateY(-30px);transform:translateY(-30px);-webkit-transition:-webkit-transform 1.4s .6s linear;transition:-webkit-transform 1.4s .6s linear;-o-transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear;transition:transform 1.4s .6s linear,-webkit-transform 1.4s .6s linear;}100%{-webkit-transform:translateY(0);transform:translateY(0);}}
         @-webkit-keyframes shadow{0%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}50%{-webkit-box-shadow:0 35px 35px -2px #000;box-shadow:0 35px 35px -2px #000;width:60%;left:20%;}100%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}}
         @keyframes shadow{0%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}50%{-webkit-box-shadow:0 35px 35px -2px #000;box-shadow:0 35px 35px -2px #000;width:60%;left:20%;}100%{-webkit-box-shadow:0 35px 33px 0 #000;box-shadow:0 35px 33px 0 #000;}}
         .ui-gradient-blue {
         background: -moz-linear-gradient(45deg, #7C4CEE 0%, #54ceff 100%);
         background: linear-gradient(-47deg, #8731E8 0%, #4528DC 100%);
         }
         a.btn.ui-gradient-blue.shadow-xl {
         background: #ffffff;
         color: #6e2de3;
         }
         a.btn.ui-gradient-green.shadow-xl {
         background: #fff;
         color: #6e2ee4;
         }
         section.section.service-area.overflow-hidden.ptb_100 {
         background: #f5f5f5;
         }
         .ui-app-icon {
         display: inline-block;
         width: 100px;
         height: 100px;
         margin: 0 auto 2rem auto;
         border-radius: 1.25rem;
         overflow: hidden;
         }
         .ui-app-icon.shadow-lg {
         background: #f1f1f1;
         }
         .btn-download span:nth-child(1) {
         font-size: 70%;
         margin-left: 20px;
         }
         .btn-download span {
         display: block;
         margin-left: 30px;
         }
         .icons svg{
         width: 16px;
         height: 16px;
         fill:#59687c;
         position: relative;
         top: 2px;
         margin-right: 3px;
         }
         .ui-tabs .nav-tabs li a.active svg {
         width: 16px;
         height: 16px;
         fill: #fff;
         position: relative;
         top: 2px;
         margin-right: 3px;
         }
         .featured-img svg {
         width: 48px;
         height: 48px;
         fill: #6d2ee3;
         }
         .icon svg {
         width: 30px;
         height: 30px;
         fill: #fff;
         position: relative;
         top: 4px;
         }
         .single-features:hover .icon svg {
         width: 30px;
         height: 30px;
         fill: #7C4CEE;
         position: relative;
         top: 4px;
         }
         .ui-hero.ui-waves::after{ display: none; }
      </style>
   </head>
   <body class="ui-transparent-nav" data-fade_in="on-load">
      <!-- Navbar Fixed + Default -->
      <nav class="navbar navbar-fixed-top transparent navbar-light bg-white">
         <div class="container">
            <!-- Navbar Logo -->
            <a class="ui-variable-logo navbar-brand" href="./" title="TikTok Clone">
               <!-- Default Logo -->
               <img class="logo-default" src="assets/img/logo/logo.png" alt="TikTok Clone" data-uhd>
               <!-- Transparent Logo -->
               <img class="logo-transparent" src="assets/img/logo/logo-transparent.png" alt="TikTok Clone" data-uhd>
            </a>
            <!-- .navbar-brand -->
            <!-- Navbar Navigation -->
            <div class="ui-navigation navbar-right">
               <ul class="nav navbar-nav">
                  <!-- Nav Item -->
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#about">About</a>
                  </li>
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#feature">Features</a>
                  </li>
                  <!-- Nav Item -->
                  
                  <!-- Nav Item -->
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#screens">App Screens</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#whychoose">Why Choose</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#faq">FAQ</a>
                  </li>
                  <!-- Nav Item -->
                  <li>
                     <a href="https://www.tiktokcloneapp.com/#testimonials">Reviews</a>
                  </li>
               </ul>
               <!--.navbar-nav -->
            </div>
            <!--.ui-navigation -->
            <!-- Navbar Button -->
            <a href="#" data-scrollto="contact" class="btn btn-sm ui-gradient-green pull-right">Request Proposal</a>
            <!-- Navbar Toggle -->
            <a href="#" class="ui-mobile-nav-toggle pull-right"></a>
         </div>
         <!-- .container -->
      </nav>
      <!-- nav -->
      <!-- Main Wrapper -->
      <div class="main" role="main">
         <!-- Hero Waves Center -->
         <div class="ui-hero hero-lg hero-center ui-gradient-blue ui-waves hero-svg-layer-3">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <h1 class="heading">
                        Thanks For Contacting Us!
                     </h1>
                     <p class="paragraph">
                        We Will Get In Touch With You Shortly .
                     </p>
                     <div class="actions">
                        <a class="btn ui-gradient-blue shadow-xl" href="./">Home</a>
                     </div>
                  </div>
               </div>
               <!-- .row -->
            </div>
            <!-- .container -->
         </div>
         <!-- .ui-hero -->
         <!--  -->
 
         <!--  -->
          
        <div class="section bg-indigo" id="contact">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-6">
                     <div class="section-heading ">
                        <h2 class="heading">About Us</h2>
                        <p class="paragraph">
                           TikTok Clone is a social video-sharing platform that enables its users to flaunt their skills and entertain their followers across the globe. At Tiktokcloneapp.com, we offer our ready-made solutions at reasonable prices and assist you to launch in your industry in the shortest turnaround time. Connect with us to become an instant hit in your niche!
                        </p>
                        <p class="mail_id"> <a href="mailto:info@tiktokcloneapp.com"> <i class="fa fa-phone-square"></i> info@tiktokcloneapp.com</a></p>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6">
                    <form id="contact_form" method="post" class="form_field">
                     <div class="row">
                        <div class="col-md-12">
                            <div class="section-heading cont">
                                <h2 class="heading">Contact Us</h2>
                            </div>
                        </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input type="hidden" name="form-title" value="Contact Us">
                                  <input class="form-control" id="name" type="text" name="Name" placeholder="Name" required="">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" id="email" type="email" name="Email" placeholder="Email" required="">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                 <input class="form-control" id="phone" type="tel" name="phone_dummy" placeholder="Phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                              <input type="hidden" class="country_code1">
                              <input type="hidden" name="Phone" class="phone_val1">
                              <input type="hidden" name="form-title" value="Contact Form">
                              </div>
                          </div>
                          <div class="col-md-6">
                              <div class="form-group">
                                  <input class="form-control" type="text" required="required" placeholder="Subject" id="subject" class="form-control" name="Subject">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group">
                                  <textarea required="required" placeholder="Message *" id="description" class="form-control" name="Message" rows="4"></textarea>
                              </div>
                          </div>
                          <div class="col-md-12 text-center">
                               <input type='hidden' value='<?php echo $_SERVER["REMOTE_ADDR"]; ?>' name='IP'>
                               <input type="hidden" name="Page_URL" id="url" value="<?php echo $_SERVER['REQUEST_URI']?>">
                          <!-- <button type="button" value="Send Message" class="btn-send submit_send submit-botton">Submit</button> -->
                             <button type="submit" class="submit_send btn btn-sm ui-gradient-green" data-form="contact_form"><span>Send Message</span></button>
                          </div>
                     
                     </div>
                    </form>
                  </div>
               </div>
               <!-- .section-heading -->
            </div>
            <!-- .container -->
         </div>
         <!-- .section -->
         <!-- Basic Footer -->
         <footer class="ui-footer">
            <!-- Footer Copyright -->
            <div class="footer-copyright bg-dark-gray">
               <div class="container">
                  <div class="row">
                     <!-- Copyright -->
                     <div class="col-12 text-center">
                        <p>
                           &copy; 2020 <a href="#" target="_blank" title="Codeytech">TikTok Clone App</a> All Rights Reserved
                        </p>
                     </div>
                  </div>
               </div>
               <!-- .container -->
            </div>
            <!-- .footer-copyright -->
         </footer>
         <div class="sticky_icons">
          <ul>
             <li><a href="https://api.whatsapp.com/send?l=en&amp;text=Hi!%20I%27m%20interested%20in%20tiktok%20&amp;phone=916369250989" target="_blank"><i class="fa shake fa-whatsapp"></i></a></li>
              <li><a href="tel:+916369250989" target="_blank"><i class="fa fa-phone"></i></a></li>
          </ul>
        </div>
         <!-- .ui-footer -->
      </div>
      <!-- .main -->
      <!-- Scripts -->
      <script type="text/javascript" src="assets/js/libs/jquery/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/slider-pro/jquery.sliderPro.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/owl.carousel/owl.carousel.min.js"></script>    
      <script type="text/javascript" src="assets/js/libs/form-validator/form-validator.min.js"></script>
      <script type="text/javascript" src="assets/js/libs/bootstrap.js"></script>
      <script type="text/javascript" src="assets/js/applify/build/applify.js"></script>
      <script src="build/js/intlTelInput.js" type="text/javascript"></script>
    <script src="build/js/utils.js" type="text/javascript"></script>
    <script>
        $("#phone").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone1").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone2").intlTelInput({
            utilsScript: "build/js/utils.js"
        });
        $("#phone3").intlTelInput({
            utilsScript: "build/js/utils.js"
        });

    </script>
    <script language="javascript" type="text/javascript">
        var maxLength = 200;
        $('textarea').keyup(function() {
        
        var msg=$(this).val();
        var urltext=msg.indexOf('http')!== -1 || msg.indexOf('www.')!== -1 || msg.indexOf('www,')!== -1;
        if(urltext){
        alert('url not allowed');
        $('textarea').val('');
        }
        else{
        var textlen = maxLength - $(this).val().length;
        $('.rchars').text(textlen);
        }
        });
        $('textarea').focusout(function() {
        
        var msg=$(this).val();
        var urltext=msg.indexOf('http')!== -1 || msg.indexOf('www.')!== -1 || msg.indexOf('www,')!== -1;
        if(urltext){
        alert('url not allowed');
        $('textarea').val('');
        }
        
        });
    </script>
    <script>
        $(".submit_send").click(function(){

            function loader(){ 
                $( '<div id="mloader"></div>' ).insertAfter( ".submit_send" );
                $(".submit_send").next().addClass("loading");
                setTimeout(function() {
                $(".submit_send").next().remove();
                }, 7000);
                
            }

            var formname = $(this).attr('data-form');
            

          $(".ajax-loader").hide();

          var name = $(this).parent().parent().parent().find('input[name="Name"]').val();
          var email = $(this).parent().parent().parent().find('input[name="Email"]').val();
          var phone = $(this).parent().parent().parent().find('input[name="phone_dummy"]').val();

          
        // console.log(name, email, phone);

          if(name!=''){

            if( (email!='') && (validateEmail(email)) ){
            if((phone!='') && (validatePhone(phone)) ){  

                $(this).attr("disabled", true);
                $(this).addClass('disabled');
                loader();    
                $(".ajax-loader").hide();
                
                
                var phone_title=$("#"+formname+" "+".selected-flag").attr("title").replace(/ *\([^)]*\) */g, "");
                $(this).parent().parent().parent().find('input[name="Phone"]').val(phone_title+" "+phone);

                var formdata=$("#"+formname).serialize()+'&_token='+$('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: "ajaxmail.php",
                    type: "POST",
                    data: formdata,
                    success: function(result){
                        console.log(result);
                        if(result == 1)
                        {
                            // alert("Mail Sent Successfully");
                            window.location.href="thank-you";
                            return false;
                        }
                        else
                        {
                            alert("Something went wrong, please try again later.");
                            return false;
                        }
                        $(".ajax-loader").hide();
                    }
                });
                } 
                else if(!validatePhone(phone)) {
                    alert("Please type correct mobile number format");
                }
                else {
                    alert("please type your phone number");
                }
            }
            else if(!validateEmail(email)) {
                alert("Please type correct email format");
            }
            else {
              alert("please type your email");
            }
          }
          else{
              alert("please fill all fields");
          } 
        }); 

        function validateEmail($email) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test($email);
        }
        function validatePhone($phone) {
            var pattern1 = new RegExp("[0-9]+");
            return pattern1.test($phone);
        }
        var url = window.location.href;
        $('#url').val(url);
    </script>
   </body>
</html>


